//=============================================================================
//
//   Co-Rotational Finite Element Method source code
//   Based on Physics-Based Animation book, chapter 10
//
//   Based on original implementation by
//							Miguel A. Otaduy,    URJC Madrid
//							Alvaro G. Perez,     URJC Madrid
//							and Javier S. Zurdo, URJC Madrid
//
//	Authors:
//			Krisztian Amit Birkas, McGill Uni.
//			Jesus Perez Rodriguez, IST Austria
//
//=============================================================================

#pragma once

#include "CommonIncludes.h"

struct LT_Triplet
{
	bool operator()(const Triplet<double> &a, const Triplet<double> &b)
	{
		return (a.row() < b.row()) || (a.row() == b.row() && a.col() < b.col());
	}
};

void AssembleSparseMatrix(VectorTd& triplets, unsigned int row, unsigned int col, const Matrix3d& m33);

void EigenTripletsToSparseMatrix(const VectorTd& triplets, SparseMatrix<double>& M);

void EigenSparseMatrixToTriplets(const SparseMatrix<double>& M, VectorTd& triplets);

void EigenVectorToCUDA(const VectorXd& veigen, vector<float>& cuda);

void CUDAToEigenVector(const vector<float>& cuda, VectorXd& veigen);

void EigenTripletsToCUDA(const VectorTd& triplets, vector<int>& rows, vector<int>& cols, vector<float>& vals);

void CUDAToEigenTriplets(const vector<int>& rows, const vector<int>& cols, const vector<float>& vals, VectorTd& triplets);