//=============================================================================
//
//   Co-Rotational Finite Element Method source code
//   Based on Physics-Based Animation book, chapter 10
//
//   Based on original implementation by
//							Miguel A. Otaduy,    URJC Madrid
//							Alvaro G. Perez,     URJC Madrid
//							and Javier S. Zurdo, URJC Madrid
//
//	Authors:
//			Krisztian Amit Birkas, McGill Uni.
//			Jesus Perez Rodriguez, IST Austria
//
//=============================================================================

#pragma once

#include "CommonIncludes.h"

#include "EnergyElement.h"

// Forward declarations

class FEM;

class NodeEBD;

//////////////////////////////////////////////////////////////////////////////
// EnergyElement_EBD_RodStretch
//
// Description:
//		Energy element corresponding to rod stretch considering the
//		element is embedded in a mesh (nodes are NodeEBD). The forces
//		and Jacobian must transferred to actual degrees of freedom.
//////////////////////////////////////////////////////////////////////////////
class EnergyElement_EBD_RodStretch: public EnergyElement
{
	///////////////////
	//// VARIABLES ////
	///////////////////
	//
	//

private:

	vector<NodeEBD*> vnodes;

	// Derivative of embeddedd nodes w.r.t. 
	// the degrees of freedom of the problem,
	// in this case a 6x18 matrix.

	double ks; // Stiffness

	/////////////////
	//// METHODS ////
	/////////////////
	//
	//

public:
	EnergyElement_EBD_RodStretch(FEM* fem, const vector<NodeEBD*>& vnodes);

	~EnergyElement_EBD_RodStretch(void) { }

	virtual double getStiffness() const { return this->ks; }
	virtual void setStiffness(double ks) { this->ks = ks; }

	virtual void Initialize();
	virtual void ComputeAndStore_Energy();
	virtual void ComputeAndStore_ForcesAndJacobian();
	virtual void AddForcesContribution(VectorXd& totalForces);
	virtual void AddJacobianContribution(VectorTd & totalJacobian);

};
#pragma once
