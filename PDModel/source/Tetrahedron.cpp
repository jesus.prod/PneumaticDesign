//=============================================================================
//
//   Co-Rotational Finite Element Method source code
//   Based on Physics-Based Animation book, chapter 10
//
//   Based on original implementation by
//							Miguel A. Otaduy,    URJC Madrid
//							Alvaro G. Perez,     URJC Madrid
//							and Javier S. Zurdo, URJC Madrid
//
//	Authors:
//			Krisztian Amit Birkas, McGill Uni.
//			Jesus Perez Rodriguez, IST Austria
//
//=============================================================================

#include "Tetrahedron.h"

void Tetrahedron::CalculateBMatrix(void)
{
	// E^-1 = (1 / det(volumeMatrix)) * Adjoint(volumeMatrix)
	Matrix3d E_inv = volumeMatrix.inverse().transpose();

	// For n > 0, i.e., n = 1, 2, 3:
	//
	//       d N_n                            d N_n                              d N_n
	// b_n = ----- = E(0,n-1)^(-1)   ;  c_n = ----- = E(1,n-1)^(-1)   ;    d_n = ----- = E(2,n-1)^(-1)
	//        dx                               dy                                 dz
	//
	// For n = 0 (there is an errata in the "Physics-Based animation" book (p.346): derivative of 1 w.r.t. x,y,z is 0):
	//
	//       d N_0
	// b_0 = ----- = - E(0,0)^(-1) - E(0,1)^(-1) - E(0,2)^(-1)
	//        dx
	// 
	//       d N_0
	// c_0 = ----- = - E(1,0)^(-1) - E(1,1)^(-1) - E(1,2)^(-1)
	//        dy
	// 
	//       d N_0
	// d_0 = ----- = - E(2,0)^(-1) - E(2,1)^(-1) - E(2,2)^(-1)
	//        dz
	//
	b[0] = -E_inv(0, 0) - E_inv(0, 1) - E_inv(0, 2); b[1] = E_inv(0, 0); b[2] = E_inv(0, 1); b[3] = E_inv(0, 2);
	c[0] = -E_inv(1, 0) - E_inv(1, 1) - E_inv(1, 2); c[1] = E_inv(1, 0); c[2] = E_inv(1, 1); c[3] = E_inv(1, 2);
	d[0] = -E_inv(2, 0) - E_inv(2, 1) - E_inv(2, 2); d[1] = E_inv(2, 0); d[2] = E_inv(2, 1); d[3] = E_inv(2, 2);
}


bool Tetrahedron::CalculateBarycentricCoords(const Vector3d& point, Vector4d& coords)
{
	// [w1, w2, w3]^T = (1 / det(volumeMatrix)) * Adjoint(volumeMatrix) * (p-x0)
	Vector3d w = initialVolumeMatrixInverse * (point - GetNode(0)->GetPosition0());
	coords = Vector4d((1.0 - w[0] - w[1] - w[2]), w[0], w[1], w[2]);
	return coords[0] >= 0.0 && coords[1] >= 0.0 && coords[2] >= 0.0 && coords[3] >= 0.0 &&
		   coords[0] <= 1.0 && coords[1] <= 1.0 && coords[2] <= 1.0 && coords[3] <= 1.0;
}


void orthonormalize(Matrix3d& matrix)
{
	Vector3d a = matrix.col(0);
	a.normalize();
	Vector3d b = matrix.col(1);
	b.normalize();
	Vector3d c = a.cross(b);
	c.normalize();

	b = c.cross(a);
	b.normalize();

	matrix.col(0) = a;
	matrix.col(1) = b;
	matrix.col(2) = c;
}

const Matrix3d Tetrahedron::CalculateRotation(void)
{
	// G = V'*V^-1
    // R = Ortho(G)

	Matrix3d R = volumeMatrix * initialVolumeMatrixInverse;
	orthonormalize(R);
	return R;
}

void Tetrahedron::PrecomputeK(const Vector3d& elasticCoeff)
{
	for (int i = 0; i < 4; ++i)
		for (int j = 0; j < 4; ++j)
			Knm[i][j] = CalculateK_nm(i, j, elasticCoeff);

	// Ensamble into a single matrix

	K = MatrixXd(12, 12);
	for (int i = 0; i < 4; ++i)
		for (int j = 0; j < 4; ++j)
			K.block<3, 3>(3*i, 3*j) = Knm[i][j];
}

const Matrix3d Tetrahedron::CalculateK_nm(int n, int m, const Vector3d& elasticCoeff)
{
	Vector3d D = elasticCoeff;
	Matrix3d K_nm;

	// K_nm = (B_n)^T * D * B_m * volume
	K_nm << D[0] * b[n] * b[m] + D[2] * (c[n] * c[m] + d[n] * d[m]), D[1] * b[n] * c[m] + D[2] * c[n] * b[m], D[1] * b[n] * d[m] + D[2] * d[n] * b[m],
		D[1] * c[n] * b[m] + D[2] * b[n] * c[m], D[0] * c[n] * c[m] + D[2] * (b[n] * b[m] + d[n] * d[m]), D[1] * c[n] * d[m] + D[2] * d[n] * c[m],
		D[1] * d[n] * b[m] + D[2] * b[n] * d[m], D[1] * d[n] * c[m] + D[2] * c[n] * d[m], D[0] * d[n] * d[m] + D[2] * (b[n] * b[m] + c[n] * c[m]);
	return K_nm * volume0;
}

Vector3d Tetrahedron::GetCenter(void)
{
	return (0.25 * (node[0]->GetPosition() + node[1]->GetPosition() + node[2]->GetPosition() + node[3]->GetPosition()));
}

// Function: SVD_3x3
//
// Input:
//      A: Matrix 3x3 which SVD is calculated (A = U*S*V')
// Output:
//      U: Orthogonal 3x3 matrix U corresponding to the SVD of the input matrix A (where A = U*S*V')
//      S: Diagonal   3x3 matrix S corresponding to the SVD of the input matrix A (where A = U*S*V')
//      V: Orthogonal 3x3 matrix V corresponding to the SVD of the input matrix A (where A = U*S*V')
//
void SVD_3x3(Matrix3d A, Matrix3d& U, Matrix3d& S, Matrix3d& V)
{
	std::cerr << "[WARNING] Not implemented function" << std::endl;

	//int m = 3, n = 3;
	//double* Aaux = new double[m*n];
	//double* Uarray = new double[m*n];
	//double* Sarray = new double[n];
	//double* Varray = new double[n*n];

	//U = Matrix3d::Zero();
	//S = Matrix3d::Zero();
	//V = Matrix3d::Zero();

	//for (int i=0; i<m; i++)
	//{
	//	int offset = i*n;
	//	for (int j=0; j<n; j++)
	//	{
	//		Aaux[offset+j] = A(i,j);
	//	}
	//}

	//svd(Aaux,m,n,Uarray,Sarray,Varray);

	//for (int i=0; i<m; i++)
	//{
	//	int offset = i*n;
	//	for (int j=0; j<n; j++)
	//	{
	//		U(i,j) = Uarray[offset+j];
	//		V(i,j) = Varray[offset+j];
	//	}
	//	S(i,i) = Sarray[i];
	//}

	//delete[] Aaux;
	//delete[] Uarray;
	//delete[] Sarray;
	//delete[] Varray;
}


double Tetrahedron::GetAverageEdgeLength(void)
{
	return (1.0 / 6.0) *
		((node[0]->GetPosition() - node[1]->GetPosition()).norm() + // edge[0]
		(node[0]->GetPosition() - node[2]->GetPosition()).norm() + // edge[1]
		(node[0]->GetPosition() - node[3]->GetPosition()).norm() + // edge[2]
		(node[1]->GetPosition() - node[2]->GetPosition()).norm() + // edge[3]
		(node[1]->GetPosition() - node[3]->GetPosition()).norm() + // edge[4]
		(node[2]->GetPosition() - node[3]->GetPosition()).norm()); // edge[5]
}

// Calculates the 6 constraints of this tetrahedron
void Tetrahedron::CalculateConstraints(void)
{
	// F = Deformation Gradient
	Matrix3d F = GetVolumeMatrix() * initialVolumeMatrixInverse;

	SVD_3x3(F, U, S, V);

	// Constraints are defined to force: smin <= S[i][i] <= smax
	C(0) = stretchAxis1 - S(0, 0);  // C0(x) = stretchAxis1 - S[0][0] >= 0
	C(1) = S(0, 0) - compressAxis1;  // C1(x) = S[0][0] - compressAxis1 >= 0
	C(2) = stretchAxis2 - S(1, 1);  // C2(x) = stretchAxis2 - S[1][1] >= 0
	C(3) = S(1, 1) - compressAxis2;  // C3(x) = S[1][1] - compressAxis2 >= 0
	C(4) = stretchAxis3 - S(2, 2);  // C4(x) = stretchAxis3 - S[2][2] >= 0
	C(5) = S(2, 2) - compressAxis3;  // C5(x) = S[2][2] - compressAxis3 >= 0
//}
}

void Tetrahedron::GetSingularValuePartialDerivatives(Matrix3d part_s[])
{
	Matrix3d E_inv_init = initialVolumeMatrixInverse;

	for (int i = 0; i < 3; i++)
	{
		// d S(i)
		// ------ = -(U_i) * (V(0,i) * (a + d + g) + V(1,i) * (b + e + h) + V(2,i) * (c + f + i))
		//  d x0
		//
		// d S(i)
		// ------ = (U_i) * (V(0,i) * a + V(1,i) * b + V(2,i) * c)
		//  d x1
		//
		// d S(i)
		// ------ = (U_i) * (V(0,i) * d + V(1,i) * e + V(2,i) * f)
		//  d x2
		//
		// d S(i)
		// ------ = (U_i) * (V(0,i) * g + V(1,i) * h + V(2,i) * i)
		//  d x3
		part_s[0].row(i) = 
			-(V(0, i) * (E_inv_init(0, 0) + E_inv_init(1, 0) + E_inv_init(2, 0)) +
			V(1, i) * (E_inv_init(0, 1) + E_inv_init(1, 1) + E_inv_init(2, 1)) +
			V(2, i) * (E_inv_init(0, 2) + E_inv_init(1, 2) + E_inv_init(2, 2))) * U.col(i);
		part_s[1].row(i) = U.col(i) * E_inv_init.row(0).dot(V.col(i));
		part_s[2].row(i) = U.col(i) * E_inv_init.row(1).dot(V.col(i));
		part_s[3].row(i) = U.col(i) * E_inv_init.row(2).dot(V.col(i));
	}
}


// Assemble the jacobian J rows for active constraints of this tetrahedron
// PRE: Tetrahedron::CalculateConstraints() method must be called before
void Tetrahedron::AssembleJ_active(vector<Triplet<double> >& J_entries, unsigned int& row_tet)
{
	unsigned int row = 0;
	const unsigned int nConstraints = 6;

	unsigned int nActive = 0;
	for (unsigned int k = 0; k < nConstraints; k++)
	{
		if (C(k) < 0.0) nActive++;
	}

	if (nActive > 0)
	{
		Matrix3d part_s[4]; // Row i of part_s[j] is the derivative of S(i) w.r.t. node[j]
		GetSingularValuePartialDerivatives(part_s);

		for (unsigned int i = 0; i < 4; i++)
		{
			if (!node[i]->IsFixed())
			{
				row = row_tet;
				unsigned int col = 3 * node[i]->GetId_Free();

				for (unsigned int k = 0; k < nConstraints; k++)
				{
					if (C(k) < 0.0) // C(k) is active
					{
						// Sparse implementation
						if (k % 2 == 0)
						{
							J_entries.push_back(Triplet<double>(row, col, -part_s[i].row(k / 2)[0]));
							J_entries.push_back(Triplet<double>(row, col + 1, -part_s[i].row(k / 2)[1]));
							J_entries.push_back(Triplet<double>(row, col + 2, -part_s[i].row(k / 2)[2]));
						}
						else
						{
							J_entries.push_back(Triplet<double>(row, col, part_s[i].row((k - 1) / 2)[0]));
							J_entries.push_back(Triplet<double>(row, col + 1, part_s[i].row((k - 1) / 2)[1]));
							J_entries.push_back(Triplet<double>(row, col + 2, part_s[i].row((k - 1) / 2)[2]));
						}
						row++;
					}
				}
			}
		}
		row_tet = row;
	}
}
