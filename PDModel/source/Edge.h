//=============================================================================
//
//   Co-Rotational Finite Element Method source code
//   Based on Physics-Based Animation book, chapter 10
//
//   Based on original implementation by
//							Miguel A. Otaduy,    URJC Madrid
//							Alvaro G. Perez,     URJC Madrid
//							and Javier S. Zurdo, URJC Madrid
//
//	Authors:
//			Krisztian Amit Birkas, McGill Uni.
//			Jesus Perez Rodriguez, IST Austria
//
//=============================================================================

#pragma once

#include "CommonIncludes.h"

// Foward declarations
class Node;

//////////////////////////////////////////////////////////////////////////////
// Edge
//
// Description:
//      Edge class for the tetrahedra mesh of OptimizedFEM
//////////////////////////////////////////////////////////////////////////////
class Edge
{

private:
   Node* origin; // Origin node of this edge
   Node* head;   // Head node of this edge
   
   /////////////////////////////////
   //// Review before deprecate ////
   ////////////////////////////////
   //
   //

   Matrix3d K_ij; // K_3x3 submatrix of stiffness matrix K
   Matrix3d A_ij; // A_3x3 submatrix of equation Av = b

   //
   //
   /////////////////////////////////
   //// Review before deprecate ////
   ////////////////////////////////

public:
   Edge(Node* node1, Node* node2)
   {
      origin = node1;
      head   = node2;
   }

   ~Edge(void) { }

   // Get/Set Methods
   Node* GetOrigin (void) const { return origin; }

   void SetOrigin (Node* node) { origin = node; }

   Node* GetHead (void) const { return head; }

   void SetHead (Node* node) { head = node; }

   bool Equals(Edge* edge) {
	   return (
		   (this->origin->GetId() == edge->GetOrigin()->GetId() && this->head->GetId() == edge->GetHead()->GetId())
		   ||
		   (this->head->GetId() == edge->GetOrigin()->GetId() && this->origin->GetId() == edge->GetHead()->GetId())
		   );
   }

   /////////////////////////////////
   //// Review before deprecate ////
   ////////////////////////////////
   //
   //

   const Matrix3d& GetKij(void) const { return K_ij; }

   void SetKij(const Matrix3d& k) { K_ij = k; }

   void AddKij(const Matrix3d& k) { K_ij += k; }

   const Matrix3d& GetAij(void) const { return A_ij; }

   void SetAij(const Matrix3d& A) { A_ij = A; }

   void AddAij(const Matrix3d& A) { A_ij += A; }

   //
   //
   /////////////////////////////////
   //// Review before deprecate ////
   ////////////////////////////////

};
