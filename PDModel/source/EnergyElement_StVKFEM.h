//=============================================================================
//
//   Co-Rotational Finite Element Method source code
//   Based on Physics-Based Animation book, chapter 10
//
//   Based on original implementation by
//							Miguel A. Otaduy,    URJC Madrid
//							Alvaro G. Perez,     URJC Madrid
//							and Javier S. Zurdo, URJC Madrid
//
//	Authors:
//			Krisztian Amit Birkas, McGill Uni.
//			Jesus Perez Rodriguez, IST Austria
//
//=============================================================================

#pragma once

#include "CommonIncludes.h"

#include "EnergyElement.h"

// Forward declarations

class FEM;

class Tetrahedron;

//////////////////////////////////////////////////////////////////////////////
// EnergyElement_StVKFEM
//
// Description:
//		Energy element corresponding on a St.VK model
//		FEM discretized using tetrahedrons. Stores the local
//		energy, foces and Jacobians prior to its assembly.
//////////////////////////////////////////////////////////////////////////////
class EnergyElement_StVKFEM : public EnergyElement
{
	///////////////////
	//// VARIABLES ////
	///////////////////
	//
	//

private:

	double lame1;
	double lame2;

	Tetrahedron* tetrahedron;

protected:


	/////////////////
	//// METHODS ////
	/////////////////
	//
	//

public:
	EnergyElement_StVKFEM(FEM* fem, Tetrahedron* tetrahedron);

	~EnergyElement_StVKFEM(void) { }

	virtual void Initialize();
	virtual void ComputeAndStore_Energy();
	virtual void ComputeAndStore_ForcesAndJacobian();
	virtual void AddForcesContribution(VectorXd& totalForces);
	virtual void AddJacobianContribution(VectorTd & totalJacobian);

};
#pragma once
