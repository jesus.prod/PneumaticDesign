//==========================================================
//
//	PhySim library. Generic library for physical simulation.
//
//	Authors:
//			Jesus Perez Rodriguez, IST Austria
//
//==========================================================

#pragma once

#include "CommonIncludes.h"

class IModel
{

public:

	// Kinematic state
	virtual int GetNumDOF() const = 0;
	virtual void GetDOFPosition(VectorXd& vx) const = 0;
	virtual void SetDOFPosition(const VectorXd& vx) = 0;

	// Mechanical state
	virtual Real GetEnergy() = 0;
	virtual const VectorXd& GetGradient() = 0;
	virtual const SMatrixXd& GetHessian() = 0;

};

