#pragma once

// State

#include <set>
#include <fstream>
#include <iostream>

using namespace std;

// Eigen

#include <Eigen/Dense>
#include <Eigen/Sparse>
using namespace Eigen;

// Typedefs

typedef double Real;
typedef vector<Triplet<Real>> VectorTd;
typedef SparseMatrix<Real> SMatrixXd;

// Utils

#include "Utils.h"