//=============================================================================
//
//   Co-Rotational Finite Element Method source code
//   Based on Physics-Based Animation book, chapter 10
//
//   Based on original implementation by
//							Miguel A. Otaduy,    URJC Madrid
//							Alvaro G. Perez,     URJC Madrid
//							and Javier S. Zurdo, URJC Madrid
//
//	Authors:
//			Krisztian Amit Birkas, McGill Uni.
//			Jesus Perez Rodriguez, IST Austria
//
//=============================================================================

#pragma once

#include "NodeEBD.h"

#include "Node.h"


NodeEBD::NodeEBD(const vector<Node*>& vnodes, const vector<double>& vcoord)
{
	assert(vnodes.size() == vcoord.size());

	this->vnodes = vnodes;
	this->vcoords = vcoord;
}

NodeEBD::~NodeEBD()
{
	// Nothing to do here...
}

Vector3d NodeEBD::ComputePosition() const
{
	Vector3d pos;
	pos.setZero();
	int k = (int) this->vnodes.size();
	for (int i = 0; i < k; ++i)
		pos += vcoords[i]*vnodes[i]->GetPosition();

	return pos;
}

Vector3d NodeEBD::ComputePosition0() const
{
	Vector3d pos;
	pos.setZero();
	int k = (int) this->vnodes.size();
	for (int i = 0; i < k; ++i)
		pos += vcoords[i] * vnodes[i]->GetPosition0();

	return pos;
}

Vector3d NodeEBD::ComputeVelocity() const
{
	Vector3d vel;
	vel.setZero();
	int k = (int) this->vnodes.size();
	for (int i = 0; i < k; ++i)
		vel += vcoords[i] * vnodes[i]->GetVelocity();

	return vel;
}

Vector3d NodeEBD::ComputeForce() const
{
	Vector3d force;
	force.setZero();
	int k = (int) this->vnodes.size();
	for (int i = 0; i < k; ++i)
		force += vcoords[i] * vnodes[i]->GetF_tot();

	return force;
}

MatrixXd NodeEBD::ComputeDerivative() const
{
	int k = (int) this->vnodes.size();
	MatrixXd der(3, 3*k);
	der.setZero();
	for (int i = 0; i < k; ++i)
		der.block<3, 3>(0, 3*k) = Matrix3d::Identity()*this->vcoords[i];
	return der;
}