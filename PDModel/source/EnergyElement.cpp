//=============================================================================
//
//   Co-Rotational Finite Element Method source code
//   Based on Physics-Based Animation book, chapter 10
//
//   Based on original implementation by
//							Miguel A. Otaduy,    URJC Madrid
//							Alvaro G. Perez,     URJC Madrid
//							and Javier S. Zurdo, URJC Madrid
//
//	Authors:
//			Krisztian Amit Birkas, McGill Uni.
//			Jesus Perez Rodriguez, IST Austria
//
//=============================================================================

#include "EnergyElement.h"

EnergyElement::EnergyElement(FEM* fem)
{
	energy = 0;
	forces = VectorXd::Zero(0);
	jacobian = MatrixXd::Zero(0, 0);

	this->fem = fem;
}