//=============================================================================
//
//   Co-Rotational Finite Element Method source code
//   Based on Physics-Based Animation book, chapter 10
//
//   Based on original implementation by
//							Miguel A. Otaduy,    URJC Madrid
//							Alvaro G. Perez,     URJC Madrid
//							and Javier S. Zurdo, URJC Madrid
//
//	Authors:
//			Krisztian Amit Birkas, McGill Uni.
//			Jesus Perez Rodriguez, IST Austria
//
//=============================================================================

#include "StaticSolver.h"

StaticSolver::StaticSolver()
{
	linearSolverType = LinearSolverType::ST_SimplicialCholesky;

	hasLastPos = false;

	linearSolverMaxError = 1e-9;
	staticSolverMaxError = 1e-3;

	linearSolverMaxIters = 1e4;
	staticSolverMaxIters = 1e2;

	lineSearchMaxIters = 10;
	lineSearchFactor = 0.5;

	regularizationIters = 10;

	maxStepSize = 10;
}

StaticSolver::~StaticSolver()
{
	// Nothing to do here...
}

void StaticSolver::Initialize(FEM* fem)
{
	// Nothing to do here...
}

double StaticSolver::ComputeEnergy(FEM* fem) const
{
	return fem->GetPotentialEnergy();
}

void StaticSolver::ComputeForces(FEM* fem, VectorXd& vf) const
{
	vf = fem->GetTotalForces();
}

void StaticSolver::ComputeJacobian(FEM* fem, SparseMatrix<double>& mJ) const
{
	mJ = fem->GetMatrix_Jacobian();
}

bool StaticSolver::SolveLinearSystem(SparseMatrix<double>& mA, VectorXd& vb, VectorXd& vx)
{
	assert(mA.cols() == mA.rows());

	switch (this->linearSolverType)
	{
	case LinearSolverType::ST_LUFactorization:
		// TODO
		break;
	case LinearSolverType::ST_ConjugateGradient:
		return StaticSolver::SolveLinearSystem_ConjugateGradient(mA, vb, vx);
		break;
	case LinearSolverType::ST_SimplicialCholesky:
		return StaticSolver::SolveLinearSystem_SimplicialCholeski(mA, vb, vx);
		break;

	}

	return false;
}

bool StaticSolver::SolveLinearSystem_ConjugateGradient(SparseMatrix<double>& mA, VectorXd& vb, VectorXd& vx)
{
	int N = vb.size();

	double normA = mA.norm();
	double sinThres = min(1e-6, max(1e-9, 1e-9*normA)); // Threshold for singularity and indefiniteness
	double regThres = min(1e-3, max(1e-6, 1e-6*normA)); // Value that determines the regularizing value

	SparseMatrix<double> mI(N, N);

	// To regularize
	mI.setIdentity();

	//// Solve the specified symmetric definite-positive linear system. Regularize the matrix
	//// if needed to create a definite-positive system, i.e. it solves (mA + tI)*vx = vb, for
	//// some value t. We iteratively increment t if the linear system is not solved prperly.

	for (int i = 0; i < this->regularizationIters; ++i)
	{
		if (i != 0)
		{
			std::cout << std::endl << "[TRACE] Regularizing system, iteration " << i;

			mA += mI*regThres*pow(10, i - 1);
		}

		ConjugateGradient<SparseMatrix<double>> solver;
		solver.setMaxIterations(this->linearSolverMaxIters);
		solver.setTolerance(this->linearSolverMaxError);

		solver.compute(mA);

		// Check triangulation

		if (solver.info() != Eigen::Success)
		{
			std::cout << std::endl << "[FAILURE] Linear solve. Error during triangulation";

			continue; // Iterate again
		}

		vx = solver.solve(vb);

		// Check calculation

		if (solver.info() != Eigen::Success)
		{
			std::cout << std::endl << "[FAILURE] Linear solve. Error during calculation";
			continue; // Iterate again
		}

		// Check indefiniteness

		double dot = vb.dot(vx);
		if (dot < 0.0)
		{
			std::cout << std::endl << "[FAILURE] Linear solve. Indefinite matrix: " << dot;
			continue; // Iterate again
		}

		// Check exact solution

		VectorXd vbTest = mA.selfadjointView<Lower>()*vx;
		double absError = (vb - vbTest).norm();
		double relError = absError / vb.norm();
		if (absError > this->linearSolverMaxError && relError > this->linearSolverMaxError)
		{
			std::cout << std::endl << "[FAILURE] Linear solve. Inexact solution: " << relError;
			continue; // Iterate again
		}

		std::cout << std::endl << "[SUCCESS] Linear solve. System solved using Conjugate Gradient";

		return true;
	}

	return false;
}

bool StaticSolver::SolveLinearSystem_SimplicialCholeski(SparseMatrix<double>& mA, VectorXd& vb, VectorXd& vx)
{
	int N = vb.size();

	double normA = mA.norm();
	double sinThres = min(1e-6, max(1e-9, 1e-9*normA)); // Threshold for singularity and indefiniteness
	double regThres = min(1e-3, max(1e-6, 1e-6*normA)); // Value that determines the regularizing value

	SparseMatrix<double> mI(N, N);

	// To regularize
	mI.setIdentity();

	//// Solve the specified symmetric definite-positive linear system. Regularize the matrix
	//// if needed to create a definite-positive system, i.e. it solves (mA + tI)*vx = vb, for
	//// some value t. We iteratively increment t if the linear system is not solved prperly.

	for (int i = 0; i < this->regularizationIters; ++i)
	{
		if (i != 0)
		{
			std::cout << std::endl << "[TRACE] Regularizing system, iteration " << i;

			mA += mI*regThres*pow(10, i - 1);
		}

		SimplicialLDLT<SparseMatrix<double>> solver;

		solver.compute(mA);

		// Check triangulation

		if (solver.info() != Eigen::Success)
		{
			std::cout << std::endl << "[FAILURE] Linear solve. Error during triangulation";

			continue; // Iterate again
		}

		// Check singularity

		bool singular= false;
		VectorXd vd = solver.vectorD();
		for (int j = 0; j < N; ++j)
		{
			if (abs(vd(j)) < sinThres)
			{
				std::cout << std::endl << "[FAILURE] Linear solve. Singular matrix, value: " << abs(vd(j));
				singular = true;
				break;
			}
		}

		if (singular)
			continue;

		vx = solver.solve(vb);

		// Check calculation

		if (solver.info() != Eigen::Success)
		{
			std::cout << std::endl << "[FAILURE] Linear solve. Error during calculation";
			continue; // Iterate again
		}

		// Check indefiniteness

		double dot = vb.dot(vx);
		if (dot < 0.0)
		{
			std::cout << std::endl << "[FAILURE] Linear solve. Indefinite matrix: " << dot;
			continue; // Iterate again
		}

		// Check exact solution

		VectorXd vbTest = mA.selfadjointView<Lower>()*vx;
		double absError = (vb - vbTest).norm();
		double relError = absError / vb.norm();
		if (absError > this->linearSolverMaxError && relError > this->linearSolverMaxError)
		{
			std::cout << std::endl << "[FAILURE] Linear solve. Inexact solution: " << relError;
			continue; // Iterate again
		}

		std::cout << std::endl << "[SUCCESS] Linear solve. System solved using Simplicial Choleski";

		return true;
	}

	return false;
}

SolveResult StaticSolver::SolveStep(FEM* fem)
{
	double E0 = 0;
	double F0 = 0;
	VectorXd dx;
	VectorXd vx0;
	VectorXd vf0;
	SparseMatrix<double> mJ;
	fem->GetPositions(vx0);

	// Syncronously update force and Jacobian. TODO: Try to avoid this, add variables
	// IsEnergyComputed, IsForceComputed, IsJacobianComputed, to the FEM class so that
	// the energy, force and Jacobian are deprecated when the position change and then
	// computed only once when they are needed.

	fem->ComputePotentialEnergy();
	fem->ComputeForceAndJacobian();

	// Get energy/force/Jacobian

	E0 = this->ComputeEnergy(fem);
	this->ComputeForces(fem, vf0);
	this->ComputeJacobian(fem, mJ);
	F0 = vf0.norm();

	std::cout << std::endl << "--" << std::endl << "Starting static solve step. Energy: " << E0 << ". Force: " << F0;

	// Check if it is in static equilibrium

	if (F0 < this->staticSolverMaxError)
		return SolveResult::Success;

	// Solve linear system. Explanation: we are minimizing the elastic potential E, which
	// is a nonlinear energy function. To minimize it, we use a Sequential Quadratic Programming
	// algorithm (SQP), i.e., we iteratively approximate E with a quadratic expression, which can
	// be minimized simply solving a linear system. Using Taylor expansion:
	//
	// E ~ G = E0 + DE/Dx * dx + (1/2) dx^T * D2E/Dx2 * dx
	//
	// To minimize this expression, we compute the derivative w.r.t. dx and equal it to zero:
	//
	// DG/Ddx = DE/Dx + D2E/Dx2 * dx = 0 => D2E/Dx2 * dx = - DE/Dx 
	//
	// Here, dx is the step in the positions that minimizes the approximate energy 
	// G. Considering that the forces are f = - DE/Dx and the Jacobian J = - D2E/Dx2,
	// we must solve the linear system -J*dx = f.

	mJ *= -1;

	this->SolveLinearSystem(mJ, vf0, dx);

	// Limit the size of dx if necessary (some time is good for debugging or convergence).

	double stepLength = dx.norm();
	if (stepLength > maxStepSize)
		dx = (dx/stepLength)*maxStepSize;

	// Line-search in dx. Explanation: the energy G is only a quadratic approximation to E,
	// and hence dx does not necessary minimize E. However, it does for sufficiently small
	// step in the dx direction. Therefore, we perform a line-search in dx until finding
	// a step-length that reduces the potential energy E.

	int itL = 0;
	//VectorXd vfL = vf0;
	VectorXd vxL = vx0;
	VectorXd dxL = dx;
	double EL = E0;
	//double FL = F0;
	bool improved = false;
	for (itL = 0; itL < this->lineSearchMaxIters; ++itL)
	{
		// Step!
		vxL = vx0 + dxL;
		stepLength = dxL.norm();
		fem->SetPositions(vxL);

		// Recompute potential energy
		fem->ComputePotentialEnergy();
		//fem->ComputeForceAndJacobian();
		EL = fem->GetPotentialEnergy();
		//this->ComputeForces(fem, vfL);
		//FL = vfL.norm();

		// Improvement?
		if (EL < E0)
		{
			improved = true;
			break; // Found!
		}

		// Didn't improve: bisect step!

		dxL = dxL*this->lineSearchFactor;
	}

	// Change improvement

	if (improved)
	{
		std::cout << std::endl << "[SUCCESS] Improved. Energy: " << EL << ". Bis: " << itL << ". Step: " << stepLength << std::endl << "--";

		// Store position
		this->lastPos = vx0;

		return SolveResult::Success;
	}
	else
	{
		std::cout << std::endl << "[FAILURE] Non-descendent. Energy: " << EL << ". Bis: " << itL << ". Step: " << stepLength << std::endl << "--";

		// Roll back changes!
		fem->SetPositions(vx0);

		return SolveResult::NonDesc;
	}
}

SolveResult StaticSolver::SolveFull(FEM* fem)
{
	// We are computing the static equilibrium of the model, i.e., f(x) = 0. Note that
	// f = -DE/Dx for some energy potential E, and hence, finding the x such that f(x) = 0
	// is equivalent to computing the critical points (minima/maxima) of energy E. This solver
	// minimizes E to find those critical points and consequently compute f(x) = 0.
	// 
	// E is a nonlinear function, i.e., it's not straightforward to minimize. We use
	// here the algorithm SQP (Sequential Quadratic Programming). That means, iteratively,
	// energy E is approximated by another function G, which is only quadratic, using Taylor
	// expansion E ~ G = E0 + DE/Dx * dx + (1/2) dx^T * D2E/Dx2 * dx. The quadratic programming
	// (QP) problem {min_x G} is really easy to solve. It involves only solving a linear system.
	// By iteratively approximating E with G and minimizing G, we progresively converge to the
	// solution.

	VectorXd vf;
	double fNorm = 0;
	int iCount = 0;

	SolveResult fullResult = SolveResult::Success;

	// While the static equilibrium is not satisfied, reiterate

	while (true)
	{
		// Solve step

		SolveResult stepResult = this->SolveStep(fem);

		if (stepResult == SolveResult::NonDesc)
		{
			fullResult = stepResult;
			break; // Cannnot improve
		}

		// Increment step

		iCount++;

		if (iCount > this->staticSolverMaxIters)
		{
			fullResult = SolveResult::MaxIter;
			break; // Reached max iterations
		}

		// Compute forces

		ComputeForces(fem, vf);
		fNorm = vf.norm();

		if (vf.norm() < this->staticSolverMaxError)
		{
			fullResult = SolveResult::Success;
			break; // Already found the solution
		}
	}

	return fullResult;
}