//==========================================================
//
//	PhySim library. Generic library for physical simulation.
//
//	Authors:
//			Jesus Perez Rodriguez, IST Austria
//
//==========================================================

#include "ModelBase.h"

ModelBase::ModelBase()
{
	this->energy = false;
	this->vgradient.setZero();
	this->mHessian.setZero();

	this->dirty = DirtyFlags::None;
}

ModelBase::~ModelBase()
{
	// Nothing to do...
}

void ModelBase::Init()
{
	// Nothing to do...
}

void ModelBase::Free()
{
	// Free energy elements

	size_t numEle = venergyElements.size();
	for (int i = 0; i < numEle; ++i)
		delete venergyElements[i];
	venergyElements.clear();
}

const vector<Node*>& ModelBase::GetNodes() const
{
	return this->vnodes;
}

int ModelBase::GetNumDOF() const
{
	return this->numDOF;
}

void ModelBase::GetDOFPosition(VectorXd& vx) const
{
	vx.resize(this->numDOF);

	size_t numNode = this->vnodes.size();
	for (size_t i = 0; i < numNode; ++i)
	{
		if (this->vnodes[i]->IsFixed())
			continue; // Ignore fixed

		vx.block<3, 1>(this->vnodes[i]->GetId_Free()*3, 0) = this->vnodes[i]->GetPosition();
	}
}

void ModelBase::SetDOFPosition(const VectorXd& vx)
{
	assert(vx.size() == this->numDOF);

	size_t numNode = this->vnodes.size();
	for (size_t i = 0; i < numNode; ++i)
	{
		if (this->vnodes[i]->IsFixed())
			continue; // Ignore fixed

		this->vnodes[i]->SetPosition(vx.block<3, 1>(this->vnodes[i]->GetId_Free() * 3, 0));
	}
}


Real ModelBase::GetEnergy()
{
	if (this->IsDirty_Energy())
		this->ComputeEnergy();

	return this->energy;
}

const VectorXd& ModelBase::GetGradient()
{
	if (this->IsDirty_Gradient())
		this->ComputeGradient();

	return this->vgradient;
}

const SMatrixXd& ModelBase::GetHessian()
{
	if (this->IsDirty_Hessian())
		this->ComputeHessian();

	return this->mHessian;
}

void ModelBase::PrepareForSimulation()
{
	// Allocate free DOF

	int idCount = 0;

	size_t numNode = this->vnodes.size();
	for (size_t i = 0; i < numNode; ++i)
	{
		if (this->vnodes[i]->IsFixed())
			continue; // Ignore fixed

		this->vnodes[i]->SetId_Free(idCount++);
	}

	this->numDOF = 3*idCount;

	// Prepare mechanics

	this->energy = 0;
	this->vgradient.resize(0);
	this->mHessian.resize(0, 0);

	this->ComputeAndStore_Gradient();
	this->ComputeAndStore_Hessian();
}

void ModelBase::DirtyMechanics()
{
	this->dirtyFlags = this->dirtyFlags | DirtyFlags::Energy;
	this->dirtyFlags = this->dirtyFlags | DirtyFlags::Gradient;
	this->dirtyFlags = this->dirtyFlags | DirtyFlags::Hessian;
}

bool ModelBase::IsDirty_Energy() const
{
	return this->dirtyFlags && DirtyFlags::Energy;
}

bool ModelBase::IsDirty_Gradient() const
{
	return this->dirtyFlags && DirtyFlags::Gradient;
}

bool ModelBase::IsDirty_Hessian() const
{
	return this->dirtyFlags && DirtyFlags::Hessian;
}

void ModelBase::ComputeAndStore_Energy()
{
	size_t numEle = this->venergyElements.size();

#pragma omp parallel for
	for (size_t i = 0; i < numEle; ++i)
		this->venergyElements[i]->ComputeAndStore_Energy();

	this->energy = 0;
	for (size_t i = 0; i < numEle; ++i)
		this->energy += this->venergyElements[i]->GetElementEnergy();
}

void ModelBase::ComputeAndStore_Gradient()
{
	size_t numEle = this->venergyElements.size();

	this->vgradient.resize(this->numDOF);
}