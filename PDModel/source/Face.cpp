//=============================================================================
//
//   Co-Rotational Finite Element Method source code
//   Based on Physics-Based Animation book, chapter 10
//
//   Based on original implementation by
//							Miguel A. Otaduy,    URJC Madrid
//							Alvaro G. Perez,     URJC Madrid
//							and Javier S. Zurdo, URJC Madrid
//
//	Authors:
//			Krisztian Amit Birkas, McGill Uni.
//			Jesus Perez Rodriguez, IST Austria
//
//=============================================================================

#pragma once

#include "Node.h"
#include "Face.h"

Face::Face() {
	n1 = new Node();
	n2 = new Node();
	n3 = new Node();
}

Face::Face(Node* node1, Node* node2, Node* node3)
{
	n1 = node1;
	n2 = node2;
	n3 = node3;
}

Face::~Face(void) {
}

double Face::computeArea() const {
	return Face::computeArea(this->n1, this->n2, this->n3);
}

Vector3d Face::computeNormal() const {
	return Face::computeNormal(this->n1, this->n2, this->n3);
}

Vector3d Face::computeCenter() const {
	return (this->n1->GetPosition() + this->n2->GetPosition() + this->n3->GetPosition()) / 3.0;
}

double Face::computeArea(Node* n1, Node* n2, Node* n3) {
	Vector3d A = n1->GetPosition();
	Vector3d B = n2->GetPosition();
	Vector3d C = n3->GetPosition();

	Vector3d AB = B - A;
	double AB_norm = AB.norm();
	Vector3d AC = C - A;
	double AC_norm = AC.norm();

	double cos_theta = AB.dot(AC) / (AB_norm * AC_norm);
	double sin_theta = std::sqrt(1.0 - std::pow(cos_theta, 2));

	double area = (0.5) * AB_norm * AC_norm * sin_theta;

	return area;
}

Vector3d Face::computeNormal(Node* n1, Node* n2, Node* n3) {
	Vector3d A = n1->GetPosition();
	Vector3d B = n2->GetPosition();
	Vector3d C = n3->GetPosition();
	Vector3d AB = B - A;
	Vector3d AC = C - A;
	Vector3d faceNormal = AB.cross(AC);

	return faceNormal;
}

double Face::computeVolume(const std::vector<Face>& faces) {
	std::vector<double> intg(10, 0);
	double mult[10] = { 1.0 / 6.0, 1.0 / 24.0, 1.0 / 24.0, 1.0 / 24.0, 1.0 / 60.0, 1.0 / 60.0, 1.0 / 60.0, 1.0 / 120.0, 1.0 / 120.0, 1.0 / 120.0 };

	for (int i = 0; i < faces.size(); ++i) {
		// vertices of face
		//double x0 = faces[i].n1->GetPosition().x();
		//double y0 = faces[i].n1->GetPosition().y();
		//double z0 = faces[i].n1->GetPosition().z();
		//double x1 = faces[i].n2->GetPosition().x();
		//double y1 = faces[i].n2->GetPosition().y();
		//double z1 = faces[i].n2->GetPosition().z();
		//double x2 = faces[i].n3->GetPosition().x();
		//double y2 = faces[i].n3->GetPosition().y();
		//double z2 = faces[i].n3->GetPosition().z();

		double x1 = faces[i].n1->GetPosition().x();
		double y1 = faces[i].n1->GetPosition().y();
		double z1 = faces[i].n1->GetPosition().z();
		double x2 = faces[i].n2->GetPosition().x();
		double y2 = faces[i].n2->GetPosition().y();
		double z2 = faces[i].n2->GetPosition().z();
		double x0 = faces[i].n3->GetPosition().x();
		double y0 = faces[i].n3->GetPosition().y();
		double z0 = faces[i].n3->GetPosition().z();

		// get edges and their crossproducts
		//double a1 = x1 - x0;
		double b1 = y1 - y0;
		double c1 = z1 - z0;
		//double a2 = x2 - x0;
		double b2 = y2 - y0;
		double c2 = z2 - z0;
		double d0 = b1*c2 - b2*c1;
		//double d1 = a2*c1 - a1*c2;
		//double d2 = a1*b2 - a2*b1;

		// compute integrals
		double f1x, f2x, f3x, g0x, g1x, g2x, f1y, f2y, f3y, g0y, g1y, g2y, f1z, f2z, f3z, g0z, g1z, g2z;
		subExpressionForVolume(x0, x1, x2, f1x, f2x, f3x, g0x, g1x, g2x);
		//subExpressionForVolume(y0, y1, y2, f1y, f2y, f3y, g0y, g1y, g2y);
		//subExpressionForVolume(z0, z1, z2, f1z, f2z, f3z, g0z, g1z, g2z);

		// update integrals
		intg[0] += d0*f1x;
		//intg[1] += d0*f2x;
		//intg[2] += d1*f2y;
		//intg[3] += d2*f2z;
		//intg[4] += d0*f3x;
		//intg[5] += d1*f3y;
		//intg[6] += d2*f3z;
		//intg[7] += d0*(y0*g0x + y1*g1x + y2*g2x);
		//intg[8] += d1*(z0*g0y + z1*g1y + z2*g2y);
		//intg[9] += d2*(x0*g0z + x1*g1z + x2*g2z);
	}

	for (int i = 0; i < 1 /*10*/; ++i) {
		intg[i] *= mult[i];
	}

	return intg[0];
}


void Face::subExpressionForVolume(double w0, double w1, double w2, double &f1, double &f2, double &f3, double &g0, double &g1, double &g2) {
	double temp0 = w0 + w1;
	f1 = temp0 + w2;
	//double temp1 = w0*w0;
	//double temp2 = temp1 + w1*temp0;
	//f2 = temp2 + w2*f1;
	//f3 = w0*temp1 + w1*temp2 + w2*f2;
	//g0 = f2 + w0*(f1 + w0);
	//g1 = f2 + w1*(f1 + w1);
	//g2 = f2 + w2*(f1 + w2);
}
