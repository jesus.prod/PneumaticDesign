﻿//=============================================================================
//
//   Co-Rotational Finite Element Method source code
//   Based on Physics-Based Animation book, chapter 10
//
//   Based on original implementation by
//							Miguel A. Otaduy,    URJC Madrid
//							Alvaro G. Perez,     URJC Madrid
//							and Javier S. Zurdo, URJC Madrid
//
//	Authors:
//			Krisztian Amit Birkas, McGill Uni.
//			Jesus Perez Rodriguez, IST Austria
//
//=============================================================================

#pragma once

#include "CommonIncludes.h"

// Forward declarations
class Node;

//////////////////////////////////////////////////////////////////////////////
// Face
//
// Description:
//      Face class for the tetrahedra mesh of OptimizedFEM
//////////////////////////////////////////////////////////////////////////////
class Face
{
public:
	Node* n1;
	Node* n2;
	Node* n3;

	Face();

	Face(Node* node1, Node* node2, Node* node3);

	~Face(void);

	double computeArea() const;
	Vector3d computeNormal() const;
	Vector3d computeCenter() const;

	static double computeArea (Node* n1, Node* n2, Node* n3);
	static Vector3d computeNormal(Node* n1, Node* n2, Node* n3);
	static double computeVolume(const std::vector<Face>& faces);

private:
	static void subExpressionForVolume(double w0, double w1, double w2, double &f1, double &f2, double &f3, double &g0, double &g1, double &g2);

};
