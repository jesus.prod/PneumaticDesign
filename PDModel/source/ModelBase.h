//==========================================================
//
//	PhySim library. Generic library for physical simulation.
//
//	Authors:
//			Jesus Perez Rodriguez, IST Austria
//
//==========================================================

#pragma once

#include "CommonIncludes.h"

#include "IModel.h"

#include "Node.h";

#include "EnergyElement.h";

enum DirtyFlags
{
	None = 0,
	Energy = 1,
	Gradient = 2,
	Hessian = 4
};

inline DirtyFlags operator|(DirtyFlags a, DirtyFlags b)
{
	return static_cast<DirtyFlags>(static_cast<int>(a) | static_cast<int>(b));
}

class ModelBase : public IModel
{
protected:

	vector<Node*> vnodes;

	int numDOF;

	Real energy;
	VectorXd vgradient;
	SMatrixXd mHessian;

	DirtyFlags dirtyFlags;

	vector<EnergyElement*> venergyElements;

public:

	// Construction
	ModelBase();

	// Destruction
	virtual ~ModelBase();

	// Initialization
	virtual void Init();
	virtual void Free();

	// Get node structure
	virtual const vector<Node*>& GetNodes() const;

	// Kinematic state
	virtual int GetNumDOF() const;
	virtual void GetDOFPosition(VectorXd& vx) const;
	virtual void SetDOFPosition(const VectorXd& vx);

	// Mechanical state
	virtual Real GetEnergy();
	virtual const VectorXd& GetGradient();
	virtual const SMatrixXd& GetHessian();

	// Simulation preparation
	virtual void PrepareForSimulation();

protected:

	// Dirty flags
	virtual void DirtyMechanics();
	virtual bool IsDirty_Energy() const;
	virtual bool IsDirty_Gradient() const;
	virtual bool IsDirty_Hessian() const;

	// Mechanical computations
	virtual void ComputeAndStore_Energy();
	virtual void ComputeAndStore_Gradient();
	virtual void ComputeAndStore_Hessian();
	virtual void AssembleGlobal_Gradient(VectorXd& vx);
	virtual void AssembleGlobal_Hessian(VectorTd& vt);

};

