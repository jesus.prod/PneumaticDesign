//=============================================================================
//
//   Co-Rotational Finite Element Method source code
//   Based on Physics-Based Animation book, chapter 10
//
//   Based on original implementation by
//							Miguel A. Otaduy,    URJC Madrid
//							Alvaro G. Perez,     URJC Madrid
//							and Javier S. Zurdo, URJC Madrid
//
//	Authors:
//			Krisztian Amit Birkas, McGill Uni.
//			Jesus Perez Rodriguez, IST Austria
//
//=============================================================================

#include <stdlib.h>
#include <GL/glut.h>
#include "FEM.h"
#include "StaticSolver.h"
#include "TetGenReader.h"

#include <iostream>
#include <fstream>

int width = 256;
int height = 256;

// Visualization controls
bool drawDebugMode = false;
bool drawTets = false;
bool drawEdges = false;
bool drawNotChamberFaces = true;
bool drawNotChamberFaceEdges = false;
bool drawFixedNodes = true;
bool drawChamberFaceEdges = false;
bool drawChamberFaces = true;
bool drawFaceNormals = false;
bool drawInternalStructure = false;
bool drawLocalCoordinateSystem = false;
bool drawNodes = false;

size_t tetrahedronIndex = 0;

// Global Tetrahedron 
vector<Tetrahedron*> e;
StaticSolver* mySolver = NULL;
FEM* myFem = NULL;

bool pause = true;
bool onePass = false;
bool useMouse = true;
double world_eye = 1.0;
bool leftButtonDown, middleButtonDown, rightButtonDown;
int mouse_x = 0, mouse_y = 0;
double rot_x = 0.0, rot_y = 0.0;
double trans_x = 0.0, trans_y = 0.0, trans_z = 0.0;
double scale = 1.0;
int res_x = 10, res_y = 2, res_z = 2;
GLfloat angle[3] = { 0.0, 0.0, 0.0 };
GLfloat camera[3] = { 0.0, 0.0, 0.0 };
GLfloat incr = 0.8;
Eigen::Vector3d worldCenter = Eigen::Vector3d(0.0, 0.0, 3.0);
double worldScale = 0.2 * height;


// Mouse interaction
void ResetTransformations(void)
{
	mouse_x = 0; mouse_y = 0;
	rot_x = 0.0; rot_y = 0.0;
	trans_x = 0.0; trans_y = 0.0; trans_z = 0.0;
	scale = 1.0;
}


void Click(int x, int y)
{
	mouse_x = x;
	mouse_y = y;
}


void DragRotate(int x, int y)
{
	double tmp = (M_PI / 10.0);
	rot_y += tmp*(x - mouse_x);
	rot_x += tmp*(y - mouse_y);
	if (rot_x < -89.9999) rot_x = -89.9999;
	if (rot_x > 89.9999) rot_x = 89.9999;
	mouse_x = x;
	mouse_y = y;
}


void DragTranslate(int x, int y)
{
	int dx = x - mouse_x;
	int dy = y - mouse_y;

	Eigen::Vector3d v(world_eye / 500.0 * dx, -world_eye / 500.0 * dy, 0.0f);

	//v.rotateX(-rot_x * M_PI / 180.0);
	//v.rotateY(-rot_y * M_PI / 180.0);

	trans_x += v[0];
	trans_y += v[1];
	trans_z += v[2];
	mouse_x = x;
	mouse_y = y;
}


void DragScale(int x, int y)
{
	double tmp = 1.0 + (y - mouse_y) * 0.01;
	scale *= tmp;
	trans_x *= tmp;
	trans_y *= tmp;
	trans_z *= tmp;
	mouse_x = x;
	mouse_y = y;
}


void LookAt(double world_scale, Eigen::Vector3d& world_center)
{
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	Eigen::Vector3d eye = Eigen::Vector3d(0.0, 0.0, 1.5 * world_eye);
	Eigen::Vector3d center = Eigen::Vector3d(0.0, 0.0, 0.0);
	Eigen::Vector3d up = Eigen::Vector3d(0.0, 1.0, 0.0);

	gluLookAt(eye[0], eye[1], eye[2], center[0], center[1], center[2], up[0], up[1], up[2]);

	// rotate
	glRotatef(rot_x, 1.0, 0.0, 0.0);
	glRotatef(rot_y, 0.0, 1.0, 0.0);

	// translate - change what's at the center of the screen
	glTranslatef(trans_x, trans_y, trans_z);

	// scale
	double s = scale * world_scale;
	glScalef(s, s, s);

	// translate - center the world's bounding box
	glTranslatef(-world_center[0], -world_center[1], -world_center[2]);
}


Eigen::Vector3d minVector(const Eigen::Vector3d& v0, const Eigen::Vector3d& v1)
{
	Eigen::Vector3d res = v0;
	if (v1[0] < v0[0]) res[0] = v1[0];
	if (v1[1] < v0[1]) res[1] = v1[1];
	if (v1[2] < v0[2]) res[2] = v1[2];

	return res;
}


Eigen::Vector3d maxVector(const Eigen::Vector3d& v0, const Eigen::Vector3d& v1)
{
	Eigen::Vector3d res = v0;

	if (v1[0] > v0[0]) res[0] = v1[0];
	if (v1[1] > v0[1]) res[1] = v1[1];
	if (v1[2] > v0[2]) res[2] = v1[2];

	return res;
}


void SetWorldCenterAndWorldScale(void)
{
	Eigen::Vector3d bb_min, bb_max;
	const vector<Node*> vectorNodes = myFem->GetNodesVector();
	unsigned int nSize = (unsigned int)vectorNodes.size();

	bb_min.setZero();
	bb_max.setZero();

	for (unsigned int i = 0; i < nSize; i++)
	{
		Eigen::Vector3d coords = vectorNodes[i]->GetPosition();

		bb_min = minVector(bb_min, coords);
		bb_max = maxVector(bb_max, coords);
	}

	worldCenter = 0.5 * (bb_min + bb_max);
	worldScale = height * world_eye * 0.002 / (bb_min - bb_max).norm();
}


void SimpleDemo(void);
void BeamDemo(int x, int y, int z);

//////////////////// OGL //////////////////
void OGL_rescalate(GLsizei w, GLsizei h)
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0, (double)w / (double)h, 1.0, 100.0);
	glViewport(0, 0, w, h);
	glMatrixMode(GL_MODELVIEW);
}

void OGL_Special_keyboard(int key, int x, int y)
{
	if (key == GLUT_KEY_RIGHT) {
		camera[0] += incr;
	}
	if (key == GLUT_KEY_LEFT) {
		camera[0] -= incr;
	}

	if (key == GLUT_KEY_UP) {
		camera[2] += incr;
	}

	if (key == GLUT_KEY_DOWN) {
		camera[2] -= incr;
	}
}

void OGL_keyboard(unsigned char key, int x, int y)
{
	if (key == '+') {
		myFem->IncreaseTargetVolume();
		std::cout << "** Target Volume increased! (" << myFem->GetTargetVolume() << ")" << std::endl;
	}
	if (key == '-') {
		myFem->DecreaseTargetVolume();
		std::cout << "** Target Volume decreased! (" << myFem->GetTargetVolume() << ")" << std::endl;
	}
	if (key == ']') {
		myFem->SetTimeStep(myFem->GetTimeStep() * 10.0);
		std::cout << "** Time step increased: " << myFem->GetTimeStep() << std::endl;
	}
	if (key == '[') {
		myFem->SetTimeStep(myFem->GetTimeStep() / 10.0);
		std::cout << "** Time step decreased: " << myFem->GetTimeStep() << std::endl;
	}
	if (key == 'p') {
		pause = !pause;
	}
	if (key == 'o') {
		onePass = true;
	}
	if (key == 'x') {
		angle[0] += incr;
	}
	if (key == 'X') {
		angle[0] -= incr;
	}
	if (key == 'y') {
		angle[1] += incr;
	}
	if (key == 'Y') {
		angle[1] -= incr;
	}
	if (key == 'z') {
		angle[2] += incr;
	}
	if (key == 'Z') {
		angle[2] -= incr;
	}
	if (key == 'r') {
		delete myFem;
		myFem = NULL;
		e.clear();
		ResetTransformations();
		BeamDemo(res_x, res_y, res_z);
	}
	if (key == 'R') {
		ResetTransformations();
	}
	if (key == 'm') {
		useMouse = !useMouse;
	}
	if (key == 't') {
		myFem->SetUseStiffnessWarping(true);
		cout << "Stiffness Warping ON" << endl << endl;
	}
	if (key == 'f') {
		myFem->SetUseStiffnessWarping(false);
		cout << "Stiffness Warping OFF" << endl << endl;
	}
	if (key == '0') {
		myFem->UseConstraints(NONE);
		cout << "Strain Limiting Constraints OFF" << endl << endl;
	}
	if (key == '1') {
		myFem->UseConstraints(STRAIN_LIMITING);
		myFem->ConstraintsMethod(POSITIONS);
		cout << "Strain Limiting Constraints ON: POSITIONS" << endl << endl;
	}
	if (key == '2') {
		myFem->UseConstraints(STRAIN_LIMITING);
		myFem->ConstraintsMethod(VELOCITIES);
		cout << "Strain Limiting Constraints ON: VELOCITIES" << endl << endl;
	}
	if (key == '3') {
		myFem->UseConstraints(STRAIN_LIMITING);
		myFem->ConstraintsMethod(LINE_SEARCH);
		cout << "Strain Limiting Constraints ON: LINE-SEARCH" << endl << endl;
	}

	if (key == '5') {
		drawTets = !drawTets;
		cout << "Drawing tets " << (drawTets ? "ON" : "OFF") << endl;
	}
	if (key == '6') {
		drawNotChamberFaces = !drawNotChamberFaces;
		cout << "Drawing not chamber faces " << (drawNotChamberFaces ? "ON" : "OFF") << endl;
	}
	if (key == '^') { // shift 6
		drawChamberFaces = !drawChamberFaces;
		cout << "Drawing chamber faces " << (drawChamberFaces ? "ON" : "OFF") << endl;
	}
	if (key == '7') {
		drawNotChamberFaceEdges = !drawNotChamberFaceEdges;
		cout << "Drawing not chamber face edges " << (drawNotChamberFaceEdges ? "ON" : "OFF") << endl;
	}
	if (key == '&') { // shift 7
		drawChamberFaceEdges = !drawChamberFaceEdges;
		cout << "Drawing chamber face edges " << (drawChamberFaceEdges ? "ON" : "OFF") << endl;
	}
	if (key == '8') {
		drawFaceNormals = !drawFaceNormals;
		cout << "Drawing face normals " << (drawFaceNormals ? "ON" : "OFF") << endl;
	}
	if (key == '9') {
		drawInternalStructure = !drawInternalStructure;
		cout << "Drawing internal structure " << (drawInternalStructure ? "ON" : "OFF") << endl;
	}
	if (key == '>') {
		//tetrahedronIndex++;
		//if (tetrahedronIndex > myFem->GetTetrahedraVector().size()) {
		//	tetrahedronIndex = myFem->GetTetrahedraVector().size() - 1;
		//}

		myFem->increaseMassDamping();
	}
	if (key == '<') {
		//tetrahedronIndex--;
		//if (tetrahedronIndex < 0) {
		//	tetrahedronIndex = 0;
		//}

		myFem->decreaseMassDamping();
	}
	if (key == '.') {
		//tetrahedronIndex++;
		//if (tetrahedronIndex > myFem->GetTetrahedraVector().size()) {
		//	tetrahedronIndex = myFem->GetTetrahedraVector().size() - 1;
		//}

		myFem->increaseStiffDamping();
	}
	if (key == ',') {
		//tetrahedronIndex--;
		//if (tetrahedronIndex < 0) {
		//	tetrahedronIndex = 0;
		//}

		myFem->decreaseStiffDamping();
	}



	if (key == 'q') {
		delete myFem;
		myFem = NULL;
		e.clear();
		exit(0);
	}
}

void OGL_display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	GLfloat color[3] = { 1.0, 1.0, 0.0 };

	if (useMouse)
	{
		LookAt(worldScale, worldCenter);
	}
	else
	{
		glLoadIdentity();
		glTranslatef(0.0, 0.0, -3.0);
		glTranslatef(camera[0], camera[1], camera[2]);

		glRotatef(angle[0], 1.0, 0.0, 0.0);
		glRotatef(angle[1], 0.0, 1.0, 0.0);
		glRotatef(angle[2], 0.0, 0.0, 1.0);
	}

	if (drawLocalCoordinateSystem) {
		Eigen::Vector3d centerOfMass(0, 0, 0);
		for (int i = 0; i < myFem->GetNodesVector().size(); ++i) {
			centerOfMass += myFem->GetNodesVector()[i]->GetPosition();
		}
		centerOfMass /= myFem->GetNodesVector().size();
		glBegin(GL_LINES);
		GLfloat black[3] = { 0.0, 0.0, 0.0 };
		glColor3fv(black);
		glVertex3f(centerOfMass[0], centerOfMass[1], centerOfMass[2]);
		glVertex3f(centerOfMass[0] + 1, centerOfMass[1], centerOfMass[2]);

		glVertex3f(centerOfMass[0], centerOfMass[1], centerOfMass[2]);
		glVertex3f(centerOfMass[0] - 1, centerOfMass[1], centerOfMass[2]);

		glVertex3f(centerOfMass[0], centerOfMass[1], centerOfMass[2]);
		glVertex3f(centerOfMass[0], centerOfMass[1] + 1, centerOfMass[2]);

		glVertex3f(centerOfMass[0], centerOfMass[1], centerOfMass[2]);
		glVertex3f(centerOfMass[0], centerOfMass[1] - 1, centerOfMass[2]);

		glVertex3f(centerOfMass[0], centerOfMass[1], centerOfMass[2]);
		glVertex3f(centerOfMass[0], centerOfMass[1], centerOfMass[2] + 1);

		glVertex3f(centerOfMass[0], centerOfMass[1], centerOfMass[2]);
		glVertex3f(centerOfMass[0], centerOfMass[1], centerOfMass[2] - 1);

		glEnd();
	}

	if (drawChamberFaces) {
		GLfloat mat_diffuseFront[] = { 0, 1, 1, 0.5 };
		GLfloat mat_diffuseBack[] = { 0, 0, 1, 0.5 };
		GLfloat mat_specular[] = { 1, 1, 1, 0.5 };
		GLfloat mat_shininess[] = { 64.0 };

		glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);

		glShadeModel(GL_SMOOTH);
		glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, mat_diffuseFront);
		glMaterialfv(GL_BACK, GL_AMBIENT_AND_DIFFUSE, mat_diffuseBack);
		glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, mat_specular);
		glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, mat_shininess);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK); /// note things are backwards.
		glFrontFace(GL_CCW);
		glEnable(GL_LIGHTING);
		glEnable(GL_LIGHT0);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glBegin(GL_TRIANGLES);
		glColor4f(0.5, 0.2, 0.3, 0.5);
		for (const Face& f : myFem->chamberFaces) {
			Eigen::Vector3d A = f.n1->GetPosition();
			Eigen::Vector3d B = f.n2->GetPosition();
			Eigen::Vector3d C = f.n3->GetPosition();

			Eigen::Vector3d normal = f.computeNormal();
			normal.normalize();

			glNormal3f(normal[0], normal[1], normal[2]);

			glVertex3f(A[0], A[1], A[2]);
			glVertex3f(B[0], B[1], B[2]);
			glVertex3f(C[0], C[1], C[2]);
		}
		glEnd();
		glDisable(GL_BLEND);
		glDisable(GL_DEPTH_TEST);
		glDisable(GL_LIGHT0);
		glDisable(GL_LIGHTING);
	}

	if (drawFaceNormals) {
		GLfloat green[3] = { 0.0, 1.0, 0.0 };
		glBegin(GL_LINES);
		glColor3fv(green);
		for (const Face& f : myFem->faces) {
			Eigen::Vector3d center = f.computeCenter();
			glVertex3f(center[0], center[1], center[2]);

			Eigen::Vector3d normal = f.computeNormal();
			normal.normalize();
			normal *= 0.1;
			glVertex3f(normal[0]+center[0], normal[1]+center[1], normal[2]+center[2]);
		}
		glEnd();
	}

	if (drawNodes) {
		GLfloat yellow[3] = { 1.0, 1.0, 0.0 };
		glPointSize(3.0);
		glBegin(GL_POINTS);
		glColor3fv(yellow);
		for (const Node* n : myFem->GetNodesVector()) {

			Eigen::Vector3d node = n->GetPosition();
			glVertex3f(node[0], node[1], node[2]);
		}
		glEnd();
	}

	if (drawDebugMode) {
		GLfloat purple[3] = { 1.0, 0.0, 1.0 };
		vector<Node*> myNodes;
		myNodes.push_back(myFem->halfEdgeDS_mesh.getNodeAtID(myFem->halfEdgeDS_mesh.nodeIndexToID(1)));
		myNodes.push_back(myFem->halfEdgeDS_mesh.getNodeAtID(myFem->halfEdgeDS_mesh.nodeIndexToID(3)));
		myNodes.push_back(myFem->halfEdgeDS_mesh.getNodeAtID(myFem->halfEdgeDS_mesh.nodeIndexToID(4)));
		myNodes.push_back(myFem->halfEdgeDS_mesh.getNodeAtID(myFem->halfEdgeDS_mesh.nodeIndexToID(6)));
		myNodes.push_back(myFem->halfEdgeDS_mesh.getNodeAtID(myFem->halfEdgeDS_mesh.nodeIndexToID(9)));
		myNodes.push_back(myFem->halfEdgeDS_mesh.getNodeAtID(myFem->halfEdgeDS_mesh.nodeIndexToID(10)));
		myNodes.push_back(myFem->halfEdgeDS_mesh.getNodeAtID(myFem->halfEdgeDS_mesh.nodeIndexToID(13)));
		myNodes.push_back(myFem->halfEdgeDS_mesh.getNodeAtID(myFem->halfEdgeDS_mesh.nodeIndexToID(14)));
		myNodes.push_back(myFem->halfEdgeDS_mesh.getNodeAtID(myFem->halfEdgeDS_mesh.nodeIndexToID(16)));
		myNodes.push_back(myFem->halfEdgeDS_mesh.getNodeAtID(myFem->halfEdgeDS_mesh.nodeIndexToID(17)));
		glPointSize(3.0);
		glBegin(GL_POINTS);
		glColor3fv(purple);
		for (size_t i = 0; i < myNodes.size(); ++i) {
				glVertex3f(myNodes[i]->GetPosition()[0], myNodes[i]->GetPosition()[1], myNodes[i]->GetPosition()[2]);
		}
		glEnd();
	}


	if (drawNotChamberFaces) {
		GLfloat mat_diffuseFront[] = { 1, 1, 0, 0.5 };
		GLfloat mat_diffuseBack[] = { 1, 0, 1, 0.5 };
		GLfloat mat_specular[] = { 1, 1, 1, 0.5 };
		GLfloat mat_shininess[] = { 64.0 };

		glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);

		glShadeModel(GL_SMOOTH);
		glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, mat_diffuseFront);
		glMaterialfv(GL_BACK, GL_AMBIENT_AND_DIFFUSE, mat_diffuseBack);
		glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, mat_specular);
		glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, mat_shininess);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT); /// note things are backwards.
		glFrontFace(GL_CCW);
		glEnable(GL_LIGHTING);
		glEnable(GL_LIGHT0);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_BLEND); 
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glBegin(GL_TRIANGLES);
		for (const Face& f : myFem->notChamberFaces) {
			Eigen::Vector3d A = f.n1->GetPosition();
			Eigen::Vector3d B = f.n2->GetPosition();
			Eigen::Vector3d C = f.n3->GetPosition();

			Eigen::Vector3d normal = f.computeNormal();

			glNormal3f(normal[0], normal[1], normal[2]);

			glVertex3f(A[0], A[1], A[2]);
			glVertex3f(B[0], B[1], B[2]);
			glVertex3f(C[0], C[1], C[2]);
		}
		glEnd();

		//glCullFace(GL_BACK);
		//glutSolidSphere(0.25, 23, 23);

		glDisable(GL_BLEND);
		glDisable(GL_DEPTH_TEST);
		glDisable(GL_LIGHT0);
		glDisable(GL_LIGHTING);
	}

	if (drawTets) {
		vector<Tetrahedron*> tets = myFem->GetTetrahedraVector();

		glEnable(GL_LINE_SMOOTH);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		
		glBegin(GL_LINES);
		glColor4f(1.0f, 1.0f, 1.0f, 0.1f);
		//size_t t = tetrahedronIndex;
		for (size_t t = 0; t < tets.size(); ++t)
		{
			for (int i = 0; i < 6; ++i) {
				Eigen::Vector3d origin = tets[t]->GetEdge(i)->GetOrigin()->GetPosition();
				Eigen::Vector3d head = tets[t]->GetEdge(i)->GetHead()->GetPosition();

				glVertex3f(origin[0], origin[1], origin[2]);
				glVertex3f(head[0], head[1], head[2]);
			}
		}
		glEnd();
	}

	if (drawEdges) {
		vector<Edge*> myEdges = myFem->GetEdgesVector();
		glBegin(GL_LINES);
		glColor3fv(color);
		for (unsigned edge = 0; edge < myEdges.size(); edge++)
		{
			Eigen::Vector3d origin(myEdges[edge]->GetOrigin()->GetPosition());
			Eigen::Vector3d head(myEdges[edge]->GetHead()->GetPosition());

			glVertex3f(origin[0], origin[1], origin[2]);
			glVertex3f(head[0], head[1], head[2]);
		}
		glEnd();
	}

	if (drawInternalStructure) {
		// Draw nodes
		vector<NodeEBD*> myNodes = myFem->getEmbeddedNodes();
		glPointSize(5.0);
		glBegin(GL_POINTS);
		glColor4f(0.0f, 1.0f, 0.0f, 1.0f);
		for (size_t i = 0; i < myNodes.size(); ++i) {
			Vector3d position = myNodes[i]->ComputePosition();
			glVertex3f(position[0], position[1], position[2]);
		}
		glEnd();

		// Draw edges
		vector<EdgeEBD*> myEdges = myFem->getEmbeddedEdges();
		glBegin(GL_LINES);
		glLineWidth(5.0);
		glColor4f(0.0f, 1.0f, 0.0f, 1.0f);
		for (unsigned edge = 0; edge < myEdges.size(); edge++)
		{
			Eigen::Vector3d origin(myEdges[edge]->GetOrigin()->ComputePosition());
			Eigen::Vector3d head(myEdges[edge]->GetHead()->ComputePosition());

			glVertex3f(origin[0], origin[1], origin[2]);
			glVertex3f(head[0], head[1], head[2]);
		}
		glEnd();
	}

	if (drawChamberFaceEdges) {
		GLfloat blue[3] = { 0.0, 0.0, 1.0 };
		glBegin(GL_LINES);
		glColor3fv(blue);
		for (const Face& f : myFem->chamberFaces) {
			Eigen::Vector3d A = f.n1->GetPosition();
			Eigen::Vector3d B = f.n2->GetPosition();
			Eigen::Vector3d C = f.n3->GetPosition();

			glVertex3f(A[0], A[1], A[2]);
			glVertex3f(B[0], B[1], B[2]);
			glVertex3f(B[0], B[1], B[2]);
			glVertex3f(C[0], C[1], C[2]);
			glVertex3f(C[0], C[1], C[2]);
			glVertex3f(A[0], A[1], A[2]);
		}
		glEnd();
	}

	if (drawNotChamberFaceEdges) {
		GLfloat cyan[3] = { 0.0, 1.0, 1.0 };
		glBegin(GL_LINES);
		glColor3fv(cyan);
		for (const Face& f : myFem->notChamberFaces) {
			Eigen::Vector3d A = f.n1->GetPosition();
			Eigen::Vector3d B = f.n2->GetPosition();
			Eigen::Vector3d C = f.n3->GetPosition();

			glVertex3f(A[0], A[1], A[2]);
			glVertex3f(B[0], B[1], B[2]);
			glVertex3f(B[0], B[1], B[2]);
			glVertex3f(C[0], C[1], C[2]);
			glVertex3f(C[0], C[1], C[2]);
			glVertex3f(A[0], A[1], A[2]);
		}
		glEnd();
	}

	if (drawFixedNodes) {
		GLfloat red[3] = { 1.0, 0.0, 0.0 };
		vector<Node*> myNodes = myFem->GetNodesVector();
		glPointSize(3.0);
		glBegin(GL_POINTS);
		glColor3fv(red);
		for (size_t i = 0; i < myNodes.size(); ++i) {
			if (myNodes[i]->IsFixed()) {
				glVertex3f(myNodes[i]->GetPosition()[0], myNodes[i]->GetPosition()[1], myNodes[i]->GetPosition()[2]);
			}
		}
		glEnd();
	}

	glFlush();
	glutSwapBuffers();
}

void OGL_idle(void)
{
	if (!pause || onePass)
	{
#if simulationMethodStatic
		mySolver->SolveStep(myFem);
#else
		myFem->SolveDynamicStep_SemiImplicit();
#endif
		onePass = false;
	}
	OGL_display();
}


void OGL_mouse(int button, int state, int x, int y)
{
	if (button == GLUT_LEFT_BUTTON && (glutGetModifiers() & GLUT_ACTIVE_SHIFT)) middleButtonDown = (state == GLUT_DOWN);
	else if (button == GLUT_LEFT_BUTTON) leftButtonDown = (state == GLUT_DOWN);
	else if (button == GLUT_MIDDLE_BUTTON) middleButtonDown = (state == GLUT_DOWN);
	else if (button == GLUT_RIGHT_BUTTON) rightButtonDown = (state == GLUT_DOWN);

	if (leftButtonDown || middleButtonDown || rightButtonDown)
	{
		Click(x, y);
	}
}


void OGL_motion(int x, int y)
{
	if (leftButtonDown)
	{
		DragRotate(x, y);
	}
	else if (middleButtonDown)
	{
		DragTranslate(x, y);
	}
	else if (rightButtonDown)
	{
		DragScale(x, y);
	}
}


void initializeOGL(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(width, height);
	glutInitWindowPosition(0, 0);
	glutCreateWindow("FEMdemo");
	glutReshapeFunc(OGL_rescalate);
	glutKeyboardFunc(OGL_keyboard);
	glutSpecialFunc(OGL_Special_keyboard);
	glutIdleFunc(OGL_idle);
	glutDisplayFunc(OGL_display);

	glutMouseFunc(OGL_mouse);
	glutMotionFunc(OGL_motion);

	GLfloat light_position[] = { 100.0, 100.0, 100.0, 1.0 };
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	glEnable(GL_NORMALIZE);

	glClearColor(0.2, 0.2, 0.2, 1.0); // Background color
}


void SimpleDemo(void)
{
	unsigned int nodeId = 0;
	unsigned int tetrahedronId = 0;

	Node* node0 = new Node(Eigen::Vector3d(1.0, 0.0, 0.0)); node0->SetId_Full(nodeId++);
	Node* node1 = new Node(Eigen::Vector3d(0.0, 0.0, 0.0)); node1->SetId_Full(nodeId++);
	Node* node2 = new Node(Eigen::Vector3d(0.0, 0.0, 1.0)); node2->SetId_Full(nodeId++);
	Node* node3 = new Node(Eigen::Vector3d(0.0, 1.0, 0.0)); node3->SetId_Full(nodeId++);
	Node* node4 = new Node(Eigen::Vector3d(-1.0, 0.0, 0.0)); node4->SetId_Full(nodeId++);

	node0->FixNode();
	node1->FixNode();
	node2->FixNode();

	Tetrahedron* e1 = new Tetrahedron(node0, node1, node2, node3); e1->SetId(tetrahedronId++);
	Tetrahedron* e2 = new Tetrahedron(node4, node1, node2, node3); e2->SetId(tetrahedronId++);

	e.push_back(e1);
	e.push_back(e2);

	myFem = new FEM(e, 0.33, 500000.0); // Poisson's ratio, Young's Modulus

	myFem->SetUseStiffnessWarping(true);
	myFem->UseDirectSolver(true); //Cholesky
	//myFem->UseDirectSolver(false);
}

void PressureDemoSimpleBoxWithCavity() {
	// tetgen generated from cube.stl

	std::vector<Node*> nodes(16+1);
	nodes[1] = new Node(Eigen::Vector3d(-5, -5, 5));
	nodes[2] = new Node(Eigen::Vector3d(-5, 5, 5));
	nodes[3] = new Node(Eigen::Vector3d(5, 5, 5));
	nodes[4] = new Node(Eigen::Vector3d(5, 5, -5));
	nodes[5] = new Node(Eigen::Vector3d(5, -5, -5));
	nodes[6] = new Node(Eigen::Vector3d(-2.5, -2.5, -5));
	nodes[7] = new Node(Eigen::Vector3d(2.5, 2.5, -5));
	nodes[8] = new Node(Eigen::Vector3d(-5, 5, -5));
	nodes[9] = new Node(Eigen::Vector3d(-5, -5, -5));
	nodes[10] = new Node(Eigen::Vector3d(5, -5, 5));
	nodes[11] = new Node(Eigen::Vector3d(-2.5, -2.5, 0));
	nodes[12] = new Node(Eigen::Vector3d(2.5, -2.5, -5));
	nodes[13] = new Node(Eigen::Vector3d(-2.5, 2.5, -5));
	nodes[14] = new Node(Eigen::Vector3d(2.5, 2.5, 0));
	nodes[15] = new Node(Eigen::Vector3d(-2.5, 2.5, 0));
	nodes[16] = new Node(Eigen::Vector3d(2.5, -2.5, 0));

	for (int i = 1; i <= 16; ++i) {
		nodes[i]->SetId(i - 1);
		if (nodes[i]->GetPosition().z() <= -4.9) {
			nodes[i]->FixNode();
		}
	}

	// inside nodes: 6,7,11,12,13,14,15,16
	std::vector<Face> faces(28 + 1);
	faces[1] = Face(nodes[7], nodes[16], nodes[12]);
	faces[2] = Face(nodes[14], nodes[16], nodes[7]);
	faces[3] = Face(nodes[10], nodes[5], nodes[1]);
	faces[4] = Face(nodes[1], nodes[5], nodes[9]);
	faces[5] = Face(nodes[2], nodes[4], nodes[3]);
	faces[6] = Face(nodes[4], nodes[2], nodes[8]);
	faces[7] = Face(nodes[12], nodes[5], nodes[4]);
	faces[8] = Face(nodes[6], nodes[5], nodes[12]);
	faces[9] = Face(nodes[15], nodes[6], nodes[11]);
	faces[10] = Face(nodes[6], nodes[9], nodes[5]);
	faces[11] = Face(nodes[4], nodes[7], nodes[12]);
	faces[12] = Face(nodes[4], nodes[13], nodes[7]);
	faces[13] = Face(nodes[8], nodes[13], nodes[4]);
	faces[14] = Face(nodes[15], nodes[13], nodes[6]);
	faces[15] = Face(nodes[3], nodes[10], nodes[1]);
	faces[16] = Face(nodes[3], nodes[1], nodes[2]);
	faces[17] = Face(nodes[15], nodes[7], nodes[13]);
	faces[18] = Face(nodes[15], nodes[14], nodes[7]);
	faces[19] = Face(nodes[14], nodes[11], nodes[16]);
	faces[20] = Face(nodes[11], nodes[14], nodes[15]);
	faces[21] = Face(nodes[16], nodes[11], nodes[6]);
	faces[22] = Face(nodes[16], nodes[6], nodes[12]);
	faces[23] = Face(nodes[3], nodes[5], nodes[10]);
	faces[24] = Face(nodes[4], nodes[5], nodes[3]);
	faces[25] = Face(nodes[8], nodes[1], nodes[9]);
	faces[26] = Face(nodes[8], nodes[2], nodes[1]);
	faces[27] = Face(nodes[6], nodes[13], nodes[8]);
	faces[28] = Face(nodes[6], nodes[8], nodes[9]);

	std::vector<Tetrahedron*> tetrahedrons(30+1);
	tetrahedrons[1] = new Tetrahedron(nodes[4], nodes[16], nodes[5], nodes[3]);
	tetrahedrons[2] = new Tetrahedron(nodes[16], nodes[4], nodes[12], nodes[7]);
	tetrahedrons[3] = new Tetrahedron(nodes[10], nodes[16], nodes[1], nodes[3]);
	tetrahedrons[4] = new Tetrahedron(nodes[16], nodes[1], nodes[3], nodes[14]);
	tetrahedrons[5] = new Tetrahedron(nodes[4], nodes[16], nodes[12], nodes[5]);
	tetrahedrons[6] = new Tetrahedron(nodes[11], nodes[15], nodes[1], nodes[14]);
	tetrahedrons[7] = new Tetrahedron(nodes[4], nodes[8], nodes[13], nodes[15]);
	tetrahedrons[8] = new Tetrahedron(nodes[1], nodes[2], nodes[3], nodes[14]);
	tetrahedrons[9] = new Tetrahedron(nodes[2], nodes[4], nodes[3], nodes[14]);
	tetrahedrons[10] = new Tetrahedron(nodes[9], nodes[11], nodes[5], nodes[6]);
	tetrahedrons[11] = new Tetrahedron(nodes[9], nodes[8], nodes[11], nodes[6]);
	tetrahedrons[12] = new Tetrahedron(nodes[11], nodes[9], nodes[5], nodes[1]);
	tetrahedrons[13] = new Tetrahedron(nodes[16], nodes[10], nodes[5], nodes[3]);
	tetrahedrons[14] = new Tetrahedron(nodes[8], nodes[9], nodes[11], nodes[1]);
	tetrahedrons[15] = new Tetrahedron(nodes[15], nodes[2], nodes[1], nodes[14]);
	tetrahedrons[16] = new Tetrahedron(nodes[2], nodes[8], nodes[4], nodes[15]);
	tetrahedrons[17] = new Tetrahedron(nodes[16], nodes[12], nodes[5], nodes[6]);
	tetrahedrons[18] = new Tetrahedron(nodes[11], nodes[16], nodes[5], nodes[6]);
	tetrahedrons[19] = new Tetrahedron(nodes[4], nodes[2], nodes[15], nodes[14]);
	tetrahedrons[20] = new Tetrahedron(nodes[15], nodes[4], nodes[14], nodes[7]);
	tetrahedrons[21] = new Tetrahedron(nodes[13], nodes[4], nodes[15], nodes[7]);
	tetrahedrons[22] = new Tetrahedron(nodes[8], nodes[6], nodes[13], nodes[15]);
	tetrahedrons[23] = new Tetrahedron(nodes[8], nodes[11], nodes[6], nodes[15]);
	tetrahedrons[24] = new Tetrahedron(nodes[8], nodes[11], nodes[15], nodes[1]);
	tetrahedrons[25] = new Tetrahedron(nodes[2], nodes[8], nodes[15], nodes[1]);
	tetrahedrons[26] = new Tetrahedron(nodes[16], nodes[11], nodes[5], nodes[1]);
	tetrahedrons[27] = new Tetrahedron(nodes[10], nodes[16], nodes[5], nodes[1]);
	tetrahedrons[28] = new Tetrahedron(nodes[4], nodes[16], nodes[3], nodes[14]);
	tetrahedrons[29] = new Tetrahedron(nodes[4], nodes[16], nodes[14], nodes[7]);
	tetrahedrons[30] = new Tetrahedron(nodes[16], nodes[11], nodes[1], nodes[14]);

	for (int i = 1; i <= 30; ++i) {
		tetrahedrons[i]->SetId(i-1);
		e.push_back(tetrahedrons[i]);
	}

	myFem = new FEM(e, 0.48, 1000000); // Poisson's ratio, Young's Modulus

	myFem->chamberFaces.push_back(faces[1]);
	myFem->chamberFaces.push_back(faces[2]);
	myFem->chamberFaces.push_back(faces[9]);
	myFem->chamberFaces.push_back(faces[14]);
	myFem->chamberFaces.push_back(faces[17]);
	myFem->chamberFaces.push_back(faces[18]);
	myFem->chamberFaces.push_back(faces[19]);
	myFem->chamberFaces.push_back(faces[20]);
	myFem->chamberFaces.push_back(faces[21]);
	myFem->chamberFaces.push_back(faces[22]);

	myFem->SetUseStiffnessWarping(true);
	myFem->UseDirectSolver(true); //Cholesky

	myFem->PrepareSimulationData(); // <-- Jesus: Should be called always before simulation
}

void PressureDemoSimpleCylinderWithCavity() {
	unsigned int nodeId = 0;
	unsigned int tetrahedronId = 0;

	int pointsOnCircle = 8;
	int height = 8;
	std::vector<Node*> nodesInner(pointsOnCircle * height);
	double midPointCoord = sqrt(2) / 2.0; // x=y in x^2 + y^2 = 1
	for (int z = 0; z < height; ++z) {
		// clockwise order in circle starting from y -> x -> -y -> -x
		nodesInner[0 + z * pointsOnCircle] = new Node(Eigen::Vector3d(0.0, 1.0, z)); nodesInner[0 + z * pointsOnCircle]->SetId_Full(nodeId++);
		nodesInner[1 + z * pointsOnCircle] = new Node(Eigen::Vector3d(midPointCoord, midPointCoord, z)); nodesInner[1 + z * pointsOnCircle]->SetId_Full(nodeId++);
		nodesInner[2 + z * pointsOnCircle] = new Node(Eigen::Vector3d(1.0, 0.0, z)); nodesInner[2 + z * pointsOnCircle]->SetId_Full(nodeId++);
		nodesInner[3 + z * pointsOnCircle] = new Node(Eigen::Vector3d(midPointCoord, -midPointCoord, z)); nodesInner[3 + z * pointsOnCircle]->SetId_Full(nodeId++);
		nodesInner[4 + z * pointsOnCircle] = new Node(Eigen::Vector3d(0.0, -1.0, z)); nodesInner[4 + z * pointsOnCircle]->SetId_Full(nodeId++);
		nodesInner[5 + z * pointsOnCircle] = new Node(Eigen::Vector3d(-midPointCoord, -midPointCoord, z)); nodesInner[5 + z * pointsOnCircle]->SetId_Full(nodeId++);
		nodesInner[6 + z * pointsOnCircle] = new Node(Eigen::Vector3d(-1.0, 0.0, z)); nodesInner[6 + z * pointsOnCircle]->SetId_Full(nodeId++);
		nodesInner[7 + z * pointsOnCircle] = new Node(Eigen::Vector3d(-midPointCoord, midPointCoord, z)); nodesInner[7 + z * pointsOnCircle]->SetId_Full(nodeId++);
	}

	std::vector<Node*> nodesOuter(pointsOnCircle * height);
	midPointCoord = sqrt(2); // x=y in x^2 + y^2 = 2
	for (int z = 0; z < height; ++z) {
		// clockwise order in circle starting from y -> x -> -y -> -x
		nodesOuter[0 + z * pointsOnCircle] = new Node(Eigen::Vector3d(0.0, 2.0, z)); nodesOuter[0 + z * pointsOnCircle]->SetId_Full(nodeId++);
		nodesOuter[1 + z * pointsOnCircle] = new Node(Eigen::Vector3d(midPointCoord, midPointCoord, z)); nodesOuter[1 + z * pointsOnCircle]->SetId_Full(nodeId++);
		nodesOuter[2 + z * pointsOnCircle] = new Node(Eigen::Vector3d(2.0, 0.0, z)); nodesOuter[2 + z * pointsOnCircle]->SetId_Full(nodeId++);
		nodesOuter[3 + z * pointsOnCircle] = new Node(Eigen::Vector3d(midPointCoord, -midPointCoord, z)); nodesOuter[3 + z * pointsOnCircle]->SetId_Full(nodeId++);
		nodesOuter[4 + z * pointsOnCircle] = new Node(Eigen::Vector3d(0.0, -2.0, z)); nodesOuter[4 + z * pointsOnCircle]->SetId_Full(nodeId++);
		nodesOuter[5 + z * pointsOnCircle] = new Node(Eigen::Vector3d(-midPointCoord, -midPointCoord, z)); nodesOuter[5 + z * pointsOnCircle]->SetId_Full(nodeId++);
		nodesOuter[6 + z * pointsOnCircle] = new Node(Eigen::Vector3d(-2.0, 0.0, z)); nodesOuter[6 + z * pointsOnCircle]->SetId_Full(nodeId++);
		nodesOuter[7 + z * pointsOnCircle] = new Node(Eigen::Vector3d(-midPointCoord, midPointCoord, z)); nodesOuter[7 + z * pointsOnCircle]->SetId_Full(nodeId++);
	}

	for (int z = 0; z < height-1; ++z) {
		for (int i = 0; i < pointsOnCircle; ++i) {
			Tetrahedron* e1 = new Tetrahedron(
				nodesInner[(z * pointsOnCircle) + i],
				nodesInner[(z * pointsOnCircle) + (i + 1) % (pointsOnCircle * 2)],
				nodesInner[(z * pointsOnCircle) + (i + pointsOnCircle) % (pointsOnCircle * 2)],
				nodesOuter[(z * pointsOnCircle) + i]
			);
			e1->SetId(tetrahedronId++);
			e.push_back(e1);

			Tetrahedron* e2 = new Tetrahedron(
				nodesInner[(z * pointsOnCircle) + (i + 1) % (pointsOnCircle * 2)],
				nodesInner[(z * pointsOnCircle) + (i + pointsOnCircle) % (pointsOnCircle * 2)],
				nodesInner[(z * pointsOnCircle) + (i + 1 + pointsOnCircle) % (pointsOnCircle * 2)],
				nodesOuter[(z * pointsOnCircle) + (i + 1 + pointsOnCircle) % (pointsOnCircle * 2)]
			);
			e2->SetId(tetrahedronId++);
			e.push_back(e2);

			Tetrahedron* e3 = new Tetrahedron(
				nodesInner[(z * pointsOnCircle) + (i + 1) % (pointsOnCircle * 2)],
				nodesOuter[(z * pointsOnCircle) + i],
				nodesOuter[(z * pointsOnCircle) + (i + 1) % (pointsOnCircle * 2)],
				nodesOuter[(z * pointsOnCircle) + (i + 1 + pointsOnCircle) % (pointsOnCircle * 2)]
			);
			e3->SetId(tetrahedronId++);
			e.push_back(e3);

			Tetrahedron* e4 = new Tetrahedron(
				nodesInner[(z * pointsOnCircle) + (i + pointsOnCircle) % (pointsOnCircle * 2)],
				nodesOuter[(z * pointsOnCircle) + i],
				nodesOuter[(z * pointsOnCircle) + (i + pointsOnCircle) % (pointsOnCircle * 2)],
				nodesOuter[(z * pointsOnCircle) + (i + 1 + pointsOnCircle) % (pointsOnCircle * 2)]);
			e4->SetId(tetrahedronId++);
			e.push_back(e4);
		}
	}

	for (int i = 0; i < pointsOnCircle; ++i) {
		nodesInner[i]->FixNode();
		nodesOuter[i]->FixNode();
	}

	myFem = new FEM(e, 0.48, 50000000); // Poisson's ratio, Young's Modulus

	for (int z = 0; z < height - 1; ++z) {
		for (int i = 0; i < pointsOnCircle-1; ++i) {
			// faces around that stand like this: |\
			//                                    |_\  //
			myFem->chamberFaces.push_back(
				Face(
					nodesInner[i + z * pointsOnCircle],
					nodesInner[i + 1 + z * pointsOnCircle],
					nodesInner[i + pointsOnCircle]
				)
			);
		}
		// faces around that stand like this: \"|
		//                                     \|  //
		for (int i = 0; i < pointsOnCircle-1; ++i) {
			myFem->chamberFaces.push_back(
				Face(
					nodesInner[i + pointsOnCircle + z * pointsOnCircle],
					nodesInner[i + 1 + z * pointsOnCircle],
					nodesInner[i + pointsOnCircle + 1 + z * pointsOnCircle]
				)
			);
		}
	}

	myFem->SetUseStiffnessWarping(true);
	myFem->UseDirectSolver(true); //Cholesky

	myFem->PrepareSimulationData(); // <-- Jesus: Should be called always before simulation
}

void BeamDemo(int x, int y, int z)
{
	double size = 0.1;

	// Create model

	myFem = FEM::CreateBlockMesh(x, y, z, size);
	myFem->SetYoungModulus(20000);

	myFem->SetUseStiffnessWarping(true);
	myFem->UseDirectSolver(false); // TAUCS
	myFem->UseConstraints(NONE); // STRAIN_LIMITING, NONE
	myFem->ConstraintsMethod(UNCONSTRAINED); // POSITIONS, VELOCITIES, LINE_SEARCH, UNCONSTRAINED

											 // Add internal structure

	double ebdy = 0;
	double ebdz = 0;
	//double ebdy = y*size / 2;
	//double ebdz = z*size / 2;
	//double ebdy = size + size / 2;
	//double ebdz = size + size / 2;
	vector<int> vidx(2 * (x - 1));
	vector<Vector3d> vpos(x);
	for (int i = 0; i < x; ++i)
	{
		if (i != 0)
		{
			vidx[2 * (i - 1) + 0] = i - 1;
			vidx[2 * (i - 1) + 1] = i - 0;
		}
		vpos[i] = Vector3d(size*i + size / 2, ebdy, ebdz);
	}

	if (!myFem->BuildEmbeddedStructure(vpos, vidx, 10000.0))
	{
		std::cout << std::endl << "There was a problem creating the internal structure...";
	}

	// Code to fix manually the initial vertices
	vector<Node*> nodes = myFem->GetNodesVector();
	for (vector<Node*>::iterator it = nodes.begin(), itEnd = nodes.end(); it != itEnd; it++)
	{
		if ((*it)->GetPosition()[0] < 0.01) (*it)->FixNode();
	}

	myFem->PrepareSimulationData();

	// Create solver

	mySolver = new StaticSolver();
	mySolver->Initialize(myFem);
}


int main(int argc, char** argv)
{
	initializeOGL(argc, argv);

	//SimpleDemo();
	//BeamDemo(res_x, res_y, res_z);
	//PressureDemoSimpleCylinderWithCavity();
	//PressureDemoSimpleBoxWithCavity();

	//TetGenReader::readAndCombineTetGenObjects("simpleTetrahedron/cylinder_with_chamber", "simpleTetrahedron/cylinder_chamber", myFem, e);
    //TetGenReader::readAndCombineTetGenObjects("cylinder_poly2/object", "cylinder_poly2/chamber", myFem, e);
	//TetGenReader::readPoly("cylinderWithThinBox/object", myFem, e);
	//TetGenReader::readPoly("cylinderWithThinBoxHigherRes/object", myFem, e);
	TetGenReader::readPoly("cylinderWithThinBoxVeryHighRes/object", myFem, e);
	//TetGenReader::readPoly("cylinderWithCylinderInside/object", myFem, e); 
	

#if simulationMethodStatic
	mySolver = new StaticSolver();
	mySolver->Initialize(myFem);
#endif

	SetWorldCenterAndWorldScale();

	//* Read FEM State from file */ myFem->Read_FEM_State();

	cout << endl << "Strain Limiting Constraints:" << endl;
	cout << "============================" << endl << endl;
	cout << "   0: Strain Limiting Constraints OFF" << endl;
	cout << "   1: Strain Limiting Constraints ON: POSITIONS" << endl;
	cout << "   2: Strain Limiting Constraints ON: VELOCITIES" << endl;
	cout << "   3: Strain Limiting Constraints ON: LINE-SEARCH (default)" << endl << endl;

	cout << "   5: Draw Tetrahedrons switch" << endl;
	cout << "   6: Draw Not Chamber Faces switch" << endl;
	cout << "   Shift 6 (^): Draw Chamber Faces switch" << endl;
	cout << "   7: Draw Not Chamber Face Edges switch" << endl;
	cout << "   Shift 7 (&): Draw Chamber Face Edges switch" << endl;
	cout << "   8: Draw Face normals" << endl;
	cout << "   <: decreaseMassDamping" << endl;
	cout << "   >: increaseMassDamping" << endl;
	cout << "   ,: decreaseStiffDamping" << endl;
	cout << "   .: increaseStiffDamping" << endl;

	cout << "   p: Run simulation" << endl;
	cout << "   o: Run simulation step by step" << endl;
	cout << "   +: Increase Pressure" << endl;
	cout << "   -: Decrease Pressure" << endl;
	cout << "   ]: Increase timestep" << endl;
	cout << "   [: Decrease timestep" << endl;
	cout << "   r: Reset simulation to default values" << endl;
	cout << "   t: Stiffness Warping ON (default)" << endl;
	cout << "   f: Stiffness Warping OFF" << endl;
	cout << "   q: Exit application" << endl << endl;

	glutMainLoop();

	delete myFem;
	myFem = NULL;
	e.clear();

	return 0;
}
