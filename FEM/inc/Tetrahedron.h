//=============================================================================
//
//   Co-Rotational Finite Element Method source code
//   Based on Physics-Based Animation book, chapter 10
//
//   Based on original implementation by
//							Miguel A. Otaduy,    URJC Madrid
//							Alvaro G. Perez,     URJC Madrid
//							and Javier S. Zurdo, URJC Madrid
//
//	Authors:
//			Krisztian Amit Birkas, McGill Uni.
//			Jesus Perez Rodriguez, IST Austria
//
//=============================================================================

#pragma once

#include "CommonIncludes.h"

#include "Node.h"
#include "Edge.h"
#include "Face.h"

// Forward declarations
class Node;
class Edge;
class Face;

// Constants

#define DENSITY 1000.0 // g/cm3
#define STRETCH_LIMIT_DEFAULT 1.05
#define COMPRESS_LIMIT_DEFAULT 0.95

//////////////////////////////////////////////////////////////////////////////
// Tetrahedron
//
// Description:
//      Tetrahedron class for the OptimizedFEM
//////////////////////////////////////////////////////////////////////////////
class Tetrahedron
{
private:
	int id; // Unique index

			// (node[0], node[1]) --> edge[0]
			// (node[0], node[2]) --> edge[1]
			// (node[0], node[3]) --> edge[2]
			// (node[1], node[2]) --> edge[3]
			// (node[1], node[3]) --> edge[4]
			// (node[2], node[3]) --> edge[5]
	Node *node[4];
	Edge *edge[6];

	double b[4], c[4], d[4]; // b_n, c_n and d_n coefficients of the four B_n matrices

	double volume0;     // Initial volume of this tetrahedron
	double volume;      // Volume of this tetrahedron
	double massDensity; // Mass density of this tetrahedron
	double determinant; // Determinant of this tetrahedron

	Matrix3d volumeMatrix;  // Matrix formed by three edges of this tetrahedron, meeting at node 0 (current volume)
	Matrix3d initialVolumeMatrix; // Initial volume matrix
	Matrix3d initialVolumeMatrixInverse; // Inverse of the initial volume matrix

	Matrix3d U, S, V; // Matrices corresponding to the SVD of the tetrahedron's Deformation Gradient
	VectorXd C; // Vector containing the 6 tetrahedron's constraints

	double stretchAxis1, compressAxis1;
	double stretchAxis2, compressAxis2;
	double stretchAxis3, compressAxis3;

	Matrix3d Knm[4][4];
	MatrixXd K;
	MatrixXd KWarped;

	double poissonRatio;
	double youngModulus;

public:
	//Constructors and destructors
	Tetrahedron(Node* x0, Node* x1, Node* x2, Node* x3)
	{
		node[0] = x0;
		node[1] = x1;
		node[2] = x2;
		node[3] = x3;

		// kg/m^3 // g/cm3
		massDensity = DENSITY; 
		C = Eigen::VectorXd(6);
		C.setZero();

		// Default compress stretch [-5%,5%]
		stretchAxis1 = stretchAxis2 = stretchAxis3 = STRETCH_LIMIT_DEFAULT;
		compressAxis1 = compressAxis2 = compressAxis3 = COMPRESS_LIMIT_DEFAULT;

		Init();
	}

	~Tetrahedron(void) { }

	int GetId(void) const { return id; }

	void SetId(int _id) { id = _id; }

	void Init(void)
	{
		UpdateDeformationDependentMagnitudes();

		volume0 = volume;
		initialVolumeMatrix = volumeMatrix;
		initialVolumeMatrixInverse = volumeMatrix.inverse();

		CalculateBMatrix();
	}

	Node* GetNode(int _id) const { return node[_id]; }

	Edge* GetEdge(int _id) const { return edge[_id]; }

	void SetEdge(Edge* _edge, int _id) { edge[_id] = _edge; }

	Vector3d GetCenter(void);

	int GetLocalNodeId(int _id) const
	{
		for (int m = 0; m<4; m++)
		{
			if (node[m]->GetId_Full() == _id) return m;
		}
		return -1;
	}

	void UpdateDeformationDependentMagnitudes()
	{
		UpdateVolumeMatrix();
		UpdateDeterminant();
		UpdateVolume();
	}

	const Matrix3d& GetVolumeMatrix(void) { return volumeMatrix; }
	void SetVolumeMatrix(const Matrix3d& m) { volumeMatrix = m; }

	void SetInitialVolumeMatrix(const Matrix3d& matrix) { initialVolumeMatrix = matrix; }
	const Matrix3d& GetInitialVolumeMatrix(void) const { return initialVolumeMatrix; }

	const Matrix3d& GetInitialVolumeMatrixInverse(void) const { return initialVolumeMatrixInverse; }
	void SetInitialVolumeMatrixInverse(const Matrix3d& matrix) { initialVolumeMatrixInverse = matrix; }

	double GetDeterminant(void) { return determinant; }
	void SetDeterminant(double d) { determinant = d; }
	void UpdateDeterminant(void) { determinant = volumeMatrix.determinant(); }

	double GetVolume(void) { return volume; }
	void SetVolume(double v) { volume = v; }
	void UpdateVolume(void) { volume = (1.0 / 6.0) * fabs(determinant); }

	double GetInitialVolume(void) { return volume0; }
	void SetInitialVolume(double v) { volume0 = v; }

	double GetMassDensity(void) { return massDensity; }
	void SetMassDensity(double d) { massDensity = d; }

	const Matrix3d CalculateRotation(void);

	bool CalculateBarycentricCoords(const Vector3d& point, Vector4d& coords);

	const MatrixXd& GetK() { return K; }
	void setKWarped(const MatrixXd& KW) { KWarped = KW; }
	const MatrixXd& GetKWarped() const { return KWarped; }

	const Matrix3d& GetK_nm(int n, int m) { return Knm[n][m]; }
	void PrecomputeK(const Vector3d& elasticCoeff);
	const Matrix3d CalculateK_nm(int n, int m, const Vector3d& elasticCoeff);

	double GetAverageEdgeLength(void);

	/////////////////////////////////
	//// Review before deprecate ////
	////////////////////////////////
	//
	//

	void CalculateConstraints(void);

	const VectorXd& GetConstraints(void) const { return C; }

	void GetSingularValuePartialDerivatives(Matrix3d part_s[]);

	void AssembleJ_active(vector<Triplet<double> >& J_entries, unsigned int& row_tet);

	//
	//
	/////////////////////////////////
	//// Review before deprecate ////
	////////////////////////////////

private:
	void UpdateVolumeMatrix(void)
	{
		volumeMatrix.col(0) = node[1]->GetPosition() - node[0]->GetPosition();
		volumeMatrix.col(1) = node[2]->GetPosition() - node[0]->GetPosition();
		volumeMatrix.col(2) = node[3]->GetPosition() - node[0]->GetPosition();
	}

	void CalculateBMatrix(void);

};
