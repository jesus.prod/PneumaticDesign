//=============================================================================
//
//   Co-Rotational Finite Element Method source code
//   Based on Physics-Based Animation book, chapter 10
//
//   Based on original implementation by
//							Miguel A. Otaduy,    URJC Madrid
//							Alvaro G. Perez,     URJC Madrid
//							and Javier S. Zurdo, URJC Madrid
//
//	Authors:
//			Krisztian Amit Birkas, McGill Uni.
//			Jesus Perez Rodriguez, IST Austria
//
//=============================================================================

#pragma once

#include "CommonIncludes.h"

// Foward declarations
class Node;

//////////////////////////////////////////////////////////////////////////////
// NodeEBD
//
// Description:
//      Embedded node. Node tha is embedded in a mesh, and whose position
//		depends on the position of other k nodes. This basic implementaion
//		considers linear interpolation.
//////////////////////////////////////////////////////////////////////////////
class NodeEBD
{
private:
	vector<Node*> vnodes;
	vector<double> vcoords;

	int id;

public:
	NodeEBD(const vector<Node*>& vnodes, const vector<double>& vcoords);

	virtual ~NodeEBD();

	int GetId(void) const { return this->id; }
	void SetId(int _id) { this->id = _id; }

	/**
	* Evaluates the deformed position of the node.
	*/
	Vector3d ComputePosition() const;

	/**
	* Evaluates the undeformed position of the node.
	*/
	Vector3d ComputePosition0() const;

	/**
	* Evaluates the velocity of the node.
	*/
	Vector3d ComputeVelocity() const;

	/**
	* Evaluates the force of the node;
	*/
	Vector3d ComputeForce() const;

	/**
	* Returns the 3x3k derivatives matrix.
	*/
	MatrixXd ComputeDerivative() const;

	const vector<Node*> GetNodes() const { return this->vnodes; }
	const vector<double> GetCoords() const { return this->vcoords; }

};
#pragma once
