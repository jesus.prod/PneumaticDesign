#pragma once

// State

#include <set>
#include <fstream>
#include <iostream>

using namespace std;

// Eigen

#include <Eigen/Dense>
#include <Eigen/Sparse>
using namespace Eigen;

// Typedefs

typedef vector<Triplet<double>> VectorTd;

// Utils

#include "Utils.h"