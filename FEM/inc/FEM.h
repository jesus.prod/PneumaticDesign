//=============================================================================
//
//   Co-Rotational Finite Element Method source code
//   Based on Physics-Based Animation book, chapter 10
//
//   Based on original implementation by
//							Miguel A. Otaduy,    URJC Madrid
//							Alvaro G. Perez,     URJC Madrid
//							and Javier S. Zurdo, URJC Madrid
//
//	Authors:
//			Krisztian Amit Birkas, McGill Uni.
//			Jesus Perez Rodriguez, IST Austria
//
//=============================================================================

#pragma once

#include "CommonIncludes.h"
 
#include "Node.h"
#include "Edge.h"
#include "Face.h"
#include "Tetrahedron.h"

#include "NodeEBD.h"
#include "EdgeEBD.h"

#include "HalfEdgeDS.h"

#include "EnergyElement.h"
#include "EnergyElement_Gravity.h"
#include "EnergyElement_StVKFEM.h"
#include "EnergyElement_CorotFEM.h"
#include "EnergyElement_Pressure.h"
#include "EnergyElement_EBD_RodStretch.h"
#include "EnergyElement_EBD_RodBending.h"

#include "omp.h"

#define simulationMethodStatic true
#define allowGravity true

// Forward declarations

class EnergyElement;
class EnergyElement_Gravity;
class EnergyElement_StVKFEM;
class EnergyElement_CorotFEM;
class EnergyElement_Pressure;

// Constants

#define GRAVITY 9.81

enum ConstraintsSimulated { NONE, STRAIN_LIMITING };
enum ConstraintMethod { UNCONSTRAINED, POSITIONS, VELOCITIES, LINE_SEARCH };

//////////////////////////////////////////////////////////////////////////////
// FEM
//
// Description:
//      FEM class for the OptimizedFEM
//////////////////////////////////////////////////////////////////////////////
class FEM
{
private:
	int globalNodeId;
	int globalTetraId;

	static double timeStep;
	static double invTimeStep;
	static double timeStepSquare;

	double massDamping;
	double stiffDamping;

	bool stiffnessWarping;

	double stretchK;
	double youngModulus; // N/m^2
	double poissonRatio;

	double initialEdgeLength;

	// Embedded structure

	vector<NodeEBD*> vectorEBDNodes;
	vector<EdgeEBD*> vectorEBDEdges;

	vector<Node*> vectorNodes;
	vector<Edge*> vectorEdges;
	vector<Tetrahedron*> vectorTetrahedra;

	double energy;
	VectorXd forces;
	SparseMatrix<double> sparseM; // Lumped matrix   
	SparseMatrix<double> sparseJ; // Jacobian matrix
	SparseMatrix<double> sparseD; // Damping matrix
	vector<Node*> vectorFreeNodes;

	bool directSolver; // true => TAUCS; false => CG

	ConstraintMethod constraintsMethod;
	ConstraintsSimulated constraintsUsed;

	vector<EnergyElement*> vectorElements;
	vector<EnergyElement_Gravity*> vectorElementsGravity; // Just pointers copies
	vector<EnergyElement_StVKFEM*> vectorElementsStVKFEM; // Just pointers copies
	vector<EnergyElement_CorotFEM*> vectorElementsCorotFEM; // Just pointers copies
	vector<EnergyElement_Pressure*> vectorElementsPressure; // Just pointers copies
	vector<EnergyElement_EBD_RodStretch*> vectorElementsEBDStretch;

	double targetVolume;

public:
	// AMIT's variables for pressure calculation START
	vector<Face> faces; // used for visualization only
	vector<Face> chamberFaces;
	vector<Face> notChamberFaces;
	HalfEdgeDS halfEdgeDS_mesh; // half edge ds on chamberFaces

	void CheckGlobalJacobian();
	void CheckGlobalForce();

	void increaseMassDamping() {
		massDamping += 0.1;
		std::cout << massDamping << std::endl;
	}
	void decreaseMassDamping() {
		massDamping -= 0.1;
		std::cout << massDamping << std::endl;
	}
	void increaseStiffDamping() {
		stiffDamping += 0.1;
		std::cout << stiffDamping << std::endl;
	}
	void decreaseStiffDamping() {
		stiffDamping -= 0.1;
		std::cout << stiffDamping << std::endl;
	}
	// AMIT's variables for pressure calculation END

	// Constructors/Destructors/Creation methods
	FEM(vector<Tetrahedron*>& _vectorTetrahedra,
		vector<Edge*>& edges,
		vector<Node*>& nodes,
		double _poisson = 0.49,
		double _young = 50000.0,
		double _density = 2.0);

	FEM(vector<Tetrahedron*>& _vectorTetrahedra,
		double _poisson = 0.49,
		double _young = 500000.0,
		double _density = 2.0);

	FEM(vector<Tetrahedron*>& _vectorTetrahedra,
		vector<Edge*>& _vectorEdges,
		vector<Node*>& _vectorNodes,
		vector<Face>& _chamberFaces,
		double _poisson = 0.49,
		double _young = 500000.0,
		double _density = 2.0);

	FEM(int nverts, double *pos, int ntets, int *inds,
		double scale = 1.0,
		const Vector3d& translate = Vector3d::Zero(),
		const Matrix3d& rotate = Matrix3d::Identity());

	~FEM(void)
	{
		freeGeometry();
		freeElements();
	}

	// Embedded structure

	const vector<NodeEBD*>& getEmbeddedNodes() const { return this->vectorEBDNodes; }
	const vector<EdgeEBD*>& getEmbeddedEdges() const { return this->vectorEBDEdges; }

	bool BuildEmbeddedStructure(const vector<Vector3d>& vpos, const vector<int>& vidx, double ks);

	// Free methods

	void freeGeometry()
	{
		for (vector<Edge*>::iterator it = vectorEdges.begin(), itEnd = vectorEdges.end(); it != itEnd; it++)
		{
			delete *it;
			*it = NULL;
		}

		for (vector<Node*>::iterator it = vectorNodes.begin(), itEnd = vectorNodes.end(); it != itEnd; it++)
		{
			delete *it;
			*it = NULL;
		}

		for (vector<Tetrahedron*>::iterator it = vectorTetrahedra.begin(), itEnd = vectorTetrahedra.end(); it != itEnd; it++)
		{
			delete *it;
			*it = NULL;
		}

		vectorNodes.clear();
		vectorEdges.clear();
		vectorFreeNodes.clear();
		vectorTetrahedra.clear();
	}

	void freeEmbeddedStructure()
	{
		for (vector<EdgeEBD*>::iterator it = vectorEBDEdges.begin(), itEnd = vectorEBDEdges.end(); it != itEnd; it++)
		{
			delete *it;
			*it = NULL;
		}

		for (vector<NodeEBD*>::iterator it = vectorEBDNodes.begin(), itEnd = vectorEBDNodes.end(); it != itEnd; it++)
		{
			delete *it;
			*it = NULL;
		}

		this->vectorEBDEdges.clear();
		this->vectorEBDNodes.clear();
	}

	void freeElements()
	{
		for (vector<EnergyElement*>::iterator it = vectorElements.begin(), itEnd = vectorElements.end(); it != itEnd; it++)
		{
			delete *it;
			*it = NULL;
		}

		this->vectorElements.clear();
		this->vectorElementsGravity.clear();
		this->vectorElementsCorotFEM.clear();
		this->vectorElementsStVKFEM.clear();
	}

	static FEM* ReadTGMeshFile(const char* filename, bool inverted = false, double scale = 1.0, const Vector3d& translate = Vector3d::Zero(), const Matrix3d& rotate = Matrix3d::Identity());
	static FEM* CreateBlockMesh(int numX, int numY, int numZ, double size, const Vector3d& translate = Vector3d::Zero(), const Matrix3d& rotate = Matrix3d::Identity());

	// Serialization methods

	void Read_FEM_State(void);
	void Save_FEM_State(void);

	// Solve static/dynamic single steps

	void SolveDynamicStep_SemiImplicit(void);
	void SolveDynamicStep_FullImplicit(void);
	void SolveStaticStep_NetwonRaphson(void);

	// Get/Set simulation attributes

	void InitializeAttributes(double _poisson = 0.33, double _young = 500000.0);

	void SetMassDensity(double density);

	void SetTimeStep(double _timeStep)
	{
		timeStep = _timeStep;
		invTimeStep = 1.0 / _timeStep;
		timeStepSquare = _timeStep * _timeStep;
	}

	static double GetTimeStep() { return timeStep; }
	static double GetInvTimeStep() { return invTimeStep; }
	static double GetTimeStepSquare() { return timeStepSquare; }

	void SetUseStiffnessWarping(bool use) { stiffnessWarping = use; }
	bool GetUseStiffnessWarping() const { return stiffnessWarping; }

	void UseConstraints(ConstraintsSimulated type) { constraintsUsed = type; }
	void ConstraintsMethod(ConstraintMethod type) { constraintsMethod = type; }

	double GetEBDStretchK() const { return stretchK; }
	double GetYoungModulus() const { return youngModulus; }
	double GetPoissonRatio() const { return poissonRatio; }

	void SetEBDStretchK(double _stretchK) { stretchK = _stretchK; }
	void SetYoungModulus(double _youngModulus) { youngModulus = _youngModulus; }
	void SetPoissonRatio(double _poissonRatio) { poissonRatio = _poissonRatio; }

	void UseDirectSolver(const bool value) { directSolver = value; }

	double GetAverageEdgeLength(void);

	// Get FEM structure vectors

	const vector<Tetrahedron*>& GetTetrahedraVector(void) const { return vectorTetrahedra; }
	const vector<Node*>& GetFreeNodesVector(void) const { return vectorFreeNodes; }
	const vector<Node*>& GetNodesVector(void) const { return vectorNodes; }
	const vector<Edge*>& GetEdgesVector(void) const { return vectorEdges; }

	// Pre-simulation data computation

	void CreateEnergyElements(void);
	void InitializeEnergyElements(void);
	void PrepareSimulationData(void);
	void ComputeLumpedMass(void);

	Vector3d CalculateElasticityCoefficients(const double poissonRatio, const double youngModulus);

	// Compute energy/force/Jacobian

	void ComputePotentialEnergy(void);
	void ResetForceAndJacobian(void);
	void ComputeForceAndJacobian(void);

	// Pressure
	void SetTargetVolume(double v) { this->targetVolume = v; }
	double GetTargetVolume(void);
	void IncreaseTargetVolume(void);
	void DecreaseTargetVolume(void);

	// Retrieve energy/force/Jacobian

	double GetPotentialEnergy(void);
	const VectorXd& GetTotalForces() const { return this->forces; };
	const SparseMatrix<double>& GetMatrix_LumpedMass() const { return this->sparseM; }
	const SparseMatrix<double>& GetMatrix_Jacobian() const { return this->sparseJ; }
	const SparseMatrix<double>& GetMatrix_Damping() const { return this->sparseD; }

	// Get/Set Kinematic state

	void GetPositions(VectorXd& x) const;
	void SetPositions(const VectorXd& x);

	void GetVelocities(VectorXd& v) const;
	void SetVelocities(const VectorXd& v);

	void SetKinematicState(const VectorXd& x, const VectorXd& v);

	/////////////////////////////////
	//// Review before deprecate ////
	////////////////////////////////
	//
	//

	SimplicialLLT<SparseMatrix<double>> llt;

	void SolveLinearSystem_Eigen(void);

	//
	//
	/////////////////////////////////
	//// Review before deprecate ////
	////////////////////////////////

protected:
	void CreateEdgesFromNodes(void);
	void CreateNodesAndEdges(void);

	int IncGlobalNodeId(void) { return globalNodeId++; }
	int GetGlobalNodeId(void) const { return globalNodeId; }
	void SetGlobalNodeId(const int id) { globalNodeId = id; }

	int IncGlobalTetrahedronId(void) { return globalTetraId++; }
	int GetGlobalTetrahedronId(void) const { return globalTetraId; }
	void SetGlobalTetrahedronId(const int id) { globalTetraId = id; }

	// TODO: The implementation is usually clearner Energy/force/Jacobian 
	// computation code is in the "energy elements", i.e. FEM elements in
	// this case. Consider moving code there at some point

	void ComputePressureForceAndJacobian(void);

	void AssembleDynamicSystem_Matrix(SparseMatrix<double>& sparseA);
	void AssembleStaticSystem_Matrix(SparseMatrix<double>& sparseA);

	void AssembleDynamicSystem_RHS(VectorXd& b);
	void AssembleStaticSystem_RHS(VectorXd& b);

	/**
	* TODO: This function should content all the code responsible of 
	* updating kinematics dependent magnitudes that are necessary for
	* internal computations, e.g. tetrahedra volume. It should be
	* called after each solver step or kinematic state change
	*/
	void UpdateDeformationDependentMagnitudes();

	void FillTripletSparseBlock3x3(vector<Triplet<double> >& _tripletVector, unsigned int row, unsigned int col, const Matrix3d& m33) const;

	/////////////////////////////////
	//// Review before deprecate ////
	////////////////////////////////
	//
	//

	void ComputeDampingForceAndJacobian(void);
	void ComputeInternalForceAndJacobian(void);
	void ComputeExternalForceAndJacobian(void);

	void AssembleMatrix_LumpedMass(SparseMatrix<double>& sparseM);
	void AssembleMatrix_Jacobian(SparseMatrix<double>& sparseJ);
	void AssembleMatrix_Damping(SparseMatrix<double>& sparseD);

	void ComputeAii_Submatrix(Node* node);
	void ComputeAij_Submatrix(Edge* edge);

	void AssembleA_Matrix_Eigen(SparseMatrix<double>& sparseA);

	void SetRhsToEigen(VectorXd& b);

	void ComputeRhsToEigen(VectorXd& _b, VectorXd& _v);

	void RhsPositionsAndVelocitiesFromEigen(const VectorXd& b, const VectorXd& x, const VectorXd& v);

	void GetForces(VectorXd& f) const;

	void FormulateActiveConstraints(unsigned int& _nActive, unsigned int& _nStrainLimiting, unsigned int& _nContacts, VectorXd& _C0, SparseMatrix<double>& _J, SparseMatrix<double>& _JT, vector<unsigned int>& inds, double& _error);

	void FormulateStrainLimitingConstraints_Eigen(unsigned int& _nStrainLimiting, VectorXd& _C0, SparseMatrix<double>& _J, SparseMatrix<double>& _JT);

	double CalculateNonLinearConstraintsError(unsigned int& _nStrainLimiting);

	void AddActiveConstraints_Eigen(VectorXd& _v, VectorXd& _b);

	//
	//
	/////////////////////////////////
	//// Review before deprecate ////
	////////////////////////////////
};

