//=============================================================================
//
//   Co-Rotational Finite Element Method source code
//   Based on Physics-Based Animation book, chapter 10
//
//   Based on original implementation by
//							Miguel A. Otaduy,    URJC Madrid
//							Alvaro G. Perez,     URJC Madrid
//							and Javier S. Zurdo, URJC Madrid
//
//	Authors:
//			Krisztian Amit Birkas, McGill Uni.
//			Jesus Perez Rodriguez, IST Austria
//
//=============================================================================

#pragma once

#include "CommonIncludes.h"

#include "EnergyElement.h"

// Forward declarations

class FEM;

//////////////////////////////////////////////////////////////////////////////
// EnergyElement_Damping
//
// Description:
//		Energy element corresponding the damping force.
//////////////////////////////////////////////////////////////////////////////
class EnergyElement_Damping : public EnergyElement
{
	///////////////////
	//// VARIABLES ////
	///////////////////
	//
	//

	/////////////////
	//// METHODS ////
	/////////////////
	//
	//

public:
	EnergyElement_Damping(FEM* fem);

	~EnergyElement_Damping(void) { }

	virtual void ComputeAndStore_Forces();

	virtual void AddForcesContribution(VectorXd& totalForces);

};
