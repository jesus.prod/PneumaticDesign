//=============================================================================
//
//   Co-Rotational Finite Element Method source code
//   Based on Physics-Based Animation book, chapter 10
//
//   Based on original implementation by
//							Miguel A. Otaduy,    URJC Madrid
//							Alvaro G. Perez,     URJC Madrid
//							and Javier S. Zurdo, URJC Madrid
//
//	Authors:
//			Krisztian Amit Birkas, McGill Uni.
//			Jesus Perez Rodriguez, IST Austria
//
//=============================================================================

#pragma once

#include "CommonIncludes.h"

// Foward declarations
class Edge;

//////////////////////////////////////////////////////////////////////////////
// Node
//
// Description:
//      Node class for the tetrahedra mesh of OptimizedFEM
//////////////////////////////////////////////////////////////////////////////
class Node
{
private:
	int id; // Unique global index

	int freeNodeId; // Unique global index for free nodes

	// Geometry info
	Vector3d position0; // Initial position
	Vector3d position; // Current position
	Vector3d velocity; // Current velocity

	double mass;

	bool fixed;
	bool marked;

	vector<Edge*> vectorEdges; // Stores the edges meeting at this node

	// NOTE this variables are now mainly for debugging and visualization
	// purposes. When the forces are assembled, they are asembled directly
	// into the N-dimensional vector.

	Vector3d f_ext;     // External force of the id'th node
	Vector3d f_dam;		// Damping force of the id'th node
	Vector3d f_int;     // Internal force of the id'th node
	Vector3d f_t;       // Total force of the id'th node

	/////////////////////////////////
	//// Review before deprecate ////
	////////////////////////////////
	//
	//

	Matrix3d K_ii;      // K_3x3 submatrix of stiffness matrix K (for nodes-edges implementation)

	Vector3d f_extTemp; // Temp (Deprecated?)

	Matrix3d A_ii;      // A_3x3 submatrix of equation Av = b (for nodes-edges implementation)
	Vector3d b_i;       // b vector of equation Av = b (for nodes-edges implementation)

	//
	//
	/////////////////////////////////
	//// Review before deprecate ////
	////////////////////////////////

public:
	Node(const Vector3d& _coords = Vector3d(0.0, 0.0, 0.0))
	{
		id = -1;
		freeNodeId = -1;
		position = _coords;
		position0 = _coords;
		velocity = Vector3d::Zero();
		mass = 0.0;
		fixed = false;
		marked = false;
		K_ii = Matrix3d::Zero();
		A_ii = Matrix3d::Zero();
		b_i = Vector3d::Zero();
		f_int = Vector3d::Zero();
		f_ext = Vector3d::Zero();
		f_dam = Vector3d::Zero();
		f_t = Vector3d::Zero();
		vectorEdges.clear();
		f_extTemp = Vector3d::Zero();
	}

	~Node(void) { }

	// Get/Set Methods

	int GetId(void) const { return GetId_Full(); }
	void SetId(int _id) { SetId_Full(_id); }

	int GetId_Full(void) const { return id; }
	void SetId_Full(int _id) { id = _id; }

	int GetId_Free(void) const { return freeNodeId; }
	void SetId_Free(int _id) { freeNodeId = _id; }

	double GetMass(void) const { return mass; }
	void SetMass(double _mass) { mass = _mass; }
	void AddMass(double _mass) { mass += _mass; }

	bool IsFixed(void) const { return fixed; }
	void FixNode(void) { fixed = true; }
	void UnfixNode(void) { fixed = false; }

	bool IsMarked(void) const { return marked; }
	void SetMarked(bool _marked) { marked = _marked; }

	const Vector3d& GetPosition(void) const { return position; }
	void SetPosition(const Vector3d& x) { position = x; }

	const Vector3d& GetPosition0(void) const { return position0; }
	void SetPosition0(const Vector3d& x) { position0 = x; }

	const Vector3d& GetVelocity(void) const { return velocity; }
	void SetVelocity(const Vector3d& v) { velocity = v; }

	const Vector3d& GetF_ext(void) const { return f_ext; }
	void SetF_ext(const Vector3d& force) { f_ext = force; }
	void AddF_ext(const Vector3d& force) { f_ext += force; }

	const Vector3d& GetF_dam(void) const { return f_dam; }
	void SetF_dam(const Vector3d& force) { f_dam = force; }
	void AddF_dam(const Vector3d& force) { f_dam += force; }

	const Vector3d& GetF_int(void) const { return f_int; }
	void SetF_int(const Vector3d& force) { f_int = force; }
	void AddF_int(const Vector3d& force) { f_int += force; }

	const Vector3d& GetF_tot(void) const { return f_t; }
	void SetF_tot(const Vector3d& force) { f_t = force; }
	void AddF_tot(const Vector3d& force) { f_t += force; }

	void AddAdjacentEdge(Edge* edge) { vectorEdges.push_back(edge); }
	const Edge* GetAdjacentEdge(int id) const { return vectorEdges[id]; }
	vector<Edge*>& GetAdjacentEdges(void) { return vectorEdges; }

	/////////////////////////////////
	//// Review before deprecate ////
	////////////////////////////////
	//
	//

	const Matrix3d& GetKii(void) const { return K_ii; }

	void SetKii(const Matrix3d& k) { K_ii = k; }


	void AddExternalForce(const Vector3d& f) { f_extTemp += f; } // TEMP

	const Vector3d& GetExternalForce(void) const { return f_extTemp; } // TEMP

	void AddKii(const Matrix3d& k) { K_ii += k; }

	const Matrix3d& GetAii(void) const { return A_ii; }

	void SetAii(const Matrix3d& A) { A_ii = A; }

	void AddAii(const Matrix3d& A) { A_ii += A; }

	const Vector3d& Get_bi(void) const { return b_i; }

	void Set_bi(const Vector3d& b) { b_i = b; }

	void Add_bi(const Vector3d& b) { b_i += b; }

	//void CalculateF_ext(void) { f_ext = Vector3d(0.0, -(mass * GRAVITY), 0.0) + f_extTemp; }
	//void CalculateF_damp(double massDamping) { f_damp = -mass * massDamping * velocity; }

	//
	//
	/////////////////////////////////
	//// Review before deprecate ////
	////////////////////////////////

};
