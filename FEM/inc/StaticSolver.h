#pragma once
//=============================================================================
//
//   Co-Rotational Finite Element Method source code
//   Based on Physics-Based Animation book, chapter 10
//
//   Based on original implementation by
//							Miguel A. Otaduy,    URJC Madrid
//							Alvaro G. Perez,     URJC Madrid
//							and Javier S. Zurdo, URJC Madrid
//
//	Authors:
//			Krisztian Amit Birkas, McGill Uni.
//			Jesus Perez Rodriguez, IST Austria
//
//=============================================================================

#pragma once

#include "CommonIncludes.h"

#include "FEM.h"

// Foward declarations
class FEM;

enum LinearSolverType
{
	ST_ConjugateGradient,
	ST_SimplicialCholesky,
	ST_LUFactorization
};

enum SolveResult
{
	Success,
	MaxIter,	// Maximum iterations reached
	NonDesc		// Non-descendent step happened
};

//////////////////////////////////////////////////////////////////////////////
// StaticSolver
//
// Description:
//		Numerical solver for computing the static equilibrium of a FEM model.
//////////////////////////////////////////////////////////////////////////////
class StaticSolver
{

private:

	LinearSolverType linearSolverType;

	bool hasLastPos;
	VectorXd lastPos;

	double linearSolverMaxError;
	double staticSolverMaxError;

	int linearSolverMaxIters;
	int	staticSolverMaxIters;

	int lineSearchMaxIters;
	double lineSearchFactor;

	int regularizationIters;

	double maxStepSize;

public:

	// Constructors/Destructors/Creation methods

	StaticSolver();

	virtual ~StaticSolver();

	virtual void Initialize(FEM* fem);

	// Solver configuration

	double GetLinearSolverMaxError() const { return this->linearSolverMaxError; }
	double GetStaticSolverMaxError() const { return this->staticSolverMaxError; }

	int GetLinearSolverMaxIters() const { return this->linearSolverMaxIters; }
	int GetStaticSolverMaxIters() const { return this->staticSolverMaxIters; }

	void SetLinearSolverMaxIters(int mi) { this->linearSolverMaxIters = mi; }
	void SetStaticSolverMaxIters(int mi) { this->staticSolverMaxIters = mi; }

	void SetLinearSolverMaxError(double me) { this->linearSolverMaxError = me; }
	void SetStaticSolverMaxError(double me) { this->staticSolverMaxError = me; }

	int GetLineSearchMaxIters() const { return this->lineSearchMaxIters; }
	void SetLineSearchMaxIters(int mi) { this->lineSearchMaxIters = mi; }

	double GetLineSearchFactor() const { return this->lineSearchFactor; }
	void SetLineSearchFactor(double fa) { this->lineSearchFactor = fa; }

	int GetRegularizationIters() const { return this->regularizationIters; }
	void SetRegularizationIters(int ri) { this->regularizationIters = ri; }

	LinearSolverType GetLinearSolverType() const { return this->linearSolverType; }
	void GetLinearSolverType(LinearSolverType st) { this->linearSolverType = st; }

	double GetMaxStepSize() const { return this->maxStepSize; }
	void SetMaxStepSize(double ms) { this->maxStepSize = ms; }

	// Last state

	bool GetHasLastPos() const { return this->hasLastPos; }
	const VectorXd& GetLastPos() const { return this->lastPos; }
	
	// Solving methods

	/**
	* Solve a linear step of the static equilibrium with the current boundary conditions.
	*/
	SolveResult SolveStep(FEM* fem);

	/**
	* Solves the full static equilibrium with the current boundary conditions.
	*/
	SolveResult SolveFull(FEM* fem);

	/**
	* Solve the linear system A*x = b, using the method currently configured.
	*/
	bool SolveLinearSystem(SparseMatrix<double>& mA, VectorXd& vb, VectorXd& vx);
	bool SolveLinearSystem_ConjugateGradient(SparseMatrix<double>& mA, VectorXd& vb, VectorXd& vx);
	bool SolveLinearSystem_SimplicialCholeski(SparseMatrix<double>& mA, VectorXd& vb, VectorXd& vx);

	// Compute energy/force/Jacobian

	double ComputeEnergy(FEM* fem) const;
	void ComputeForces(FEM* fem, VectorXd& vf) const;
	void ComputeJacobian(FEM* fem, SparseMatrix<double>& mH) const;

};
