//=============================================================================
//
//   Co-Rotational Finite Element Method source code
//   Based on Physics-Based Animation book, chapter 10
//
//   Based on original implementation by
//							Miguel A. Otaduy,    URJC Madrid
//							Alvaro G. Perez,     URJC Madrid
//							and Javier S. Zurdo, URJC Madrid
//
//	Authors:
//			Krisztian Amit Birkas, McGill Uni.
//			Jesus Perez Rodriguez, IST Austria
//
//=============================================================================

#pragma once

#include "CommonIncludes.h"

// Forward declarations

class FEM;

//////////////////////////////////////////////////////////////////////////////
// EnergyElement
//
// Description:
//		Base class for all energy elements. Encapsulates some basic
//		data and methods for the computation and temporal storage of 
//		energy, forces and Jacobians (mainly for parallelization).
//////////////////////////////////////////////////////////////////////////////
class EnergyElement
{
	///////////////////
	//// VARIABLES ////
	///////////////////
	//
	//

protected:
	double energy;
	VectorXd forces;
	MatrixXd jacobian;

	FEM* fem;

	/////////////////
	//// METHODS ////
	/////////////////
	//
	//

public:
	EnergyElement(FEM* fem);

	~EnergyElement(void) { }

	/**
	* Initializes this energy element. This method
	* will be called prior to any simulation. Place
	* here all precomputations that are not dependent
	* on the kinematic state (e.g. precompute K in FEM).
	*/
	virtual void Initialize() {};

	/**
	* Compute and locally store potential energy
	*/
	virtual void ComputeAndStore_Energy() {};

	/**
	* Compute and locally store element forces
	*/
	virtual void ComputeAndStore_Forces() {};

	/**
	* Compute and locally store element Jacobian
	*/
	virtual void ComputeAndStore_Jacobian() {};

	/**
	* Compute and locally store element forces and Jacobian together.
	*/
	virtual void ComputeAndStore_ForcesAndJacobian()
	{
		ComputeAndStore_Forces();
		ComputeAndStore_Jacobian();
	}

	/**
	* Add the contribution to the total forces corresponding to
	* this energy element. Each implementation is responsible of
	* assembling the forces properly depending on the supporting
	* geometry (e.g. CoRotFEM -> tetrahedron).
	*/
	virtual void AddForcesContribution(VectorXd& totalForces) {};

	/**
	* Add the contribution to the total Jacobian corresponding to
	* this energy element. Each implementation is responsible of
	* assembling the Jacobian properly depending on the supporting
	* geometry (e.g. CoRotFEM -> tetrahedron).
	*/
	virtual void AddJacobianContribution(VectorTd & totalJacobian) {};

	/**
	* Returns local element energy
	*/
	double GetElementEnergy() const { return this->energy; }

	/**
	* Returns local element forces (size depending on each energy)
	*/
	VectorXd GetElementForces() const { return this->forces; }

	/**
	* Returns local element forces (size depending on each energy)
	*/
	MatrixXd GetElementJacobian() const { return this->jacobian; }

};
