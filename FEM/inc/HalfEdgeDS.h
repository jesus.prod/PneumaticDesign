//=============================================================================
//
//   Co-Rotational Finite Element Method source code
//   Based on Physics-Based Animation book, chapter 10
//
//   Based on original implementation by
//							Miguel A. Otaduy,    URJC Madrid
//							Alvaro G. Perez,     URJC Madrid
//							and Javier S. Zurdo, URJC Madrid
//
//	Authors:
//			Krisztian Amit Birkas, McGill Uni.
//			Jesus Perez Rodriguez, IST Austria
//
//=============================================================================


#include <vector>
#include <map>
#include <Face.h>
#include <Node.h>

//////////////////////////////////////////////////////////////////////////////
// HalfEdgeDS
//
// Description:
//      HalfEdgeDS class for the chamber faces to improve pressure calculation
//////////////////////////////////////////////////////////////////////////////
class HalfEdgeDS
{
public:
	struct HalfEdge
	{
		// Index into the vertex array.
		long to_vertex;
		// Index into the face array.
		long face;
		// Index into the edges array.
		long edge;
		// Index into the halfedges array.
		long twin;
		// Index into the halfedges array.
		long next;

		HalfEdge() :
			to_vertex(-1),
			face(-1),
			edge(-1),
			twin(-1),
			next(-1)
		{}
	};

	// Builds the half-edge data structures from the given faces
	void build(std::vector<Face> &faces);

	/* Useful getters and converters */
	const HalfEdge& halfedge(const long i) const { return m_halfedges[i]; }
	long vertex_neighbourK(long i);
	long vertex_neighbourL(long i);
	long vertex_neighbourJ(long i);
	std::vector<HalfEdge> halfEdgesToNeighboursOfVertex(long vertex_index);


	const long faceToHalfEdge(const long i) const { return m_face_halfedges[i]; }

	long getNodeCount() { return nodeMap.size(); };
	long nodeIndexToID(const long index) { return nodeIndexToIDMap[index]; };
	long nodeIDToIndex(const long ID) { return nodeIDToIndexMap[ID]; };
	Node* getNodeAtID(const long ID) { return nodeMap[ID]; }

	/* Useful utility functions on HalfEdge DS */
	// Returns in 'result' all neighbours (as indices) of vertex_index
	void vertex_vertex_neighbors(const long vertex_index, std::vector<long>& result); 
	// Returns all neighbours (as indices) of vertex_index
	std::vector<long> vertex_vertex_neighbors(const long vertex_index);
	// Returns in 'result' all neighbouring faces (as indices) of vertex_index
	void vertex_face_neighbors(const long vertex_index, std::vector<long>& result);
	// Returns all neighbouring faces (as indices) of vertex_index
	std::vector<long> vertex_face_neighbors(const long vertex_index);
	// Returns the number of vertex neigbours (valence) of vertex_index
	int vertex_valence(const long vertex_index);

	// Returns whether vertex_index is on the boundary
	bool vertex_is_boundary(const long vertex_index);
	// Returns a list of boundary vertex indices
	std::vector<long> boundary_vertices() const;
	// Returns a list of undirected boundary edges. If (i,j) in list, then (j,i) won't be!
	std::vector<std::pair<long, long>> boundary_edges() const;

	// Returns directed edge corresponding to index of a 'halfedge_t'
	std::pair<long, long> he_index2directed_edge(const long he_index) const;
	// Returns index of the 'halfedge_t' corresponding to edge (i, j)
	long directed_edge2he_index(const long i, const long j) const;

private:
	/* Internal data structures START */
	struct Edge_heDS
	{
		long v[2];

		long& start() { return v[0]; }
		const long& start() const { return v[0]; }

		long& end() { return v[1]; }
		const long& end() const { return v[1]; }

		Edge_heDS()
		{
			v[0] = v[1] = -1;
		}
	};
	struct Face_heDS
	{
		long v[3];

		long& i() { return v[0]; }
		const long& i() const { return v[0]; }

		long& j() { return v[1]; }
		const long& j() const { return v[1]; }

		long& k() { return v[2]; }
		const long& k() const { return v[2]; }

		Face_heDS()
		{
			v[0] = v[1] = v[2] = -1;
		}
	};
	/* Internal data structures END */

	std::map<long, Node*> nodeMap; // maps node ID to the Node itself
	std::map<long, long> nodeIDToIndexMap; // maps "node ID" => "the internal ordered unique indexing of nodes"
	std::map<long, long> nodeIndexToIDMap; // maps "the internal ordered unique indexing of nodes" => "node ID"

	std::vector<HalfEdge> m_halfedges;

	std::vector<long> m_vertex_halfedges; // Offsets into the 'halfedges' sequence, one per vertex.
	std::vector<long> m_face_halfedges; // Offset into the 'halfedges' sequence, one per face.
	std::vector<long> m_edge_halfedges; // Offset into the 'halfedges' sequence, one per edge (unordered pair of vertex indices).
	std::map< std::pair<long, long>, long> m_directed_edge2he_index; // A map from an ordered edge (an std::pair of long's) to an offset into the 'halfedge' sequence.

	long directed_edge2face_index(const std::map< std::pair<long, long>, long>& de2fi, long vertex_i, long vertex_j);
	void unordered_edges_from_faces(const std::vector<Face_heDS>& triangles, std::vector<Edge_heDS>& edges_out);
	void clear();
};