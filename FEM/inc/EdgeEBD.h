#pragma once

//=============================================================================
//
//   Co-Rotational Finite Element Method source code
//   Based on Physics-Based Animation book, chapter 10
//
//   Based on original implementation by
//							Miguel A. Otaduy,    URJC Madrid
//							Alvaro G. Perez,     URJC Madrid
//							and Javier S. Zurdo, URJC Madrid
//
//	Authors:
//			Krisztian Amit Birkas, McGill Uni.
//			Jesus Perez Rodriguez, IST Austria
//
//=============================================================================

#pragma once

#include "CommonIncludes.h"

// Foward declarations
class NodeEBD;

//////////////////////////////////////////////////////////////////////////////
// Edge
//
// Description:
//      EdgeEBD class for the tetrahedra mesh of OptimizedFEM
//////////////////////////////////////////////////////////////////////////////
class EdgeEBD
{

private:
	NodeEBD* origin; // Origin node of this edge
	NodeEBD* head;   // Head node of this edge

public:
	EdgeEBD(NodeEBD* node1, NodeEBD* node2)
	{
		origin = node1;
		head = node2;
	}

	~EdgeEBD(void) { }

	// Get/Set Methods
	NodeEBD* GetOrigin(void) const { return origin; }

	void SetOrigin(NodeEBD* node) { origin = node; }

	NodeEBD* GetHead(void) const { return head; }

	void SetHead(NodeEBD* node) { head = node; }

	bool Equals(EdgeEBD* edge) {
		return (
			(this->origin->GetId() == edge->GetOrigin()->GetId() && this->head->GetId() == edge->GetHead()->GetId())
			||
			(this->head->GetId() == edge->GetOrigin()->GetId() && this->origin->GetId() == edge->GetHead()->GetId())
			);
	}

};
