//=============================================================================
//
//   Co-Rotational Finite Element Method source code
//   Based on Physics-Based Animation book, chapter 10
//
//   Based on original implementation by
//							Miguel A. Otaduy,    URJC Madrid
//							Alvaro G. Perez,     URJC Madrid
//							and Javier S. Zurdo, URJC Madrid
//
//	Authors:
//			Krisztian Amit Birkas, McGill Uni.
//			Jesus Perez Rodriguez, IST Austria
//
//=============================================================================

#pragma once

#include "CommonIncludes.h"
#include "EnergyElement.h"

// Forward declarations

class FEM;

class Tetrahedron;

//////////////////////////////////////////////////////////////////////////////
// EnergyElement_Pressure
//
// Description:
//		Energy element corresponding to pressure
//////////////////////////////////////////////////////////////////////////////
class EnergyElement_Pressure : public EnergyElement
{
	///////////////////
	//// VARIABLES ////
	///////////////////
	//
	//

private:
	Tetrahedron* tetrahedron;

	double pressureConstant;

	double pressureValue;
	double volume;
	
	double springConstant;
	long numOfNodes; 

	MatrixXd densePart;
	MatrixXd sparsePart;

	void FiniteDifferenceForceCheck(double step = std::pow(10, -3));
	void FiniteDifferenceJacobianCheck(double step = std::pow(10, -3));
	double CalculateEnergy(double springConstant, double volumeDistance);
	void ComputeJ(Eigen::VectorXd &J);
	void ComputeGradJ(Eigen::MatrixXd& gradJ);
	void printValues(Eigen::VectorXd forces, string name);
	void printValues(Eigen::MatrixXd forces, string name);
protected:


	/////////////////
	//// METHODS ////
	/////////////////
	//
	//

public:

	EnergyElement_Pressure(FEM* fem, double targetVolume, double springConstant);

	~EnergyElement_Pressure(void) { }

	virtual void Initialize();
	virtual void ComputeAndStore_ForcesAndJacobian();
	virtual void ComputeAndStore_Energy();
	virtual void AddForcesContribution(VectorXd& totalForces);
	virtual void AddJacobianContribution(VectorTd & totalJacobian);

	void ComputeAndStore_ForcesAndJacobianSimple();
};