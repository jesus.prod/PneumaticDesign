#include "HalfEdgeDS.h"

// needed for implementation
#include <set>
#include <iostream>
#include <algorithm>
#include <assert.h>

std::vector<HalfEdgeDS::HalfEdge> HalfEdgeDS::halfEdgesToNeighboursOfVertex(long vertex_index) {
	std::vector<HalfEdge> halfedges;

	long start_hei = m_vertex_halfedges[vertex_index];
	long hei = start_hei;
	while (true)
	{
		const HalfEdge& he = m_halfedges[hei];
		halfedges.push_back(he);

		hei = m_halfedges[he.twin].next;
		if (hei == start_hei) break;
	}

	return halfedges;
}

long HalfEdgeDS::vertex_neighbourK(long i) {
	HalfEdge he = this->m_halfedges[this->m_vertex_halfedges[i]];
	return this->m_halfedges[he.next].to_vertex;
}

long HalfEdgeDS::vertex_neighbourL(long i) {
	HalfEdge he = this->m_halfedges[this->m_vertex_halfedges[i]];
	return this->m_halfedges[this->m_halfedges[he.twin].next].to_vertex;
}

long HalfEdgeDS::vertex_neighbourJ(long i) {
	HalfEdge he = this->m_halfedges[this->m_vertex_halfedges[i]];
	return he.to_vertex;
}

void HalfEdgeDS::build(std::vector<Face> &facesFEM)
{
	for (int i = 0; i < facesFEM.size(); ++i) {
		this->nodeMap[facesFEM[i].n1->GetId()] = facesFEM[i].n1;
		this->nodeMap[facesFEM[i].n2->GetId()] = facesFEM[i].n2;
		this->nodeMap[facesFEM[i].n3->GetId()] = facesFEM[i].n3;
	}

	size_t IDMapping = 0;
	for (std::map<long, Node*>::iterator it = nodeMap.begin(); it != nodeMap.end(); ++it) {
		std::map<long, long>::iterator itIDtoIndex = this->nodeIDToIndexMap.find(it->first);
		if (itIDtoIndex == nodeIDToIndexMap.end()) {
			this->nodeIDToIndexMap[it->first] = IDMapping;
			this->nodeIndexToIDMap[IDMapping] = it->first;

			++IDMapping;
		}
	}

	std::vector<Face_heDS> faces(facesFEM.size());
	for (int i = 0; i < facesFEM.size(); ++i) {
		faces[i].v[0] = this->nodeIDToIndexMap[facesFEM[i].n1->GetId()];
		faces[i].v[1] = this->nodeIDToIndexMap[facesFEM[i].n2->GetId()];
		faces[i].v[2] = this->nodeIDToIndexMap[facesFEM[i].n3->GetId()];
	}
	const unsigned long num_faces = faces.size();

	std::vector<HalfEdgeDS::Edge_heDS> edges;
	unordered_edges_from_faces(faces, edges);
	const unsigned long num_edges = edges.size();
	const unsigned long num_vertices = nodeMap.size();

	std::map<std::pair<long, long>, long> directedEdgeToFaceIndex;
	for (int fi = 0; fi < num_faces; ++fi)
	{
		const Face_heDS& tri = faces[fi];
		directedEdgeToFaceIndex[std::make_pair(tri.v[0], tri.v[1])] = fi;
		directedEdgeToFaceIndex[std::make_pair(tri.v[1], tri.v[2])] = fi;
		directedEdgeToFaceIndex[std::make_pair(tri.v[2], tri.v[0])] = fi;
	}

	clear();
	m_vertex_halfedges.resize(num_vertices, -1);
	m_face_halfedges.resize(num_faces, -1);
	m_edge_halfedges.resize(num_edges, -1);
	m_halfedges.reserve(num_edges * 2);

	for (int ei = 0; ei < num_edges; ++ei)
	{
		const Edge_heDS& edge = edges[ei];

		// Add the HalfEdge structures to the end of the list.
		const long he0index = m_halfedges.size();
		m_halfedges.push_back(HalfEdge());
		HalfEdge& he0 = m_halfedges.back();

		const long he1index = m_halfedges.size();
		m_halfedges.push_back(HalfEdge());
		HalfEdge& he1 = m_halfedges.back();

		// The face will be -1 if it is a boundary half-edge.
		he0.face = directed_edge2face_index(directedEdgeToFaceIndex, edge.v[0], edge.v[1]);
		he0.to_vertex = edge.v[1];
		he0.edge = ei;

		// The face will be -1 if it is a boundary half-edge.
		he1.face = directed_edge2face_index(directedEdgeToFaceIndex, edge.v[1], edge.v[0]);
		he1.to_vertex = edge.v[0];
		he1.edge = ei;

		// Store the opposite half-edge index.
		he0.twin = he1index;
		he1.twin = he0index;

		// Also store the index in our m_directed_edge2he_index map.
		assert(m_directed_edge2he_index.find(std::make_pair(edge.v[0], edge.v[1])) == m_directed_edge2he_index.end());
		assert(m_directed_edge2he_index.find(std::make_pair(edge.v[1], edge.v[0])) == m_directed_edge2he_index.end());
		m_directed_edge2he_index[std::make_pair(edge.v[0], edge.v[1])] = he0index;
		m_directed_edge2he_index[std::make_pair(edge.v[1], edge.v[0])] = he1index;

		// If the vertex pointed to by a half-edge doesn't yet have an out-going
		// halfedge, store the opposite halfedge.
		// Also, if the vertex is a boundary vertex, make sure its
		// out-going halfedge is a boundary halfedge.
		// NOTE: Halfedge data structure can't properly handle butterfly vertices.
		//       If the mesh has butterfly vertices, there will be multiple outgoing
		//       boundary halfedges.  Because we have to pick one as the vertex's outgoing
		//       halfedge, we can't iterate over all neighbors, only a single wing of the
		//       butterfly.
		if (m_vertex_halfedges[he0.to_vertex] == -1 || -1 == he1.face)
		{
			m_vertex_halfedges[he0.to_vertex] = he0.twin;
		}
		if (m_vertex_halfedges[he1.to_vertex] == -1 || -1 == he0.face)
		{
			m_vertex_halfedges[he1.to_vertex] = he1.twin;
		}

		// If the face pointed to by a half-edge doesn't yet have a
		// halfedge pointing to it, store the halfedge.
		if (-1 != he0.face && m_face_halfedges[he0.face] == -1)
		{
			m_face_halfedges[he0.face] = he0index;
		}
		if (-1 != he1.face && m_face_halfedges[he1.face] == -1)
		{
			m_face_halfedges[he1.face] = he1index;
		}

		// Store one of the half-edges for the edge.
		assert(m_edge_halfedges[ei] == -1);
		m_edge_halfedges[ei] = he0index;
	}

	// Now that all the half-edges are created, set the remaining next_he field.
	// We can't yet handle boundary halfedges, so store them for later.
	std::vector< long > boundary_heis;
	for (int hei = 0; hei < m_halfedges.size(); ++hei)
	{
		HalfEdge& he = m_halfedges.at(hei);
		// Store boundary halfedges for later.
		if (-1 == he.face)
		{
			boundary_heis.push_back(hei);
			continue;
		}

		const Face_heDS& face = faces[he.face];
		const long i = he.to_vertex;

		long j = -1;
		if (face.v[0] == i) j = face.v[1];
		else if (face.v[1] == i) j = face.v[2];
		else if (face.v[2] == i) j = face.v[0];
		assert(-1 != j);

		he.next = m_directed_edge2he_index[std::make_pair(i, j)];
	}

	// Make a map from vertices to boundary halfedges (indices) originating from them.
	// NOTE: There will only be multiple originating boundary halfedges at butterfly vertices.
	std::map< long, std::set< long > > vertex2outgoing_boundary_hei;
	for (std::vector< long >::const_iterator hei = boundary_heis.begin(); hei != boundary_heis.end(); ++hei)
	{
		const long originating_vertex = m_halfedges[m_halfedges[*hei].twin].to_vertex;
		vertex2outgoing_boundary_hei[originating_vertex].insert(*hei);
		if (vertex2outgoing_boundary_hei[originating_vertex].size() > 1)
		{
			std::cerr << "Butterfly vertex encountered.\n";
		}
	}

	// For each boundary halfedge, make its next_he one of the boundary halfedges
	// originating at its to_vertex.
	for (std::vector< long >::const_iterator hei = boundary_heis.begin(); hei != boundary_heis.end(); ++hei)
	{
		HalfEdge& he = m_halfedges[*hei];

		std::set< long >& outgoing = vertex2outgoing_boundary_hei[he.to_vertex];
		if (!outgoing.empty())
		{
			std::set< long >::iterator outgoing_hei = outgoing.begin();
			he.next = *outgoing_hei;

			outgoing.erase(outgoing_hei);
		}
	}

#ifndef NDEBUG
	for (std::map< long, std::set< long > >::const_iterator it = vertex2outgoing_boundary_hei.begin(); it != vertex2outgoing_boundary_hei.end(); ++it)
	{
		assert(it->second.empty());
	}
#endif
}

void HalfEdgeDS::vertex_vertex_neighbors(const long vertex_index, std::vector< long >& result)
{
	result.clear();

	long start_hei = m_vertex_halfedges[vertex_index];
	long hei = start_hei;
	while (true)
	{
		const HalfEdge& he = m_halfedges[hei];
		result.push_back(he.to_vertex);

		hei = m_halfedges[he.twin].next;
		if (hei == start_hei) break;
	}
}

std::vector<long> HalfEdgeDS::vertex_vertex_neighbors(const long vertex_index)
{
	std::vector<long> result;
	vertex_vertex_neighbors(vertex_index, result);
	return result;
}

void HalfEdgeDS::vertex_face_neighbors(const long vertex_index, std::vector< long >& result)
{
	result.clear();

	const long start_hei = m_vertex_halfedges[vertex_index];
	long hei = start_hei;
	while (true)
	{
		const HalfEdge& he = m_halfedges[hei];
		if (-1 != he.face) result.push_back(he.face);

		hei = m_halfedges[he.twin].next;
		if (hei == start_hei) break;
	}
}

std::vector<long> HalfEdgeDS::vertex_face_neighbors(const long vertex_index)
{
	std::vector<long> result;
	vertex_face_neighbors(vertex_index, result);
	return result;
}

int HalfEdgeDS::vertex_valence(const long vertex_index)
{
	std::vector<long> neighbors;
	vertex_vertex_neighbors(vertex_index, neighbors);
	return neighbors.size();
}

bool HalfEdgeDS::vertex_is_boundary(const long vertex_index)
{
	return -1 == m_halfedges[m_vertex_halfedges[vertex_index]].face;
}

std::vector<long> HalfEdgeDS::boundary_vertices() const
{
	std::set< long > result;
	for (int hei = 0; hei < m_halfedges.size(); ++hei)
	{
		const HalfEdge& he = m_halfedges[hei];

		if (-1 == he.face)
		{
			// result.extend( self.he_index2directed_edge( hei ) )
			result.insert(he.to_vertex);
			result.insert(m_halfedges[he.twin].to_vertex);
		}
	}

	return std::vector< long >(result.begin(), result.end());
}

std::vector<std::pair<long, long>> HalfEdgeDS::boundary_edges() const
{
	std::vector< std::pair< long, long > > result;
	for (int hei = 0; hei < m_halfedges.size(); ++hei)
	{
		const HalfEdge& he = m_halfedges[hei];

		if (he.face == -1)
		{
			result.push_back(he_index2directed_edge(hei));
		}
	}

	return result;
}

std::pair< long, long > HalfEdgeDS::he_index2directed_edge(const long he_index) const
{
	const HalfEdge& he = m_halfedges[he_index];
	return std::make_pair(m_halfedges[he.twin].to_vertex, he.to_vertex);
}

long HalfEdgeDS::directed_edge2he_index(const long i, const long j) const
{
	/// This isn't const,'t handle the case where (i,j) isn't known:
	// return m_directed_edge2he_index[ std::make_pair( i,j ) ];

	std::map< std::pair< long, long >, long >::const_iterator result = m_directed_edge2he_index.find(std::make_pair(i, j));
	if (result == m_directed_edge2he_index.end()) return -1;

	return result->second;
}

long HalfEdgeDS::directed_edge2face_index(const std::map< std::pair<long, long>, long>& de2fi, long vertex_i, long vertex_j)
{
	std::map< std::pair<long, long>, long>::const_iterator it = de2fi.find(std::make_pair(vertex_i, vertex_j));

	// If no such directed edge exists, then there's no such face in the mesh.
	// The edge must be a boundary edge.
	// In this case, the reverse orientation edge must have a face.
	if (it == de2fi.end())
	{
		assert(de2fi.find(std::make_pair(vertex_j, vertex_i)) != de2fi.end());
		return -1;
	}

	return it->second;
}

void HalfEdgeDS::unordered_edges_from_faces(const std::vector<HalfEdgeDS::Face_heDS>& faces, std::vector< HalfEdgeDS::Edge_heDS >& edges_out)
{
	std::set<std::pair<long, long>> edges;
	for (int t = 0; t < faces.size(); ++t)
	{
		edges.insert(std::make_pair(std::min(faces[t].i(), faces[t].j()), std::max(faces[t].i(), faces[t].j())));
		edges.insert(std::make_pair(std::min(faces[t].j(), faces[t].k()), std::max(faces[t].j(), faces[t].k())));
		edges.insert(std::make_pair(std::min(faces[t].k(), faces[t].i()), std::max(faces[t].k(), faces[t].i())));
	}

	edges_out.resize(edges.size());
	int e = 0;
	for (std::set<std::pair<long, long>>::const_iterator it = edges.begin(); it != edges.end(); ++it, ++e)
	{
		edges_out.at(e).start() = it->first;
		edges_out.at(e).end() = it->second;
	}
}

void HalfEdgeDS::clear()
{
	m_halfedges.clear();
	m_vertex_halfedges.clear();
	m_face_halfedges.clear();
	m_edge_halfedges.clear();
	m_directed_edge2he_index.clear();
}
