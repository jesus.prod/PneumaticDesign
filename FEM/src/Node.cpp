//=============================================================================
//
//   Finite Element Method source code
//   Based on Physics-Based Animation book, chapter 10
//
//
//   Copyright (C) 2011 by Miguel A. Otaduy,    URJC Madrid
//                         Alvaro G. Perez,     URJC Madrid
//                         and Javier S. Zurdo, URJC Madrid
//
//=============================================================================

#include "Node.h"
