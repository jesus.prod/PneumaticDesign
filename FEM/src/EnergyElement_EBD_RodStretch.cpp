//=============================================================================
//
//   Co-Rotational Finite Element Method source code
//   Based on Physics-Based Animation book, chapter 10
//
//   Based on original implementation by
//							Miguel A. Otaduy,    URJC Madrid
//							Alvaro G. Perez,     URJC Madrid
//							and Javier S. Zurdo, URJC Madrid
//
//	Authors:
//			Krisztian Amit Birkas, McGill Uni.
//			Jesus Perez Rodriguez, IST Austria
//
//=============================================================================

#include "EnergyElement_EBD_RodStretch.h"

#include "FEM.h"

#include "NodeEBD.h"

EnergyElement_EBD_RodStretch::EnergyElement_EBD_RodStretch(FEM* fem, const vector<NodeEBD*>& vnodes) : EnergyElement(fem)
{
	energy = 0;
	forces = VectorXd(18);
	jacobian = MatrixXd(18, 18);

	this->vnodes = vnodes;
}

void EnergyElement_EBD_RodStretch::Initialize()
{
	//// Precompute derivative matrix

	//int nodeNum = 0;
	//int nodeCount = 0;

	//for (int i = 0; i < (int) this->vnodes.size(); ++i)
	//	nodeNum += (int) this->vnodes[i]->getNodes().size();

	//this->mDeDx = MatrixXd(6, 3*nodeNum);
	//this->mDeDx.setZero();

	//for (int i = 0; i < (int) this->vnodes.size(); ++i)
	//{
	//	const vector<Node*>& vnodes = this->vnodes[i]->getNodes();
	//	const vector<double>& vcoords = this->vnodes[i]->getCoords();
	//	for (int j = 0; j < (int) vnodes.size(); ++j)
	//	{
	//		this->mDeDx.block<3, 3>(0, 3*nodeCount) = Matrix3d::Identity()*vcoords[j];
	//		nodeCount++;
	//	}
	//}

	this->ks = this->fem->GetEBDStretchK();
}

void EnergyElement_EBD_RodStretch::ComputeAndStore_Energy()
{
	// Get rest nodes

	Vector3d a0 = this->vnodes[0]->ComputePosition0();
	Vector3d b0 = this->vnodes[1]->ComputePosition0();

	// Get defo nodes

	Vector3d ax = this->vnodes[0]->ComputePosition();
	Vector3d bx = this->vnodes[1]->ComputePosition();

	// Compute lengths

	double L0 = (b0 - a0).norm();
	double Lx = (bx - ax).norm();

	// Strain

	double s = (Lx / L0 - 1);

	// Energy

	this->energy = 0.5*this->ks*L0*s*s;
}

void EnergyElement_EBD_RodStretch::ComputeAndStore_ForcesAndJacobian()
{
	// Get rest nodes

	Vector3d a0 = this->vnodes[0]->ComputePosition0();
	Vector3d b0 = this->vnodes[1]->ComputePosition0();

	// Get defo nodes

	Vector3d ax = this->vnodes[0]->ComputePosition();
	Vector3d bx = this->vnodes[1]->ComputePosition();

	// Compute lengths

	double L0 = (b0 - a0).norm();
	double Lx = (bx - ax).norm();

	// Strain

	double s = (Lx / L0 - 1);

	// Forces

	Vector3d ub = (bx - ax)/Lx;
	Vector3d fb = -this->ks*s*ub;
	this->forces.block<3,1>(0, 0) = -fb;
	this->forces.block<3,1>(3, 0) = fb;

	// Jacobian

	double L0inv = 1.0 / L0;
	double Lxinv = 1.0 / Lx;
	Matrix3d mI = Matrix3d::Identity();
	Matrix3d ububT = ub*ub.transpose();
	Matrix3d DfbDb = -this->ks*(L0inv*ububT + s*((mI - ububT)*Lxinv));
	this->jacobian.block<3, 3>(0, 0) = DfbDb;
	this->jacobian.block<3, 3>(3, 3) = DfbDb;
	this->jacobian.block<3, 3>(3, 0) = -DfbDb;
	this->jacobian.block<3, 3>(0, 3) = -DfbDb;
}

void EnergyElement_EBD_RodStretch::AddForcesContribution(VectorXd& totalForces)
{
	for (int i = 0; i < 2; ++i)
	{
		Vector3d nodeFor = forces.block<3, 1>(3*i, 0);

		for (int j = 0; j < 4; ++j)
		{
			Node* node = this->vnodes[i]->GetNodes()[j];

			int freeId = node->GetId_Free();

			// Is simulated?
			if (freeId < 0)
				continue;

			totalForces.block<3, 1>(3*freeId, 0) += nodeFor*this->vnodes[i]->GetCoords()[j];
		}
	}
}

void EnergyElement_EBD_RodStretch::AddJacobianContribution(VectorTd & totalJacobian)
{
	for (int i0 = 0; i0 < 2; ++i0)
	{
		for (int i1 = 0; i1 < 2; ++i1)
		{
			Matrix3d nodesJac = jacobian.block<3, 3>(3*i0, 3*i1);

			for (int j0 = 0; j0 < 4; ++j0)
			{
				Node* node0 = this->vnodes[i0]->GetNodes()[j0];
				int freeId0 = node0->GetId_Free();
				if (freeId0 < 0)
					continue;

				for (int j1 = 0; j1 < 4; ++j1)
				{
					Node* node1 = this->vnodes[i1]->GetNodes()[j1];
					int freeId1 = node1->GetId_Free();
					if (freeId1 < 0)
						continue;

					// Is upper triangular?
					if (freeId0 < freeId1)
						continue;

					double coord0 = this->vnodes[i0]->GetCoords()[j0];
					double coord1 = this->vnodes[i1]->GetCoords()[j1];
					Matrix3d nodesJacWeighted = coord0*nodesJac*coord1;
					AssembleSparseMatrix(totalJacobian, 3*freeId0, 3*freeId1, nodesJacWeighted);
				}
			}
		}
	}
}
