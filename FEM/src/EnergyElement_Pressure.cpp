#include "EnergyElement_Pressure.h"
#include "FEM.h"

EnergyElement_Pressure::EnergyElement_Pressure(FEM* fem, double targetVolume, double springConstant) : EnergyElement(fem) {
	//energy = 0;
	//forces = VectorXd(12);
	//jacobian = MatrixXd(12, 12);

	this->fem->SetTargetVolume(Face::computeVolume(this->fem->chamberFaces));
	this->volume = Face::computeVolume(this->fem->chamberFaces);

	this->springConstant = 5e7;
	this->pressureConstant = 1;

	this->numOfNodes = this->fem->halfEdgeDS_mesh.getNodeCount();
}

void EnergyElement_Pressure::Initialize() {
	this->volume = Face::computeVolume(this->fem->chamberFaces);
	this->pressureValue = this->springConstant * (this->volume - this->fem->GetTargetVolume());
}

void EnergyElement_Pressure::ComputeAndStore_Energy() {
	this->volume = Face::computeVolume(this->fem->chamberFaces);
	double volumeDistance = this->volume - this->fem->GetTargetVolume(); 
	this->energy = this->CalculateEnergy(this->springConstant, volumeDistance);
}

void EnergyElement_Pressure::ComputeAndStore_ForcesAndJacobian() {
	this->volume = Face::computeVolume(this->fem->chamberFaces);
	std::cout << "*** Volume of chamber: " << this->volume << std::endl;
	double volumeDistance = this->volume - this->fem->GetTargetVolume();

	Eigen::VectorXd J(this->numOfNodes * 3); // J = dV over dx
	J.setZero();
	ComputeJ(J);

	this->forces = this->springConstant*volumeDistance*J;
	//FiniteDifferenceForceCheck(); // Finite Difference

	Eigen::MatrixXd JJ_t = (J * J.transpose());
	this->densePart = this->springConstant*JJ_t;

	Eigen::MatrixXd gradJ(JJ_t.cols(), JJ_t.rows()); // square matrix, just like JJ_t
	gradJ.setZero();
	ComputeGradJ(gradJ);
	this->sparsePart = (1.0/6.0) * this->springConstant*(volumeDistance)*gradJ;

	//this->sparsePart.setZero();
	//this->densePart.setZero();
	this->jacobian = this->densePart + this->sparsePart; // sparse part + dense part -> dense part is not that impactful -> can be removed
	//FiniteDifferenceJacobianCheck(); // Finite Difference
}

void EnergyElement_Pressure::FiniteDifferenceForceCheck(double stepSize) {
	Eigen::VectorXd FD_forceCheck(this->numOfNodes * 3);
	FD_forceCheck.setZero();

	auto FD = [this](Node* n1, Vector3d originalPosition, Vector3d step) {
		n1->SetPosition(originalPosition + step);
		double volume1 = Face::computeVolume(this->fem->chamberFaces);
		double energy1 = this->CalculateEnergy(this->springConstant, volume1 - this->fem->GetTargetVolume());

		n1->SetPosition(originalPosition - step);
		double volume2 = Face::computeVolume(this->fem->chamberFaces);
		double energy2 = this->CalculateEnergy(this->springConstant, volume2 - this->fem->GetTargetVolume());

		return (energy2 - energy1) / (step.norm() * 2.0);
	};

	double step = stepSize;
	for (int node_i = 0; node_i < this->numOfNodes; ++node_i) {
		Node* n1 = this->fem->halfEdgeDS_mesh.getNodeAtID(this->fem->halfEdgeDS_mesh.nodeIndexToID(node_i));
		const Vector3d originalPosition = n1->GetPosition();

		/*
		{   // x+step, x-step -> forces[i*3]   <- x deriv
			n1->SetPosition(originalPosition + Vector3d(step, 0, 0));
			double volume1 = Face::computeVolume(this->fem->chamberFaces);
			double energy1 = this->CalculateEnergy(this->springConstant, volume1 - targetVolume);

			n1->SetPosition(originalPosition + Vector3d(-step, 0, 0));
			double volume2 = Face::computeVolume(this->fem->chamberFaces);
			double energy2 = this->CalculateEnergy(this->springConstant, volume2 - targetVolume);

			double FD_x = (energy2 - energy1) / (step * 2.0);
			FD_forceCheck(node_i * 3 + 0) = FD_x;

			double check = FD(this, this->fem, n1, originalPosition, Vector3d(step, 0, 0), targetVolume);
			double a = FD_x - check;
			if (a < 0.0000000001 && a > 0-0.0000000001) {

			}
			else {
				std::cout << "ERRRRRRRRRRRRROOOOOOOOOOOOOOOOOOOORRRRRRRRRRRRRRRRRRRRRRRRRRRR" << std::endl;
			}
		}

		{   // y+step, y-step -> forces[i*3+1] <- y deriv
			n1->SetPosition(originalPosition + Vector3d(0, step, 0));
			double volume1 = Face::computeVolume(this->fem->chamberFaces);
			double energy1 = this->CalculateEnergy(this->springConstant, volume1 - targetVolume);

			n1->SetPosition(originalPosition + Vector3d(0, -step, 0));
			double volume2 = Face::computeVolume(this->fem->chamberFaces);
			double energy2 = this->CalculateEnergy(this->springConstant, volume2 - targetVolume);

			double FD_y = (energy2 - energy1) / (step * 2.0);
			FD_forceCheck(node_i * 3 + 1) = FD_y;

			double check = FD(this, this->fem, n1, originalPosition, Vector3d(0, step, 0), targetVolume);
			double a = FD_y - check;
			if (a < 0.0000000001 && a > 0 - 0.0000000001) {

			}
			else {
				std::cout << "ERRRRRRRRRRRRROOOOOOOOOOOOOOOOOOOORRRRRRRRRRRRRRRRRRRRRRRRRRRR" << std::endl;
			}
		}

		{   // z+step, z-step -> forces[i*3+2] <- z deriv
			n1->SetPosition(originalPosition + Vector3d(0, 0, step));
			double volume1 = Face::computeVolume(this->fem->chamberFaces);
			double energy1 = this->CalculateEnergy(this->springConstant, volume1 - targetVolume);

			n1->SetPosition(originalPosition + Vector3d(0, 0, -step));
			double volume2 = Face::computeVolume(this->fem->chamberFaces);
			double energy2 = this->CalculateEnergy(this->springConstant, volume2 - targetVolume);

			double FD_z = (energy2 - energy1) / (step * 2.0);
			FD_forceCheck(node_i * 3 + 2) = FD_z;

			double check = FD(this, this->fem, n1, originalPosition, Vector3d(0, 0, step), targetVolume);
			double a = FD_z - check;
			if (a < 0.0000000001 && a > 0 - 0.0000000001) {

			}
			else {
				std::cout << "ERRRRRRRRRRRRROOOOOOOOOOOOOOOOOOOORRRRRRRRRRRRRRRRRRRRRRRRRRRR" << std::endl;
			}
		}
		*/

		FD_forceCheck(node_i * 3 + 0) = FD(n1, originalPosition, Vector3d(step, 0, 0));
		FD_forceCheck(node_i * 3 + 1) = FD(n1, originalPosition, Vector3d(0, step, 0));
		FD_forceCheck(node_i * 3 + 2) = FD(n1, originalPosition, Vector3d(0, 0, step));

		n1->SetPosition(originalPosition);
	}

	printValues(this->forces, "J");
	printValues(FD_forceCheck, "FD");

	Eigen::VectorXd relativeDifference = (FD_forceCheck - this->forces) / this->forces.norm();
	cout << "Relative Difference norm (FD forces <-> forces): " << relativeDifference.norm() << endl;
}

void EnergyElement_Pressure::ComputeGradJ(Eigen::MatrixXd& gradJ) {
	gradJ.setZero();

	for (long index_i = 0; index_i < this->numOfNodes; ++index_i) {
		std::vector<HalfEdgeDS::HalfEdge> hes = fem->halfEdgeDS_mesh.halfEdgesToNeighboursOfVertex(index_i);
		for (int i = 0; i < hes.size(); ++i) {
			int index_j = hes[i].to_vertex;
			int index_k = (i == 0) ? hes[hes.size() - 1].to_vertex : hes[i - 1].to_vertex;
			int index_l = (i == hes.size() - 1) ? hes[0].to_vertex : hes[i + 1].to_vertex;

			/* // checking correctness: equivalent but different way of getting indices.
			int test_index_j = hes[i].to_vertex;
			int test_index_k = fem->halfEdgeDS_mesh.halfedge(hes[i].next).to_vertex;
			int test_index_l = fem->halfEdgeDS_mesh.halfedge(fem->halfEdgeDS_mesh.halfedge(hes[i].twin).next).to_vertex;
			*/
			Node* k = fem->halfEdgeDS_mesh.getNodeAtID(fem->halfEdgeDS_mesh.nodeIndexToID(index_k));
			Node* l = fem->halfEdgeDS_mesh.getNodeAtID(fem->halfEdgeDS_mesh.nodeIndexToID(index_l));

			Eigen::Vector3d kl = l->GetPosition() - k->GetPosition();

			// cross product matrix form
			gradJ(index_i * 3, index_j * 3) = 0;
			gradJ(index_i * 3, index_j * 3 + 1) = -kl.z();
			gradJ(index_i * 3, index_j * 3 + 2) = kl.y();

			gradJ(index_i * 3 + 1, index_j * 3) = kl.z();
			gradJ(index_i * 3 + 1, index_j * 3 + 1) = 0;
			gradJ(index_i * 3 + 1, index_j * 3 + 2) = -kl.x();

			gradJ(index_i * 3 + 2, index_j * 3) = -kl.y();
			gradJ(index_i * 3 + 2, index_j * 3 + 1) = kl.x();
			gradJ(index_i * 3 + 2, index_j * 3 + 2) = 0;
		}
	}
}

void EnergyElement_Pressure::ComputeJ(Eigen::VectorXd &J) {
	// Checked values by hand on simple tetrahedron - passed
	J.setZero();
	for (const Face& f : this->fem->chamberFaces) {
		double area = f.computeArea();
		Vector3d normal = f.computeNormal();
		normal.normalize();
		Eigen::Vector3d F = -(1.0/3.0) * area * normal;

		long n1_ID = f.n1->GetId();
		long n2_ID = f.n2->GetId();
		long n3_ID = f.n3->GetId();

		long n1_i = this->fem->halfEdgeDS_mesh.nodeIDToIndex(n1_ID);
		long n2_i = this->fem->halfEdgeDS_mesh.nodeIDToIndex(n2_ID);
		long n3_i = this->fem->halfEdgeDS_mesh.nodeIDToIndex(n3_ID);

		J[n1_i * 3 + 0] += F.x();
		J[n1_i * 3 + 1] += F.y();
		J[n1_i * 3 + 2] += F.z();

		J[n2_i * 3 + 0] += F.x();
		J[n2_i * 3 + 1] += F.y();
		J[n2_i * 3 + 2] += F.z();

		J[n3_i * 3 + 0] += F.x();
		J[n3_i * 3 + 1] += F.y();
		J[n3_i * 3 + 2] += F.z();
	}

	// Equivalent way by halfedge ds
	//Eigen::VectorXd J2 = J;
	//J.setZero();
	//for (long node_i = 0; node_i < this->numOfNodes; ++node_i)
	//{
	//	std::vector<long> neighbourFaces = fem->halfEdgeDS_mesh.vertex_face_neighbors(node_i);
	//	for (int i = 0; i < neighbourFaces.size(); ++i)
	//	{
	//		HalfEdgeDS::HalfEdge he1 = this->fem->halfEdgeDS_mesh.halfedge(this->fem->halfEdgeDS_mesh.faceToHalfEdge(neighbourFaces[i]));
	//		HalfEdgeDS::HalfEdge he2 = this->fem->halfEdgeDS_mesh.halfedge(he1.next);
	//		HalfEdgeDS::HalfEdge he3 = this->fem->halfEdgeDS_mesh.halfedge(he2.next);
	//		long n1_ID = this->fem->halfEdgeDS_mesh.nodeIndexToID(he1.to_vertex);
	//		long n2_ID = this->fem->halfEdgeDS_mesh.nodeIndexToID(he2.to_vertex);
	//		long n3_ID = this->fem->halfEdgeDS_mesh.nodeIndexToID(he3.to_vertex);
    //
	//		double area = Face::computeArea(this->fem->halfEdgeDS_mesh.getNodeAtID(n1_ID), this->fem->halfEdgeDS_mesh.getNodeAtID(n2_ID), this->fem->halfEdgeDS_mesh.getNodeAtID(n3_ID));
    //
	//		Vector3d normal = Face::computeNormal(this->fem->halfEdgeDS_mesh.getNodeAtID(n1_ID), this->fem->halfEdgeDS_mesh.getNodeAtID(n2_ID), this->fem->halfEdgeDS_mesh.getNodeAtID(n3_ID));
	//		normal.normalize();
    //
	//		Eigen::Vector3d F = ((-1.0 / 3.0)*area) * normal;
	//		J[node_i * 3] += F.x();
	//		J[node_i * 3 + 1] += F.y();
	//		J[node_i * 3 + 2] += F.z();
	//	}
	//}
}

double EnergyElement_Pressure::CalculateEnergy(double springConstant, double volumeDistance) {
	return 0.5 * springConstant * std::pow(volumeDistance, 2);
}



void EnergyElement_Pressure::FiniteDifferenceJacobianCheck(double stepSize) {
	Eigen::MatrixXd FD_gradJ(this->jacobian.cols(), this->jacobian.rows());
	FD_gradJ.setZero();

	auto FD = [this](Node* n1, Vector3d originalPosition, Vector3d step) {
		n1->SetPosition(originalPosition + step);
		double volume1 = Face::computeVolume(this->fem->chamberFaces);

		Eigen::VectorXd FD_J1(this->numOfNodes * 3);
		FD_J1.setZero();
		ComputeJ(FD_J1);

		FD_J1 *= this->springConstant*(volume1 - this->fem->GetTargetVolume());

		n1->SetPosition(originalPosition - step);
		double volume2 = Face::computeVolume(this->fem->chamberFaces);

		Eigen::VectorXd FD_J2(this->numOfNodes * 3);
		FD_J2.setZero();
		ComputeJ(FD_J2);

		FD_J2 *= this->springConstant*(volume2 - this->fem->GetTargetVolume());

		VectorXd result = (FD_J2 - FD_J1) / (2 * step.norm());
		return result;
	};

	double step = stepSize;
	for (int node_i = 0; node_i < this->numOfNodes; ++node_i) {
		Node* n1 = this->fem->halfEdgeDS_mesh.getNodeAtID(this->fem->halfEdgeDS_mesh.nodeIndexToID(node_i));
		Vector3d originalPosition = n1->GetPosition();

		Eigen::VectorXd FD_JresultX = FD(n1, originalPosition, Vector3d(step, 0, 0));
		Eigen::VectorXd FD_JresultY = FD(n1, originalPosition, Vector3d(0, step, 0));
		Eigen::VectorXd FD_JresultZ = FD(n1, originalPosition, Vector3d(0, 0, step));

		for (int i = 0; i < FD_JresultX.size(); ++i) {
			FD_gradJ(i, node_i * 3 + 0) = FD_JresultX[i];
		}
		for (int i = 0; i < FD_JresultY.size(); ++i) {
			FD_gradJ(i, node_i * 3 + 1) = FD_JresultY[i];
		}
		for (int i = 0; i < FD_JresultZ.size(); ++i) {
			FD_gradJ(i, node_i * 3 + 2) = FD_JresultZ[i];
		}

		n1->SetPosition(originalPosition);

		/*{
		// x deriv
		n1->SetPosition(originalPosition + Vector3d(step, 0, 0));
		double volume1 = Face::computeVolume(this->fem->chamberFaces);

		Eigen::VectorXd FD_J1x(this->numOfNodes * 3);
		FD_J1x.setZero();
		ComputeJ(FD_J1x);

		FD_J1x *= this->springConstant*(volume1 - this->fem->GetTargetVolume());

		n1->SetPosition(originalPosition + Vector3d(-step, 0, 0));
		double volume2 = Face::computeVolume(this->fem->chamberFaces);

		Eigen::VectorXd FD_J2x(this->numOfNodes * 3);
		FD_J2x.setZero();
		ComputeJ(FD_J2x);

		FD_J2x *= this->springConstant*(volume2 - this->fem->GetTargetVolume());

		FD_JresultX = (FD_J2x - FD_J1x) / (2 * step);
		}
		{
		// y deriv
		n1->SetPosition(originalPosition + Vector3d(0, step, 0));
		double volume1 = Face::computeVolume(this->fem->chamberFaces);

		Eigen::VectorXd FD_J1y(this->numOfNodes * 3);
		FD_J1y.setZero();
		ComputeJ(FD_J1y);

		FD_J1y *= this->springConstant*(volume1 - this->fem->GetTargetVolume());

		n1->SetPosition(originalPosition + Vector3d(0, -step, 0));
		double volume2 = Face::computeVolume(this->fem->chamberFaces);

		Eigen::VectorXd FD_J2y(this->numOfNodes * 3);
		FD_J2y.setZero();
		ComputeJ(FD_J2y);

		FD_J2y *= this->springConstant*(volume2 - this->fem->GetTargetVolume());

		FD_JresultY = (FD_J2y - FD_J1y) / (2 * step);
		}
		{
		// z deriv
		n1->SetPosition(originalPosition + Vector3d(0, 0, step));
		double volume1 = Face::computeVolume(this->fem->chamberFaces);

		Eigen::VectorXd FD_J1z(this->numOfNodes * 3);
		FD_J1z.setZero();
		ComputeJ(FD_J1z);

		FD_J1z *= this->springConstant*(volume1 - this->fem->GetTargetVolume());

		n1->SetPosition(originalPosition + Vector3d(0, 0, -step));
		double volume2 = Face::computeVolume(this->fem->chamberFaces);

		Eigen::VectorXd FD_J2z(this->numOfNodes * 3);
		FD_J2z.setZero();
		ComputeJ(FD_J2z);

		FD_J2z *= this->springConstant*(volume2 - this->fem->GetTargetVolume());

		FD_JresultZ = (FD_J2z - FD_J1z) / (2 * step);
		}*/
	}

	printValues(FD_gradJ, "FD");
	printValues(this->jacobian, "J");

	Eigen::MatrixXd FD_dense = FD_gradJ - this->densePart;
	Eigen::MatrixXd FD_sparse = FD_gradJ - this->sparsePart;
	//printValues(this->densePart, "dense");
	//printValues(this->sparsePart, "sparse");
	//printValues(FD_dense, "FD_dense");
	//printValues(FD_sparse, "FD_sparse");

	Eigen::MatrixXd relativeDifference = (FD_gradJ - this->jacobian) / this->jacobian.norm();


	cout << "Relative Difference norm (FD jacobian <-> jacobian): " << relativeDifference.norm() << endl;
}


void EnergyElement_Pressure::AddForcesContribution(VectorXd& totalForces) {
	for (int r = 0; r < this->numOfNodes; ++r) {
		int freeID_r = this->fem->GetNodesVector()[this->fem->halfEdgeDS_mesh.nodeIndexToID(r)]->GetId_Free();

		if (freeID_r < 0) {
			continue;
		}

		Vector3d nodeForce = forces.block<3, 1>(3 * r, 0);
		totalForces.block<3, 1>(3 * freeID_r, 0) += nodeForce;
	}
}

void EnergyElement_Pressure::AddJacobianContribution(VectorTd & totalJacobian) {
	for (int r = 0; r < this->numOfNodes; ++r) {
		for (int c = 0; c < this->numOfNodes; ++c) {
			int freeID_r = this->fem->GetNodesVector()[this->fem->halfEdgeDS_mesh.nodeIndexToID(r)]->GetId_Free();
			int freeID_c = this->fem->GetNodesVector()[this->fem->halfEdgeDS_mesh.nodeIndexToID(c)]->GetId_Free();

			// Is upper triangular? or is simulated
			if (freeID_c < freeID_r || freeID_r < 0 || freeID_c < 0) {
				continue;
			}

			AssembleSparseMatrix(totalJacobian, 3*freeID_r, 3*freeID_c, this->jacobian.block<3, 3>(3 * r, 3 * c));
		}
	}
}


void EnergyElement_Pressure::ComputeAndStore_ForcesAndJacobianSimple(void)
{
	// Using Boyle's law => P = k/V
	double V = Face::computeVolume(this->fem->chamberFaces);
	double P = this->pressureConstant / V;

	std::cout << "*** Volume of chamber: " << V << std::endl;
	std::cout << "*** Pressure in chamber: " << P << std::endl;

	// P = F/area => F = P*area
	Eigen::VectorXd forces(this->numOfNodes * 3);
	forces.setZero();
	for (const Face& f : this->fem->chamberFaces)
	{
		double area = f.computeArea();
		Vector3d faceNormal = f.computeNormal();
		faceNormal.normalize();
		Vector3d F = faceNormal * area * P / 3.0; 

		long n1_ID = f.n1->GetId();
		long n2_ID = f.n2->GetId();
		long n3_ID = f.n3->GetId();

		long n1_i = this->fem->halfEdgeDS_mesh.nodeIDToIndex(n1_ID);
		long n2_i = this->fem->halfEdgeDS_mesh.nodeIDToIndex(n2_ID);
		long n3_i = this->fem->halfEdgeDS_mesh.nodeIDToIndex(n3_ID);

		forces[n1_i * 3] += F.x();
		forces[n1_i * 3 + 1] += F.y();
		forces[n1_i * 3 + 2] += F.z();

		forces[n2_i * 3] += F.x();
		forces[n2_i * 3 + 1] += F.y();
		forces[n2_i * 3 + 2] += F.z();

		forces[n3_i * 3] += F.x();
		forces[n3_i * 3 + 1] += F.y();
		forces[n3_i * 3 + 2] += F.z();
	}

	printValues(forces, "Simple");
}

bool first = true;
void EnergyElement_Pressure::printValues(Eigen::VectorXd forces, string name) {
	ofstream myfile;
	if (first) {
		myfile.open("forces.txt");
		first = false;
	}
	else {
		myfile.open("forces.txt", std::ios_base::app);
	}


	myfile << name << " = [";
	for (int i = 0; i < forces.size(); ++i) {
		myfile << forces[i];
		if (i != forces.size() - 1) {
			myfile << ',';
		}
	}
	myfile << "]" << endl;
	myfile.close();
}

bool first2 = true;
void EnergyElement_Pressure::printValues(Eigen::MatrixXd jacobian, string name) {
	ofstream myfile;
	if (first2) {
		myfile.open("jacobian.txt");
		first2 = false;
	}
	else {
		myfile.open("jacobian.txt", std::ios_base::app);
	}

	myfile << name << " = [";
	for (int r = 0; r < jacobian.rows(); ++r) {
		for (int c = 0; c < jacobian.cols(); ++c) {
			myfile << jacobian(r, c) << ' ';
		}
		//myfile << ';';
	}
	myfile << "]" << endl;
	myfile.close();
}