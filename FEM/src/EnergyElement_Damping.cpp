//=============================================================================
//
//   Co-Rotational Finite Element Method source code
//   Based on Physics-Based Animation book, chapter 10
//
//   Based on original implementation by
//							Miguel A. Otaduy,    URJC Madrid
//							Alvaro G. Perez,     URJC Madrid
//							and Javier S. Zurdo, URJC Madrid
//
//	Authors:
//			Krisztian Amit Birkas, McGill Uni.
//			Jesus Perez Rodriguez, IST Austria
//
//=============================================================================

#include "EnergyElement_Damping.h"

#include "FEM.h"

EnergyElement_Damping::EnergyElement_Damping(FEM* fem) : EnergyElement(fem)
{
	int N = (int) this->fem->GetNodesVector().size() * 3;

	energy = 0;
	forces = VectorXd(N);
	jacobian = MatrixXd::Zero(0, 0);
}

void EnergyElement_Damping::ComputeAndStore_Forces()
{
	// TODO
}

void EnergyElement_Damping::AddForcesContribution(VectorXd& totalForces)
{
	// TODO
}
