//=============================================================================
//
//   Co-Rotational Finite Element Method source code
//   Based on Physics-Based Animation book, chapter 10
//
//   Based on original implementation by
//							Miguel A. Otaduy,    URJC Madrid
//							Alvaro G. Perez,     URJC Madrid
//							and Javier S. Zurdo, URJC Madrid
//
//	Authors:
//			Krisztian Amit Birkas, McGill Uni.
//			Jesus Perez Rodriguez, IST Austria
//
//=============================================================================

#include "EnergyElement_CorotFEM.h"

#include "FEM.h"

#include "Tetrahedron.h"

EnergyElement_CorotFEM::EnergyElement_CorotFEM(FEM* fem, Tetrahedron* tetrahedron) : EnergyElement(fem)
{
	energy = 0;
	forces = VectorXd(12);
	jacobian = MatrixXd(12, 12);

	this->tetrahedron = tetrahedron;
}

void EnergyElement_CorotFEM::Initialize()
{
	this->tetrahedron->PrecomputeK(this->fem->CalculateElasticityCoefficients(fem->GetPoissonRatio(), fem->GetYoungModulus()));
}

void EnergyElement_CorotFEM::ComputeAndStore_Energy()
{
	// Elasticity coefficients

	Eigen::Vector3d D = this->fem->CalculateElasticityCoefficients(this->fem->GetPoissonRatio(), this->fem->GetYoungModulus());

	// Compute deformation gradient

	Matrix3d F = this->tetrahedron->GetVolumeMatrix() * this->tetrahedron->GetInitialVolumeMatrixInverse();
	Matrix3d R = (fem->GetUseStiffnessWarping() ? tetrahedron->CalculateRotation() : Matrix3d::Identity());
	F = R.transpose()*F;

	// Compute Cauchy strain

	Eigen::Matrix3d nablaU = F - Eigen::Matrix3d::Identity();
	Eigen::Matrix3d C = (1.0 / 2.0)*(nablaU + nablaU.transpose());

	// Compute Cauchy stress

	Eigen::Matrix3d T = Eigen::Matrix3d::Zero();
	T(0, 0) = D(0)*C(0, 0) + D(1)*C(1, 1) + D(1)*C(2, 2);
	T(1, 1) = D(1)*C(0, 0) + D(0)*C(1, 1) + D(1)*C(2, 2);
	T(2, 2) = D(1)*C(0, 0) + D(1)*C(1, 1) + D(0)*C(2, 2);
	T(0, 1) = T(1, 0) = D(2)*C(0, 1);
	T(0, 2) = T(2, 0) = D(2)*C(0, 2);
	T(1, 2) = T(2, 1) = D(2)*C(1, 2);

	// Energy density
	this->energy = 0;
	for (unsigned int i = 0; i < 3; i++)
		for (unsigned int j = 0; j < 3; j++)
			this->energy += C(i, j)*T(i, j);
	this->energy /= 2;
}

void EnergyElement_CorotFEM::ComputeAndStore_ForcesAndJacobian()
{
	// Explanation: 
	// 1. Compute tetrahedron rotation T ~ R*T0
	// 2. The deformed configuration is rotated back to material R^T*x
	// 3. Compute deformed and material difference is compute u = R^T*x - x0
	// 4. Compute force in material space based on deformation f0 = K*u
	// 5. Rotate the force forward to deformation spaces R*f0
	// Summarized: f = R*K*(K^T*x - x0)

	Matrix3d R = (fem->GetUseStiffnessWarping() ? tetrahedron->CalculateRotation() : Matrix3d::Identity());

	// Assemble rotations

	MatrixXd Re = MatrixXd(12, 12);

	Re.setZero();
	for (int i = 0; i < 4; ++i)
		Re.block<3, 3>(3 * i, 3 * i) = R;

	// Assemble deformations

	VectorXd vx = VectorXd(12);
	VectorXd v0 = VectorXd(12);
	for (int i = 0; i < 4; ++i)
	{
		Node* node_i = tetrahedron->GetNode(i);
		Vector3d posx = node_i->GetPosition();
		Vector3d pos0 = node_i->GetPosition0();
		vx.block<3, 1>(3 * i, 0) = posx;
		v0.block<3, 1>(3 * i, 0) = pos0;
	}

	// Get precomputed K

	const MatrixXd& Ke = tetrahedron->GetK();

	// Compute and store elastic Jacobian

	jacobian = Re*Ke*Re.transpose();

	// Compute and store elastic forces

	forces = -(jacobian*vx - Re*(Ke*v0));
}

void EnergyElement_CorotFEM::AddForcesContribution(VectorXd& totalForces)
{
	for (int i = 0; i < 4; ++i)
	{
		Node* node_i = tetrahedron->GetNode(i);

		int freeId = node_i->GetId_Free();
		
		// Is simulated?
		if (freeId < 0)
			continue;

		Vector3d nodeForce = forces.block<3, 1>(3 * i, 0);
		totalForces.block<3, 1>(3 * freeId, 0) += nodeForce;
	}
}

void EnergyElement_CorotFEM::AddJacobianContribution(VectorTd & totalJacobian)
{
	for (int i = 0; i < 4; ++i)
	{
		Node* node_i = tetrahedron->GetNode(i);
		int freeId_i = node_i->GetId_Free();
		if (freeId_i < 0) continue;

		for (int j = 0; j < 4; ++j)
		{
			Node* node_j = tetrahedron->GetNode(j);
			int freeId_j = node_j->GetId_Free();
			if (freeId_j < 0) continue;

			// Is upper triangular?
			if (freeId_i < freeId_j)
				continue;

			AssembleSparseMatrix(totalJacobian, 3*freeId_i, 3*freeId_j, jacobian.block<3, 3>(3 * i, 3 * j));
		}
	}
}
