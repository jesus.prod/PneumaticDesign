//=============================================================================
//
//   Co-Rotational Finite Element Method source code
//   Based on Physics-Based Animation book, chapter 10
//
//   Based on original implementation by
//							Miguel A. Otaduy,    URJC Madrid
//							Alvaro G. Perez,     URJC Madrid
//							and Javier S. Zurdo, URJC Madrid
//
//	Authors:
//			Krisztian Amit Birkas, McGill Uni.
//			Jesus Perez Rodriguez, IST Austria
//
//=============================================================================

#include "Utils.h"

void AssembleSparseMatrix(VectorTd & triplets, unsigned int row, unsigned int col, const Matrix3d& m33)
{
	triplets.push_back(Triplet<double>(row, col, m33(0, 0)));
	triplets.push_back(Triplet<double>(row, col + 1, m33(0, 1)));
	triplets.push_back(Triplet<double>(row, col + 2, m33(0, 2)));

	triplets.push_back(Triplet<double>(row + 1, col, m33(1, 0)));
	triplets.push_back(Triplet<double>(row + 1, col + 1, m33(1, 1)));
	triplets.push_back(Triplet<double>(row + 1, col + 2, m33(1, 2)));

	triplets.push_back(Triplet<double>(row + 2, col, m33(2, 0)));
	triplets.push_back(Triplet<double>(row + 2, col + 1, m33(2, 1)));
	triplets.push_back(Triplet<double>(row + 2, col + 2, m33(2, 2)));
}

void EigenTripletsToSparseMatrix(const VectorTd& triplets, SparseMatrix<double>& M)
{
	M.setFromTriplets(triplets.begin(), triplets.end());
}

void EigenSparseMatrixToTriplets(const SparseMatrix<double>& M, VectorTd& triplets)
{
	int NZ = M.nonZeros();
	if (triplets.capacity() < NZ)
		triplets.reserve(NZ);
	triplets.clear();

	for (int k = 0; k < M.outerSize(); ++k)
		for (SparseMatrix<double>::InnerIterator it(M, k); it; ++it)
			triplets.push_back(Triplet<double>(it.row(), it.col(), it.value()));
}

void EigenVectorToCUDA(const VectorXd& veigen, vector<float>& vcuda)
{
	int numVal = (int) veigen.size();
	vcuda.reserve(numVal);
	for (int i = 0; i < numVal; ++i)
		vcuda.push_back((float) veigen(i));
}

void CUDAToEigenVector(const vector<float>& vcuda, VectorXd& veigen)
{
	int numVal = (int)vcuda.size();
	veigen.resize(numVal);
	for (int i = 0; i < numVal; ++i)
		veigen(i) = vcuda[i];
}

void EigenTripletsToCUDA(const VectorTd& triplets, vector<int>& rows, vector<int>& cols, vector<float>& vals)
{
	int numNZ = (int) triplets.size();
	rows.reserve(numNZ);
	cols.reserve(numNZ);
	vals.reserve(numNZ);
	for (int i = 0; i < numNZ; ++i)
	{
		const Triplet<double>& t = triplets[i];
		rows.push_back(t.row());
		cols.push_back(t.col());
		vals.push_back(t.value());
	}
}

void CUDAToEigenTriplets(const vector<int>& rows, const vector<int>& cols, const vector<float>& vals, VectorTd& triplets)
{
	int numNZ = (int)rows.size();
	triplets.reserve(numNZ);
	for (int i = 0; i < numNZ; ++i)
	{
		triplets.push_back(Triplet<double>(rows[i], cols[i], vals[i]));
	}
}