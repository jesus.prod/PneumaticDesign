//=============================================================================
//
//   Co-Rotational Finite Element Method source code
//   Based on Physics-Based Animation book, chapter 10
//
//   Based on original implementation by
//							Miguel A. Otaduy,    URJC Madrid
//							Alvaro G. Perez,     URJC Madrid
//							and Javier S. Zurdo, URJC Madrid
//
//	Authors:
//			Krisztian Amit Birkas, McGill Uni.
//			Jesus Perez Rodriguez, IST Austria
// 
//=============================================================================

#include "EnergyElement_Gravity.h"

#include "FEM.h"

EnergyElement_Gravity::EnergyElement_Gravity(FEM* fem) : EnergyElement(fem)
{
	int N = (int) this->fem->GetNodesVector().size() * 3;

	energy = 0;
	forces = VectorXd(N);
	jacobian = MatrixXd::Zero(0,0);
} 

void EnergyElement_Gravity::Initialize()
{
	// Gravity forces constant, computed at initialization

	const vector<Node*>& vectorNodes = fem->GetNodesVector();
	int numNode = (int )vectorNodes.size();
	for (int i = 0; i < numNode; ++i)
	{
		const Node* node_i = vectorNodes[i];

		Vector3d nodeForce = Vector3d(0,-1,0)*GRAVITY*node_i->GetMass();
		forces.block<3, 1>(3*node_i->GetId_Full(), 0) = nodeForce;
	}
}

void EnergyElement_Gravity::ComputeAndStore_Energy()
{
	const vector<Node*>& vectorFreeNodes = fem->GetFreeNodesVector();
	int numNode = (int)vectorFreeNodes.size();
	int N = 3*numNode;
	VectorXd vx(N);
	VectorXd vf(N);
	vx.setZero();
	vf.setZero();
	this->fem->GetPositions(vx);
	this->AddForcesContribution(vf);
	this->energy = -vx.dot(vf);
}

void EnergyElement_Gravity::AddForcesContribution(VectorXd& totalForces)
{
	const vector<Node*>& vectorFreeNodes = fem->GetFreeNodesVector();
	int numNode = (int) vectorFreeNodes.size();
	for (int i = 0; i < numNode; ++i)
	{
		const Node* node_i = vectorFreeNodes[i];

		int fullId = node_i->GetId_Full();
		int freeId = node_i->GetId_Free();

		Vector3d nodeForce = forces.block<3, 1>(3*fullId, 0);
		totalForces.block<3, 1>(3 * freeId, 0) += nodeForce;
	}
}
