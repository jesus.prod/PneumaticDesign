//=============================================================================
//
//   Co-Rotational Finite Element Method source code
//   Based on Physics-Based Animation book, chapter 10
//
//   Based on original implementation by
//							Miguel A. Otaduy,    URJC Madrid
//							Alvaro G. Perez,     URJC Madrid
//							and Javier S. Zurdo, URJC Madrid
//
//	Authors:
//			Krisztian Amit Birkas, McGill Uni.
//			Jesus Perez Rodriguez, IST Austria
//
//=============================================================================

#include "FEM.h"

#include <iostream>
#include <fstream>

//#include "amgx_c.h"

// CUDA //////////////////////////////

//#include <cuda_runtime.h>
//
//#include <cuda.h>
//#include <cusparse.h>
//#include <cusolverSp.h>

// CUDA //////////////////////////////

#define LS_THRESHOLD 0.001
#define ZERO_THRESHOLD 1e-16

double FEM::timeStep = 0.01;
double FEM::invTimeStep = 1.0 / timeStep;
double FEM::timeStepSquare = timeStep * timeStep;

// Six Strain Limiting constraints
const unsigned int nConstraints = 6;

// Less Than comparison class for the Node* set added in creation
struct lt_nodes
{
	bool operator()(Node* a, Node* b) const
	{
		return a < b;
	}
};

// Less Than comparison class for the Edge* set added in creation
struct lt_edges
{
	bool operator()(Edge* a, Edge* b) const
	{
		return (a->GetOrigin() < b->GetOrigin()) || (a->GetOrigin() == b->GetOrigin() && a->GetHead() < b->GetHead());
	}
};

///////////////////
//// UTILITIES ////
///////////////////
//
//

void writeToFile(const VectorXd& v, const string& str)
{
	ofstream file;
	file.open(str, ios::out | ios::trunc);
	for (int i = 0; i < (int)v.size(); ++i)
	{
		if (i != (int)v.size() - 1)
			file << v(i) << ",";
		else file << v(i);
	}
	file.close();
}

void writeToFile(const MatrixXd& M, const string& str)
{
	ofstream file;
	file.open(str, ios::out | ios::trunc);
	for (int i = 0; i < (int)M.rows(); ++i)
	{
		for (int j = 0; j < (int)M.cols(); ++j)
		{
			if (j != (int)M.cols() - 1)
				file << M(i, j) << ",";
			else file << M(i, j);
		}
		file << "\n";
	}
	file.close();
}

void FEM::UpdateDeformationDependentMagnitudes()
{
	int numTet = (int) this->vectorTetrahedra.size();

#pragma omp parallel for
	for (int i = 0; i < numTet; ++i)
		this->vectorTetrahedra[i]->UpdateDeformationDependentMagnitudes();
}

double FEM::GetAverageEdgeLength(void)
{
	unsigned int nTetrahedra = (unsigned int)vectorTetrahedra.size();

	double averageEdgeLength = 0.0;
	for (unsigned int e = 0; e < nTetrahedra; e++)
		averageEdgeLength += vectorTetrahedra[e]->GetAverageEdgeLength();

	return averageEdgeLength / (double)nTetrahedra;
}

void FEM::SetMassDensity(double _density)
{
	unsigned int nTetrahedra = (unsigned int)vectorTetrahedra.size(), i;

	for (i = 0; i < nTetrahedra; i++)
	{
		vectorTetrahedra[i]->SetMassDensity(_density);
	}
}


//////////////////
//// CREATION ////
//////////////////
//
//

void FEM::InitializeAttributes(double _poissonRatio, double _youngModulus)
{
	globalNodeId = 0;
	globalTetraId = 0;

	timeStep = FEM::timeStep;
	invTimeStep = 1.0 / timeStep;
	timeStepSquare = timeStep * timeStep;
	massDamping = 0.2;  // alpha
	stiffDamping = 0.0; // beta
	stiffnessWarping = true;

	constraintsUsed = NONE;
	youngModulus = _youngModulus;
	poissonRatio = _poissonRatio;
	constraintsMethod = UNCONSTRAINED;

	vectorNodes.clear();
	vectorEdges.clear();

	directSolver = false;
}

void FEM::CreateEdgesFromNodes(void)
{
	unsigned int nTetrahedra = (unsigned int)vectorTetrahedra.size(), e, n, m, pos;
	set<Edge*, lt_edges> setEdgesTemp;
	set<Edge*, lt_edges>::iterator itEdges;
	Tetrahedron* tetra_e;
	Node *node1, *node2;
	Edge* edge;

	for (e = 0; e < nTetrahedra; e++)
	{
		pos = 0;
		tetra_e = vectorTetrahedra[e];

		for (m = 0; m < 4; m++)
		{
			node1 = tetra_e->GetNode(m);

			// for (n=m+1;...): n=m to avoid adding duplicated edges in tetrahedron e; n=m+1 to skip nodes (n=m)
			for (n = m + 1; n < 4; n++)
			{
				node2 = tetra_e->GetNode(n);
				edge = ((node1->GetId_Full() <= node2->GetId_Full()) ? new Edge(node1, node2) : new Edge(node2, node1));

				itEdges = setEdgesTemp.find(edge);
				if (itEdges == setEdgesTemp.end()) // If not found -> it must be added
				{
					vectorEdges.push_back(edge);
					setEdgesTemp.insert(edge); // setEdgesTemp has to be filled in order to find duplicates

											   // Adds edge to both nodes (edges corresponding to the upper and lower triangular matrix)
					node1->AddAdjacentEdge(edge);
					node2->AddAdjacentEdge(edge);
				}
				else
				{
					delete edge;
					edge = *itEdges;
				}
				tetra_e->SetEdge(edge, pos++);
			}
		}
	}
}

void FEM::CreateNodesAndEdges(void)
{
	set<Node*, lt_nodes> setNodesTemp;
	set<Edge*, lt_edges> setEdgesTemp;
	set<Edge*, lt_edges>::iterator itEdges;

	for (unsigned int e = 0; e < this->vectorTetrahedra.size(); e++)
	{
		int pos = 0;

		for (unsigned int m = 0; m < 4; m++)
		{
			Node* node1 = this->vectorTetrahedra[e]->GetNode(m);

			if (setNodesTemp.find(node1) == setNodesTemp.end()) // If not found -> it must be added
			{
				this->vectorNodes.push_back(node1);
				setNodesTemp.insert(node1); // setNodesTemp has to be filled in order to find duplicates
			}
		}

		for (unsigned int m = 0; m < 4; m++)
		{
			Node* node1 = this->vectorTetrahedra[e]->GetNode(m);

			// for (n=m+1;...): n=m to avoid adding duplicated edges in tetrahedron e; n=m+1 to skip nodes (n=m)
			for (unsigned int n = m + 1; n < 4; n++)
			{
				Node* node2 = this->vectorTetrahedra[e]->GetNode(n);
				Edge* edge = ((node1->GetId_Full() <= node2->GetId_Full()) ? new Edge(node1, node2) : new Edge(node2, node1));

				itEdges = setEdgesTemp.find(edge);
				if (itEdges == setEdgesTemp.end()) // If not found -> it must be added
				{
					this->vectorEdges.push_back(edge);
					setEdgesTemp.insert(edge); // setEdgesTemp has to be filled in order to find duplicates

											   // Adds edge to both nodes (edges corresponding to the upper and lower triangular matrix)
					node1->AddAdjacentEdge(edge);
					node2->AddAdjacentEdge(edge);
				}
				else
				{
					delete edge;
					edge = *itEdges;
				}
				this->vectorTetrahedra[e]->SetEdge(edge, pos++);
			}
		}
	}
}

bool FEM::BuildEmbeddedStructure(const vector<Vector3d>& vpos, const vector<int>& vidx, double ks)
{
	this->stretchK = ks;

	int numNode = (int) vpos.size();
	int numEdge = (int) vidx.size()/2;

	int numTets = (int) this->vectorTetrahedra.size();

	this->vectorEBDNodes.reserve(numNode);
	this->vectorEBDEdges.reserve(numEdge);

	// Create nodes

	for (int i = 0; i < numNode; ++i)
	{
		// Find embedding

		const Vector3d& vp = vpos[i];
		
		Vector4d vb; 
		vb.setZero();

		bool found = false;

		for (int j = 0; j < numTets; ++j)
		{
			if (this->vectorTetrahedra[j]->CalculateBarycentricCoords(vp, vb))
			{
				found = true;
				vector<Node*> vnodes(4);
				vnodes[0] = this->vectorTetrahedra[j]->GetNode(0);
				vnodes[1] = this->vectorTetrahedra[j]->GetNode(1);
				vnodes[2] = this->vectorTetrahedra[j]->GetNode(2);
				vnodes[3] = this->vectorTetrahedra[j]->GetNode(3);
				vector<double> vcoords(4);
				vcoords[0] = vb[0];
				vcoords[1] = vb[1];
				vcoords[2] = vb[2];
				vcoords[3] = vb[3];
				this->vectorEBDNodes.push_back(new NodeEBD(vnodes, vcoords));

				// Testing

				std::cout << "Testing node defo: " << (vp - this->vectorEBDNodes[i]->ComputePosition()).norm() << std::endl;
				std::cout << "Testing node rest: " << (vp - this->vectorEBDNodes[i]->ComputePosition0()).norm() << std::endl;

				break; // Found embedding tetrahedron, no need more search
			}
		}

		if (!found) 
		{
			freeEmbeddedStructure();
			return false; // Invalid
		}
	}

	// Create edges

	for (int i = 0; i < numEdge; ++i)
	{
		int offset = 2*i;
		NodeEBD* node0 = this->vectorEBDNodes[vidx[offset + 0]];
		NodeEBD* node1 = this->vectorEBDNodes[vidx[offset + 1]];
		this->vectorEBDEdges.push_back(new EdgeEBD(node0, node1));
	}

	return true;
}

FEM::FEM(vector<Tetrahedron*>& _vectorTetrahedra, double _poisson, double _young, double _density)
{
	InitializeAttributes(_poisson, _young);
	vectorTetrahedra = _vectorTetrahedra;

	// Create data structures

	CreateNodesAndEdges();

	SetMassDensity(_density);
	PrepareSimulationData();
}

FEM::FEM(vector<Tetrahedron*>& _vectorTetrahedra, vector<Edge*>& edges, vector<Node*>& nodes, double _poisson, double _young, double _density)
{
	InitializeAttributes(_poisson, _young);
	this->vectorTetrahedra = _vectorTetrahedra;

	// Create data structures
	this->vectorNodes = nodes;
	this->vectorEdges = edges;

	for (int e = 0; e < this->vectorEdges.size(); ++e) {
		Node* n1 = this->vectorEdges[e]->GetOrigin();
		Node* n2 = this->vectorEdges[e]->GetHead();

		n1->AddAdjacentEdge(this->vectorEdges[e]);
		n2->AddAdjacentEdge(this->vectorEdges[e]);
	}

	for (int t = 0; t < this->vectorTetrahedra.size(); ++t) {
		int numOfNodesInTetrahedra = 4;

		std::vector<Node*> nodes;
		for (int i = 0; i < numOfNodesInTetrahedra; ++i) {
			nodes.push_back(this->vectorTetrahedra[t]->GetNode(i));
		}

		std::vector<Edge*> edges;
		for (int i = 0; i < numOfNodesInTetrahedra; ++i) {
			std::vector<Edge*> tempEdges = nodes[i]->GetAdjacentEdges();
			for (int j = 0; j < tempEdges.size(); ++j) {
				edges.push_back(tempEdges[j]);
			}
		}

		std::vector<Edge*> uniqueEdges;
		for (int i = 0; i < edges.size(); ++i) {
			bool add = true;
			for (int j = 0; j < uniqueEdges.size(); ++j) {
				if ((edges[i]->GetOrigin()->GetId() == uniqueEdges[j]->GetOrigin()->GetId() && edges[i]->GetHead()->GetId() == uniqueEdges[j]->GetHead()->GetId()) ||
					(edges[i]->GetHead()->GetId() == uniqueEdges[j]->GetOrigin()->GetId() && edges[i]->GetOrigin()->GetId() == uniqueEdges[j]->GetHead()->GetId()))
				{
					add = false;
					break;
				}
			}
			if (add) {
				uniqueEdges.push_back(edges[i]);
			}
		}

		this->vectorTetrahedra[t]->SetEdge(new Edge(nodes[0], nodes[1]), 0);
		this->vectorTetrahedra[t]->SetEdge(new Edge(nodes[0], nodes[2]), 1);
		this->vectorTetrahedra[t]->SetEdge(new Edge(nodes[0], nodes[3]), 2);
		this->vectorTetrahedra[t]->SetEdge(new Edge(nodes[1], nodes[2]), 3);
		this->vectorTetrahedra[t]->SetEdge(new Edge(nodes[1], nodes[3]), 4);
		this->vectorTetrahedra[t]->SetEdge(new Edge(nodes[2], nodes[3]), 5);

		std::vector<Edge*> tetEdges;
		for (int i = 0; i < numOfNodesInTetrahedra-1; ++i) {
			for (int j = i + 1; j < numOfNodesInTetrahedra; ++j) {
				tetEdges.push_back(new Edge(nodes[i], nodes[j]));
			}
		}

		int edgeID = 0;
		for (int i = 0; i < 6; ++i) {
			bool matched = false;
			for (int j = 0; j < uniqueEdges.size(); ++j) {
				if (tetEdges[i]->Equals(uniqueEdges[j])) {
					this->vectorTetrahedra[t]->SetEdge(uniqueEdges[j], edgeID);
					matched = true;
					break;
				}
			}

			if (!matched) {
				this->vectorTetrahedra[t]->SetEdge(tetEdges[i], edgeID);
				this->vectorEdges.push_back(tetEdges[i]);
			}
			++edgeID;
		}

		//int edgeID = 0;
		//for (int e = 0; e < uniqueEdges.size(); ++e) {
		//	for (int n1 = 0; n1 < nodes.size() - 1; ++n1) {
		//		for (int n2 = n1 + 1; n2 < nodes.size(); ++n2) {
		//			if ((nodes[n1]->GetId() == uniqueEdges[e]->GetOrigin()->GetId() && nodes[n2]->GetId() == uniqueEdges[e]->GetHead()->GetId()) ||
		//				(nodes[n2]->GetId() == uniqueEdges[e]->GetOrigin()->GetId() && nodes[n1]->GetId() == uniqueEdges[e]->GetHead()->GetId())) {
		//				this->vectorTetrahedra[t]->SetEdge(uniqueEdges[e], edgeID);
		//				++edgeID;
		//			}
		//		}
		//	}
		//}
	}



	SetMassDensity(_density);
}



FEM::FEM(vector<Tetrahedron*>& _vectorTetrahedra, vector<Edge*>& _vectorEdges, vector<Node*>& _vectorNodes, vector<Face>& _chamberFaces, double _poisson, double _young, double _density)
{
	InitializeAttributes(_poisson, _young);
	vectorTetrahedra = _vectorTetrahedra;
	vectorEdges = _vectorEdges;
	vectorNodes = _vectorNodes;

	SetMassDensity(_density);
	PrepareSimulationData();
}

FEM::FEM(int nverts, double *pos, int ntets, int *inds, double scale, const Vector3d& translate, const Matrix3d& rotate)
{
	InitializeAttributes();

	// Create nodes
	vectorNodes.clear();
	vectorNodes.reserve(nverts);

	Node *vi;
	for (int i = 0, j = 0; i < nverts; i++, j += 3)
	{
		vi = new Node(translate + rotate * scale * Vector3d(pos[j], pos[j + 1], pos[j + 2]));
		vi->SetId_Full(IncGlobalNodeId()); // Set node globalId

		vectorNodes.push_back(vi);
	}

	// Create tetrahedra
	vectorTetrahedra.clear();
	vectorTetrahedra.reserve(ntets);

	Tetrahedron *ti;
	for (int i = 0, j = 0; i < ntets; i++, j += 4)
	{
		ti = new Tetrahedron(vectorNodes[inds[j]], vectorNodes[inds[j + 1]], vectorNodes[inds[j + 2]], vectorNodes[inds[j + 3]]);
		ti->SetId(IncGlobalTetrahedronId()); // Set tetrahedron globalId

		vectorTetrahedra.push_back(ti);
	}

	CreateEdgesFromNodes();
	PrepareSimulationData();
	initialEdgeLength = GetAverageEdgeLength();
}

FEM *FEM::ReadTGMeshFile(const char* filename, bool inverted, double scale, const Vector3d &translate, const Matrix3d& rotate)
{
	// Open the tetrahedral mesh file
	//
	FILE *file = fopen(filename, "r");
	if (!file) return NULL;

	// Search for the vertices
	//
	const unsigned int lineLength = 256;
	char lineStr[lineLength] = "\0";
	unsigned int nverts, ntets;

	while (strcmp(lineStr, "Vertices\n") != 0) fgets(lineStr, lineLength, file);
	fgets(lineStr, lineLength, file);
	sscanf(lineStr, "%u", &nverts);

	// Read out the vertices
	//
	double x, y, z;
	unsigned int a, b, c, d, dummy;

	double *pos = new double[3 * nverts];
	for (unsigned int i = 0, j = 0; i < nverts; i++, j += 3)
	{
		fgets(lineStr, lineLength, file);
		sscanf(lineStr, "%lf %lf %lf %d", &x, &y, &z, &dummy);
		if (inverted)
		{
			y = -y;
			z = -z;
		}

		pos[j] = x;
		pos[j + 1] = y;
		pos[j + 2] = z;
	}

	// Search for the tetrahedra
	//
	while (strcmp(lineStr, "Tetrahedra\n") != 0) fgets(lineStr, lineLength, file);
	fgets(lineStr, lineLength, file);
	sscanf(lineStr, "%u", &ntets);

	// Read out the tetrahedra
	//
	int *inds = new int[4 * ntets];
	for (unsigned int i = 0, j = 0; i < ntets; i++, j += 4)
	{
		fgets(lineStr, lineLength, file);
		sscanf(lineStr, "%u %u %u %u %u", &a, &b, &c, &d, &dummy);

		// Vertices' indexes inside the file start in 1 instead of 0
		inds[j] = a - 1;
		inds[j + 1] = b - 1;
		inds[j + 2] = c - 1;
		inds[j + 3] = d - 1;
	}

	fclose(file);

	FEM *mesh = new FEM(nverts, pos, ntets, inds, scale, translate, rotate);
	delete[] pos;
	delete[] inds;

	return mesh;
}

FEM *FEM::CreateBlockMesh(int numX, int numY, int numZ, double size, const Vector3d &translate, const Matrix3d& rotate)
{
	// Create vertices
	int nverts = (numX + 1) * (numY + 1) * (numZ + 1);
	double *pos = new double[3 * nverts];
	for (int i = 0, l = 0; i <= numX; i++)
	{
		for (int j = 0; j <= numY; j++)
		{
			for (int k = 0; k <= numZ; k++)
			{
				pos[l++] = size*i; pos[l++] = size*j; pos[l++] = size*k;
			}
		}
	}

	// Create tetrahedra
	int ntets = 5 * numX * numY * numZ;
	int *inds = new int[4 * ntets];
	for (int i = 0, l = 0; i < numX; i++)
	{
		for (int j = 0; j < numY; j++)
		{
			for (int k = 0; k < numZ; k++)
			{
				int i5 = (i*(numY + 1) + j)*(numZ + 1) + k; int i1 = i5 + 1;
				int i6 = ((i + 1)*(numY + 1) + j)*(numZ + 1) + k; int i2 = i6 + 1;
				int i7 = ((i + 1)*(numY + 1) + (j + 1))*(numZ + 1) + k; int i3 = i7 + 1;
				int i8 = (i*(numY + 1) + (j + 1))*(numZ + 1) + k; int i4 = i8 + 1;

				if ((i + j + k) % 2 == 1)
				{
					inds[l++] = i1; inds[l++] = i2; inds[l++] = i3; inds[l++] = i6;
					inds[l++] = i6; inds[l++] = i3; inds[l++] = i8; inds[l++] = i7;
					inds[l++] = i1; inds[l++] = i8; inds[l++] = i3; inds[l++] = i4;
					inds[l++] = i1; inds[l++] = i6; inds[l++] = i8; inds[l++] = i5;
					inds[l++] = i1; inds[l++] = i3; inds[l++] = i8; inds[l++] = i6;
				}
				else {
					inds[l++] = i2; inds[l++] = i5; inds[l++] = i4; inds[l++] = i1;
					inds[l++] = i2; inds[l++] = i7; inds[l++] = i5; inds[l++] = i6;
					inds[l++] = i2; inds[l++] = i4; inds[l++] = i7; inds[l++] = i3;
					inds[l++] = i5; inds[l++] = i7; inds[l++] = i4; inds[l++] = i8;
					inds[l++] = i2; inds[l++] = i5; inds[l++] = i7; inds[l++] = i4;
				}
			}
		}
	}

	FEM *mesh = new FEM(nverts, pos, ntets, inds, 1.0, translate, rotate);
	delete[] pos;
	delete[] inds;

	return mesh;
}

////////////////////
//// SIMULATION ////
////////////////////
//
//

Vector3d FEM::CalculateElasticityCoefficients(const double poissonRatio, const double youngModulus)
{
	// This method returns D_0, D_1 and D_2 coefficients of the elasticity matrix D,
	//
	//           | D_0 D_1 D_1  0  0  0  |
	//           | D_1 D_0 D_1  0  0  0  |
	// where D = | D_1 D_1 D_0  0  0  0  |
	//           |  0   0   0  D_2 0  0  |
	//           |  0   0   0   0 D_2 0  |
	//           |  0   0   0   0  0 D_2 |
	// 
	//             Y(1-v)                    Yv                    Y(1-2v)
	// and D_0 = -----------   ;   D_1 = -----------   ;   D_2 = ------------   ,
	//           (1+v)(1-2v)             (1+v)(1-2v)             2(1+v)(1-2v)
	//
	// being Y the Young's modulus and v the Poisson's ratio, passed as arguments.
	//
	double tmp_value = 1.0 - (poissonRatio + poissonRatio);
	double factor = youngModulus * 1.0 / ((1.0 + poissonRatio)*tmp_value);

	// Returns D_0, D_1 and D_2 coefficients
	return Vector3d(factor * (1.0 - poissonRatio), factor * poissonRatio, factor * 0.5 * tmp_value);
}

void FEM::CreateEnergyElements(void)
{
	freeElements();

	int numTet = (int)vectorTetrahedra.size();
	int numEBDNode = (int) this->vectorEBDNodes.size();
	int numEBDEdge = (int) this->vectorEBDEdges.size();

	int numTotal = numTet + 1 + 1 + numEBDNode + numEBDEdge;

	this->vectorElements.reserve(numTotal);

	// FEM energy elements

#if simulationMethodStatic
	this->vectorElementsStVKFEM.resize(numTet);
	for (int i = 0; i < numTet; ++i)
	{
		EnergyElement_StVKFEM* newStVKEle = new EnergyElement_StVKFEM(this, vectorTetrahedra[i]);
		this->vectorElementsStVKFEM[i] = newStVKEle;
		this->vectorElements.push_back(newStVKEle);
	}
#else
	this->vectorElementsCorotFEM.resize(numTet);
	for (int i = 0; i < numTet; ++i)
	{
		EnergyElement_CorotFEM* newCorotEle = new EnergyElement_CorotFEM(this, vectorTetrahedra[i]);
		this->vectorElementsCorotFEM[i] = newCorotEle;
		this->vectorElements.push_back(newCorotEle);
	}
#endif

	

	// Gravity elements
#if allowGravity
	EnergyElement_Gravity* newGravityEle = new EnergyElement_Gravity(this);
	this->vectorElementsGravity.resize(1);
	this->vectorElementsGravity[0] = newGravityEle;
	this->vectorElements.push_back(newGravityEle);
#endif
	// Pressure elements

	EnergyElement_Pressure* newPressureEle = new EnergyElement_Pressure(this, 1, 1);
	this->vectorElementsPressure.resize(1);
	this->vectorElementsPressure[0] = newPressureEle;
	this->vectorElements.push_back(newPressureEle);

	// Embedded structure elements

	this->vectorElementsEBDStretch.resize(numEBDEdge);

	for (int i = 0; i < numEBDEdge; ++i)
	{
		vector<NodeEBD*> vnodes(2);
		vnodes[0] = this->vectorEBDEdges[i]->GetOrigin();
		vnodes[1] = this->vectorEBDEdges[i]->GetHead();
		EnergyElement_EBD_RodStretch* newEBDStretchEle = new EnergyElement_EBD_RodStretch(this, vnodes);
		this->vectorElementsEBDStretch[i] = newEBDStretchEle;
		this->vectorElements.push_back(newEBDStretchEle);
	}
}

void FEM::InitializeEnergyElements(void)
{
	int numTotal = (int) this->vectorElements.size();

	// Initialize energies

	for (int i = 0; i < numTotal; ++i)
	{
		this->vectorElements[i]->Initialize();
	}
}

void FEM::PrepareSimulationData(void)
{
	this->CreateEnergyElements();
	this->InitializeEnergyElements();

	// Update free vector nodes

	vectorFreeNodes.clear();

	for (unsigned int i = 0, id = 0; i < vectorNodes.size(); i++)
	{
		if (vectorNodes[i]->IsFixed())
		{
			vectorNodes[i]->SetId_Free(-1);
		}
		else
		{
			vectorNodes[i]->SetId_Free(id++);
			vectorFreeNodes.push_back(vectorNodes[i]);
		}
	}

	// Restart sparse matrices

	this->sparseD.setZero();
	this->sparseM.setZero();
	this->sparseJ.setZero();

	// Recompute lumped mass

	this->ComputeLumpedMass();
}

void FEM::ComputeLumpedMass(void)
{
	double mass;

	// Clear current lumped mass

	for (int n = 0; n < vectorNodes.size(); n++)
	{
		vectorNodes[n]->SetMass(0.0);
	}

	// Add lump mass for each tet

	for (int e = 0; e < vectorTetrahedra.size(); e++)
	{
		mass = 0.25 * vectorTetrahedra[e]->GetMassDensity() * vectorTetrahedra[e]->GetVolume();

		for (int n = 0; n < 4; n++)
		{
			vectorTetrahedra[e]->GetNode(n)->AddMass(mass);
		}
	}

	// Assemble the lumped mass matrix

	// Preallocate triplet vector for performance

	VectorTd  tripletVector;

	if (sparseM.nonZeros() > 0)
		if ((int)tripletVector.capacity() < sparseM.nonZeros())
			tripletVector.reserve(sparseM.nonZeros());

	// Iterate through nodes creating the triplets

	unsigned int nFreeNodes = (unsigned int)vectorFreeNodes.size(), i, row, col;

	for (i = 0; i < nFreeNodes; i++)
	{
		Node* node_i = vectorFreeNodes[i];
		row = col = 3 * node_i->GetId_Free();
		AssembleSparseMatrix(tripletVector, row, col, node_i->GetMass()*Matrix3d::Identity());
	}

	// Assemble the mass matrix

	sparseM = SparseMatrix<double>(3 * nFreeNodes, 3 * nFreeNodes);
	sparseM.setFromTriplets(tripletVector.begin(), tripletVector.end());
	sparseM.makeCompressed();
}

void FEM::ResetForceAndJacobian(void)
{
	int numNode = (int)vectorNodes.size();

	for (int n = 0; n < numNode; n++)
	{
		vectorNodes[n]->SetF_int(Vector3d::Zero());
		vectorNodes[n]->SetF_ext(Vector3d::Zero());
		vectorNodes[n]->SetF_dam(Vector3d::Zero());
		vectorNodes[n]->SetF_tot(Vector3d::Zero());
	}
}

void FEM::ComputeForceAndJacobian(void)
{
	ResetForceAndJacobian();

	int numFreeNodes = (int)vectorFreeNodes.size();

	int N = 3 * numFreeNodes;

	this->forces.resize(N);
	this->forces.setZero();

	// COMPUTE ------------------------------------------------------

	// Compute forces/Jacobians

	int numEle = (int) this->vectorElements.size();

#pragma omp parallel for
	for (int i = 0; i < numEle; ++i)
	{
		this->vectorElements[i]->ComputeAndStore_ForcesAndJacobian();
	}

	// ASSEMBLE -----------------------------------------------------

	// Assemble forces -------------

	for (int i = 0; i < numEle; ++i)
	{
		this->vectorElements[i]->AddForcesContribution(this->forces);
	}

	// Assemble DfDx -------------

	VectorTd  tripletVector;

	if (sparseJ.nonZeros() > 0) // Preallocate for performance
		if ((int)tripletVector.capacity() < sparseJ.nonZeros())
			tripletVector.reserve(sparseJ.nonZeros());

	for (int i = 0; i < numEle; ++i)
	{
		this->vectorElements[i]->AddJacobianContribution(tripletVector);
	}

	sparseJ = SparseMatrix<double>(3 * numFreeNodes, 3 * numFreeNodes);
	sparseJ.setFromTriplets(tripletVector.begin(), tripletVector.end());
	sparseJ.makeCompressed();

	// Assemble DfDv -------------

	sparseD = massDamping*sparseM + stiffDamping*sparseJ;
}

void FEM::CheckGlobalForce(void)
{
	double eps = 1e-6;

	int numNode = (int) this->vectorFreeNodes.size();

	int N = 3 * numNode;

	VectorXd vx_FD(N); 
	VectorXd vf_FD(N);
	this->GetPositions(vx_FD);

	for (int i = 0; i < N; ++i)
	{
		// +
		vx_FD(i) += eps;
		this->SetPositions(vx_FD);
		this->ComputePotentialEnergy();
		double energyp = this->energy;

		// -
		vx_FD(i) -= 2*eps;
		this->SetPositions(vx_FD);
		this->ComputePotentialEnergy();
		double energym = this->energy;

		// Estimate

		vf_FD(i) = (energym - energyp) / (2*eps);

		// Restore

		vx_FD(i) += eps;
	}

	// Restore

	this->SetPositions(vx_FD);

	// Compute analytical

	this->ComputeForceAndJacobian();
	VectorXd vf_AN = this->forces;
	
	// Check

	double norm_FD = vf_FD.norm();
	double norm_AN = vf_AN.norm();
	VectorXd vf_diff = vf_FD - vf_AN;
	double norm_diff = vf_diff.norm();
	
	std::cout << std::endl << "Force FD check: " << norm_diff / norm_FD << std::endl;
}

void FEM::CheckGlobalJacobian(void)
{
	double eps = 1e-6;

	int numNode = (int) this->vectorFreeNodes.size();

	int N = 3 * numNode;

	VectorXd vx_FD(N);
	MatrixXd mJ_FD(N,N);
	this->GetPositions(vx_FD);

	for (int i = 0; i < N; ++i)
	{
		// +
		vx_FD(i) += eps;
		this->SetPositions(vx_FD);
		this->ComputeForceAndJacobian();
		VectorXd forcep = this->forces;

		// -
		vx_FD(i) -= 2 * eps;
		this->SetPositions(vx_FD);
		this->ComputeForceAndJacobian();
		VectorXd forcem = this->forces;

		// Estimate

		mJ_FD.col(i) = (forcep - forcem) / (2 * eps);

		// Restore

		vx_FD(i) += eps;
	}

	// Restore

	this->SetPositions(vx_FD);

	// Compute analytical

	this->ComputeForceAndJacobian();
	SparseMatrix<double> wholeJ = sparseJ.selfadjointView<Lower>();
	MatrixXd mJ_AN = wholeJ.toDense();

	// Check

	double norm_FD = mJ_FD.norm();
	double norm_AN = mJ_AN.norm();
	MatrixXd mJ_diff = mJ_FD - mJ_AN;
	double norm_diff = mJ_diff.norm();

	std::cout << std::endl << "Jacobian FD check: " << norm_diff / norm_FD << std::endl;
}

void FEM::ComputePotentialEnergy(void)
{
	this->energy = 0;

	int numEle = (int)this->vectorElements.size();

	// COMPUTE ------------------------------------------------------

#pragma omp parallel for
	for (int i = 0; i < numEle; ++i)
	{
		this->vectorElements[i]->ComputeAndStore_Energy();
	}

	// ASSEMBLE -----------------------------------------------------

	this->energy = 0;

	for (int i = 0; i < numEle; ++i)
	{
		this->energy += this->vectorElements[i]->GetElementEnergy();
	}
}

// DEPCRAPTED - Remove
void FEM::ComputePressureForceAndJacobian(void)
{
	// Using Boyle's law => P = k/V
	double V = Face::computeVolume(this->chamberFaces);
	double P = this->GetTargetVolume() / V;

	std::cout << "*** Volume of chamber: " << V << std::endl;
	std::cout << "*** Pressure in chamber: " << P << std::endl;

	// P = F/area => F = P*area
	for (const Face& f : this->chamberFaces)
	{
		Vector3d A = f.n1->GetPosition();
		Vector3d B = f.n2->GetPosition();
		Vector3d C = f.n3->GetPosition();

		Vector3d AB = B - A;
		double AB_norm = AB.norm();
		Vector3d AC = C - A;
		double AC_norm = AC.norm();

		double cos_theta = AB.dot(AC) / (AB_norm * AC_norm);
		double sin_theta = std::sqrt(1.0 - std::pow(cos_theta, 2));

		double area = (0.5) * AB_norm * AC_norm * sin_theta;

		Vector3d faceNormal = AB.cross(AC).normalized();
		Vector3d F = faceNormal * ((area*P) / 3.0); // division by 3, because applying force to all nodes on the triangle

		f.n1->AddF_ext(F);
		f.n2->AddF_ext(F);
		f.n3->AddF_ext(F);


		// debug couts
		std::cout << "* F = <" << F.x() << "," << F.y() << "," << F.z() << ">" << std::endl;

		// normals checked for cube - they are all good
		// std::cout << "* A = <" << A.x() << "," << A.y() << "," << A.z() << ">" << std::endl;
		// std::cout << "* B = <" << B.x() << "," << B.y() << "," << B.z() << ">" << std::endl;
		// std::cout << "* C = <" << C.x() << "," << C.y() << "," << C.z() << ">" << std::endl;
		// std::cout << "* F_normal = <" << faceNormal.x() << "," << faceNormal.y() << "," << faceNormal.z() << ">" << std::endl;
	}
}

double FEM::GetTargetVolume(void) {
	return this->targetVolume; // initial pressure should be 1, so P = V/V = 1 at start
}

void FEM::IncreaseTargetVolume(void) {
	//this->pressureConstant += 0.01;
	this->targetVolume *= 1.1;
}

void FEM::DecreaseTargetVolume(void) {
	//this->pressureConstant -= 0.01;
	this->targetVolume /= 1.1;
}

double FEM::GetPotentialEnergy(void)
{
	return this->energy;
}

void FEM::GetPositions(VectorXd& x) const
{
	unsigned int nFreeNodes = (unsigned int)vectorFreeNodes.size();

	x.resize(3 * nFreeNodes);

	for (unsigned int i = 0, j = 0; i < nFreeNodes; i++, j += 3)
	{
		Vector3d pos = vectorFreeNodes[i]->GetPosition();

		x(j) = pos[0];
		x(j + 1) = pos[1];
		x(j + 2) = pos[2];
	}
}

void FEM::SetPositions(const VectorXd& x)
{
	unsigned int nFreeNodes = (unsigned int)vectorFreeNodes.size();

	for (unsigned int i = 0, j = 0; i < nFreeNodes; i++, j += 3)
	{
		vectorFreeNodes[i]->SetPosition(Vector3d(x(j), x(j + 1), x(j + 2)));
	}

	this->UpdateDeformationDependentMagnitudes();
}

void FEM::GetVelocities(VectorXd& v) const
{
	unsigned int nFreeNodes = (unsigned int)vectorFreeNodes.size();

	v.resize(3 * nFreeNodes);

	for (unsigned int i = 0, j = 0; i < nFreeNodes; i++, j += 3)
	{
		Vector3d vel = vectorFreeNodes[i]->GetVelocity();

		v(j) = vel[0];
		v(j + 1) = vel[1];
		v(j + 2) = vel[2];
	}
}

void FEM::SetVelocities(const VectorXd& v)
{
	unsigned int nFreeNodes = (unsigned int)vectorFreeNodes.size();

	for (unsigned int i = 0, j = 0; i < nFreeNodes; i++, j += 3)
	{
		vectorFreeNodes[i]->SetVelocity(Vector3d(v(j), v(j + 1), v(j + 2)));
	}
}

void FEM::SetKinematicState(const VectorXd& x, const VectorXd& v)
{
	unsigned int nFreeNodes = (unsigned int)vectorFreeNodes.size();

	for (unsigned int i = 0, j = 0; i < nFreeNodes; i++, j += 3)
	{
		Node* node_i = vectorFreeNodes[i];
		node_i->SetPosition(Vector3d(x(j), x(j + 1), x(j + 2)));
		node_i->SetVelocity(Vector3d(v(j), v(j + 1), v(j + 2)));
	}

	this->UpdateDeformationDependentMagnitudes();
}

void FEM::AssembleDynamicSystem_Matrix(SparseMatrix<double>& sparseA)
{
	const SparseMatrix<double>& sparseM = this->GetMatrix_LumpedMass();
	const SparseMatrix<double>& sparseJ = this->GetMatrix_Jacobian();
	const SparseMatrix<double>& sparseD = this->GetMatrix_Damping();

	// DEBUG

	//writeToFile(sparseM.toDense(), "MMatrix.csv");
	//writeToFile(sparseJ.toDense(), "JMatrix.csv");
	//writeToFile(sparseD.toDense(), "Dmatrix.csv");

	// TODO: Check if we can preallocate sparseA somehow for eficiency

	sparseA = sparseM + timeStep * sparseD + timeStepSquare * sparseJ;
}

void FEM::AssembleStaticSystem_Matrix(SparseMatrix<double>& sparseA)
{
	// TODO
}

void FEM::AssembleDynamicSystem_RHS(VectorXd& b)
{
	VectorXd v; // Velocity
	this->GetVelocities(v);

	b = this->GetMatrix_LumpedMass() * v + timeStep * this->GetTotalForces();
}

void FEM::AssembleStaticSystem_RHS(VectorXd& b)
{

}

void FEM::Read_FEM_State(void) // DEBUG
{
	ifstream fIn;

	fIn.open("pos_vels.txt", ios::in);
	if (!fIn.rdbuf()->is_open())
	{
		cerr << "Error: 'pos_vels.txt' file could not be opened for reading" << endl;
		return;
	}

	unsigned int nVertices, nEdges, nTetrahedra;
	fIn >> nVertices >> nEdges >> nTetrahedra;

	int id, freeNodeId, fixed, marked, originId, headId;
	double mass, volume0, volume, massDensity, determinant;
	Vector3d x0, x, v, b, f_k, f_ext, f_damp, f_Kx, f;
	Matrix3d K, A, Vm, initVm, initVm_inv;

	for (unsigned int i = 0; i < nVertices; i++)
	{
		fIn >> id >> freeNodeId >> fixed >> marked >> mass;
		fIn >> K(0, 0) >> K(0, 1) >> K(0, 2) >> K(1, 0) >> K(1, 1) >> K(1, 2) >> K(2, 0) >> K(2, 1) >> K(2, 2);
		fIn >> A(0, 0) >> A(0, 1) >> A(0, 2) >> A(1, 0) >> A(1, 1) >> A(1, 2) >> A(2, 0) >> A(2, 1) >> A(2, 2);
		fIn >> x0[0] >> x0[1] >> x0[2] >> x[0] >> x[1] >> x[2] >> v[0] >> v[1] >> v[2] >> b[0] >> b[1] >> b[2];
		fIn >> f_k[0] >> f_k[1] >> f_k[2] >> f_ext[0] >> f_ext[1] >> f_ext[2] >> f_Kx[0] >> f_Kx[1] >> f_Kx[2];
		fIn >> f_damp[0] >> f_damp[1] >> f_damp[2] >> f[0] >> f[1] >> f[2];

		if (vectorNodes[i]->GetId_Full() == id)
		{
			vectorNodes[i]->SetId_Free(freeNodeId);
			(fixed == 1 ? vectorNodes[i]->FixNode() : vectorNodes[i]->UnfixNode());
			(marked == 1 ? vectorNodes[i]->SetMarked(true) : vectorNodes[i]->SetMarked(false));
			vectorNodes[i]->SetMass(mass);

			vectorNodes[i]->SetPosition0(x0);
			vectorNodes[i]->SetPosition(x);
			vectorNodes[i]->SetVelocity(v);

			vectorNodes[i]->SetKii(K);
			vectorNodes[i]->SetAii(A);
			vectorNodes[i]->Set_bi(b);

			vectorNodes[i]->SetF_int(f_k);
			vectorNodes[i]->SetF_ext(f_ext);
			vectorNodes[i]->SetF_dam(f_damp);
			vectorNodes[i]->SetF_tot(f);
		}
		else cerr << "ERROR: Vertex ID does not correspond." << id << endl;
	}

	for (unsigned int i = 0; i < nEdges; i++)
	{
		fIn >> originId >> headId;
		fIn >> K(0, 0) >> K(0, 1) >> K(0, 2) >> K(1, 0) >> K(1, 1) >> K(1, 2) >> K(2, 0) >> K(2, 1) >> K(2, 2);
		fIn >> A(0, 0) >> A(0, 1) >> A(0, 2) >> A(1, 0) >> A(1, 1) >> A(1, 2) >> A(2, 0) >> A(2, 1) >> A(2, 2);

		if (vectorEdges[i]->GetOrigin()->GetId_Full() == originId && vectorEdges[i]->GetHead()->GetId_Full() == headId)
		{
			vectorEdges[i]->SetKij(K);
			vectorEdges[i]->SetAij(A);
		}
		else cerr << "ERROR: Origin and/or head edge IDs do not correspond." << endl;
	}

	for (unsigned int i = 0; i < nTetrahedra; i++)
	{
		fIn >> id >> volume0 >> volume >> massDensity >> determinant;
		fIn >> Vm(0, 0) >> Vm(0, 1) >> Vm(0, 2) >> Vm(1, 0) >> Vm(1, 1) >> Vm(1, 2) >> Vm(2, 0) >> Vm(2, 1) >> Vm(2, 2);
		fIn >> initVm(0, 0) >> initVm(0, 1) >> initVm(0, 2) >> initVm(1, 0) >> initVm(1, 1) >> initVm(1, 2) >> initVm(2, 0) >> initVm(2, 1) >> initVm(2, 2);
		fIn >> initVm_inv(0, 0) >> initVm_inv(0, 1) >> initVm_inv(0, 2) >> initVm_inv(1, 0) >> initVm_inv(1, 1) >> initVm_inv(1, 2) >> initVm_inv(2, 0) >> initVm_inv(2, 1) >> initVm_inv(2, 2);

		if (vectorTetrahedra[i]->GetId() == id)
		{
			vectorTetrahedra[i]->SetInitialVolume(volume0);
			vectorTetrahedra[i]->SetVolume(volume);
			vectorTetrahedra[i]->SetMassDensity(massDensity);
			vectorTetrahedra[i]->SetDeterminant(determinant);
			vectorTetrahedra[i]->SetVolumeMatrix(Vm);
			vectorTetrahedra[i]->SetInitialVolumeMatrix(initVm);
			vectorTetrahedra[i]->SetInitialVolumeMatrixInverse(initVm_inv);
		}
		else cerr << "ERROR: Tetrahedron ID does not correspond." << endl;
	}

	fIn.close();
}

void FEM::Save_FEM_State(void) // DEBUG
{
	ofstream fOut;

	fOut.open("pos_vels.txt", ios::out);
	if (!fOut.rdbuf()->is_open())
	{
		cerr << "Error: 'pos_vels.txt' file could not be opened for writing" << endl;
		return;
	}

	unsigned int nVertices = (unsigned int)vectorNodes.size();
	unsigned int nEdges = (unsigned int)vectorEdges.size();
	unsigned int nTetrahedra = (unsigned int)vectorTetrahedra.size();
	fOut << nVertices << " " << nEdges << " " << nTetrahedra << endl;

	int id, freeNodeId, fixed, marked, originId, headId;
	double mass, volume0, volume, massDensity, determinant;
	Vector3d x0, x, v, b, f_k, f_ext, f_damp, f_Kx, f;
	Matrix3d K, A, Vm, initVm, initVm_inv;

	fOut.precision(18);
	//fOut.setf(ios::fixed,ios::floatfield);

	for (unsigned int i = 0; i < nVertices; i++)
	{
		id = vectorNodes[i]->GetId_Full();
		freeNodeId = vectorNodes[i]->GetId_Free();
		fixed = (vectorNodes[i]->IsFixed() ? 1 : 0);
		marked = (vectorNodes[i]->IsMarked() ? 1 : 0);
		mass = vectorNodes[i]->GetMass();

		x0 = vectorNodes[i]->GetPosition0();
		x = vectorNodes[i]->GetPosition();
		v = vectorNodes[i]->GetVelocity();

		K = vectorNodes[i]->GetKii();
		A = vectorNodes[i]->GetAii();
		b = vectorNodes[i]->Get_bi();

		f_k = vectorNodes[i]->GetF_int();
		f_ext = vectorNodes[i]->GetF_ext();
		f_damp = vectorNodes[i]->GetF_dam();
		f = vectorNodes[i]->GetF_tot();

		fOut << id << " " << freeNodeId << " " << fixed << " " << marked << " " << mass << endl;
		fOut << K(0, 0) << " " << K(0, 1) << " " << K(0, 2) << " " << K(1, 0) << " " << K(1, 1) << " " << K(1, 2) << " " << K(2, 0) << " " << K(2, 1) << " " << K(2, 2) << endl;
		fOut << A(0, 0) << " " << A(0, 1) << " " << A(0, 2) << " " << A(1, 0) << " " << A(1, 1) << " " << A(1, 2) << " " << A(2, 0) << " " << A(2, 1) << " " << A(2, 2) << endl;
		fOut << x0[0] << " " << x0[1] << " " << x0[2] << " " << x[0] << " " << x[1] << " " << x[2] << " " << v[0] << " " << v[1] << " " << v[2] << " " << b[0] << " " << b[1] << " " << b[2] << endl;
		fOut << f_k[0] << " " << f_k[1] << " " << f_k[2] << " " << f_ext[0] << " " << f_ext[1] << " " << f_ext[2] << " " << f_Kx[0] << " " << f_Kx[1] << " " << f_Kx[2] << endl;
		fOut << f_damp[0] << " " << f_damp[1] << " " << f_damp[2] << " " << f[0] << " " << f[1] << " " << f[2] << endl;
	}

	for (unsigned int i = 0; i < nEdges; i++)
	{
		originId = vectorEdges[i]->GetOrigin()->GetId_Full();
		headId = vectorEdges[i]->GetHead()->GetId_Full();
		K = vectorEdges[i]->GetKij();
		A = vectorEdges[i]->GetAij();

		fOut << originId << " " << headId << endl;
		fOut << K(0, 0) << " " << K(0, 1) << " " << K(0, 2) << " " << K(1, 0) << " " << K(1, 1) << " " << K(1, 2) << " " << K(2, 0) << " " << K(2, 1) << " " << K(2, 2) << endl;
		fOut << A(0, 0) << " " << A(0, 1) << " " << A(0, 2) << " " << A(1, 0) << " " << A(1, 1) << " " << A(1, 2) << " " << A(2, 0) << " " << A(2, 1) << " " << A(2, 2) << endl;
	}

	for (unsigned int i = 0; i < nTetrahedra; i++)
	{
		id = vectorTetrahedra[i]->GetId();
		volume0 = vectorTetrahedra[i]->GetInitialVolume();
		volume = vectorTetrahedra[i]->GetVolume();
		massDensity = vectorTetrahedra[i]->GetMassDensity();
		determinant = vectorTetrahedra[i]->GetDeterminant();
		Vm = vectorTetrahedra[i]->GetVolumeMatrix();
		initVm = vectorTetrahedra[i]->GetInitialVolumeMatrix();
		initVm_inv = vectorTetrahedra[i]->GetInitialVolumeMatrixInverse();

		fOut << id << " " << volume0 << " " << volume << " " << massDensity << " " << determinant << endl;
		fOut << Vm(0, 0) << " " << Vm(0, 1) << " " << Vm(0, 2) << " " << Vm(1, 0) << " " << Vm(1, 1) << " " << Vm(1, 2) << " " << Vm(2, 0) << " " << Vm(2, 1) << " " << Vm(2, 2) << endl;
		fOut << initVm(0, 0) << " " << initVm(0, 1) << " " << initVm(0, 2) << " " << initVm(1, 0) << " " << initVm(1, 1) << " " << initVm(1, 2) << " " << initVm(2, 0) << " " << initVm(2, 1) << " " << initVm(2, 2) << endl;
		fOut << initVm_inv(0, 0) << " " << initVm_inv(0, 1) << " " << initVm_inv(0, 2) << " " << initVm_inv(1, 0) << " " << initVm_inv(1, 1) << " " << initVm_inv(1, 2) << " " << initVm_inv(2, 0) << " " << initVm_inv(2, 1) << " " << initVm_inv(2, 2) << endl;
	}

	fOut.close();
}

void FEM::SolveDynamicStep_SemiImplicit(void)
{
	unsigned int nSize = (unsigned int)3 * vectorFreeNodes.size();
	SparseMatrix<double> sparseA(nSize, nSize);
	VectorXd v(nSize), x(nSize), b(nSize);

	ComputeForceAndJacobian();
	AssembleDynamicSystem_RHS(b);
	AssembleDynamicSystem_Matrix(sparseA);

	std::cout << std::endl << "RHS norm: " << b.norm();
	std::cout << std::endl << "JAC norm: " << sparseA.norm();

	//VectorTd tripletsA;
	//LT_Triplet predicate;
	//EigenSparseMatrixToTriplets(sparseA.selfadjointView<Lower>(), tripletsA);
	//sort(tripletsA.begin(), tripletsA.end(), predicate);

	//int numRows = sparseA.rows();
	//int numCols = sparseA.cols();
	//int numNoZ = tripletsA.size();

	//////////////////////////////////////////

	//// Testing cuSolver

	//// Create library handles:

	//cusolverStatus_t cusolver_status;
	//cusolverSpHandle_t cusolver_handle;
	//cusolver_status = cusolverSpCreate(&cusolver_handle);
	//std::cout << "status create cusolver handle: " << cusolver_status << std::endl;

	//cusparseHandle_t cusparse_handle;
	//cusparseStatus_t cusparse_status;
	//cusparse_status = cusparseCreate(&cusparse_handle);
	//std::cout << "status create cusparse handle: " << cusparse_status << std::endl;

	//// Prepare matrix

	//vector<int> host_rows_coo;
	//vector<int> host_cols_csr;
	//vector<float> host_vals_csr;

	//EigenTripletsToCUDA(tripletsA, host_rows_coo, host_cols_csr, host_vals_csr);

	//// Prepare vectors

	//vector<float> host_x;
	//vector<float> host_b;
	//EigenVectorToCUDA(b, host_b);

	//// Reserving memory

	//// RHS and solutions

	//float *db, *dx;
	//cudaMalloc((void**)&db, numRows * sizeof(float));
	//cudaMalloc((void**)&dx, numRows * sizeof(float));

	//// Sparse matrix COO

	//float *dcsrVal;  
	//int *dcooRowInd, *dcsrColInd, *dcsrRowPtr;
	//cudaMalloc((void**)&dcsrVal, numNoZ * sizeof(float));
	//cudaMalloc((void**)&dcooRowInd, numNoZ * sizeof(int));
	//cudaMalloc((void**)&dcsrColInd, numNoZ * sizeof(int));

	//// Sparse matrix CSR

	//cudaMalloc((void**)&dcsrRowPtr, (numRows + 1) * sizeof(int));

	//// Copy RHS to device

	//cudaMemcpy(db, host_b.data(), host_b.size() * sizeof(float), cudaMemcpyHostToDevice);

	//// Copy Matrix to device in COO

	//cudaMemcpy(dcsrVal, host_vals_csr.data(), host_vals_csr.size() * sizeof(float), cudaMemcpyHostToDevice);
	//cudaMemcpy(dcooRowInd, host_rows_coo.data(), host_rows_coo.size() * sizeof(int), cudaMemcpyHostToDevice);
	//cudaMemcpy(dcsrColInd, host_cols_csr.data(), host_cols_csr.size() * sizeof(int), cudaMemcpyHostToDevice);

	//// Convert row from COO to CSR

	//cusparse_status = cusparseXcoo2csr(cusparse_handle, dcooRowInd, numNoZ, numRows, dcsrRowPtr, CUSPARSE_INDEX_BASE_ZERO);
	//
	////std::cout << "status cusparse coo2csr conversion: " << cusparse_status << std::endl;

	//// Syncronize with device

	//cudaError_t error;
	//error = cudaDeviceSynchronize();
	////std::cout << "error cudaDeviceSynchronize: " << error << std::endl;

	//// Get matrix descriptor

	//cusparseMatDescr_t descrA;
	//cusparse_status = cusparseCreateMatDescr(&descrA);
	////std::cout << "status cusparse createMatDescr: " << cusparse_status << std::endl;

	//// Solving

	//float tol = 1e-5;
	//int reorder = 0;
	//int singular = 0;

	//cusolver_status = cusolverSpScsrlsvchol(cusolver_handle, numRows, numNoZ, descrA, dcsrVal, dcsrRowPtr, dcsrColInd, db, tol, reorder, dx, &singular);

	//cudaDeviceSynchronize();

	//std::cout << "singularity (should be -1): " << singular << std::endl;

	//std::cout << "status cusolver solving (!): " << cusolver_status << std::endl;

	//host_x.resize(numRows);

	//cudaMemcpy(host_x.data(), dx, numRows * sizeof(float), cudaMemcpyDeviceToHost);

	//// relocated these 2 lines from above to solve (2):
	//cusparse_status = cusparseDestroy(cusparse_handle);
	////std::cout << "status destroy cusparse handle: " << cusparse_status << std::endl;

	//cusolver_status = cusolverSpDestroy(cusolver_handle);
	////std::cout << "status destroy cusolver handle: " << cusolver_status << std::endl;

	//cudaFree(db);
	//cudaFree(dx);
	//cudaFree(dcsrVal);
	//cudaFree(dcooRowInd);
	//cudaFree(dcsrColInd);
	//cudaFree(dcsrRowPtr);

	//CUDAToEigenVector(host_x, v);

	//////////////////////////////////////////

	if (directSolver)
	{
		llt.compute(sparseA); // The compute() method is equivalent to calling both analyzePattern() and factorize()
		v = llt.solve(b);

		cout << "sparseA.norm(): " << sparseA.norm() << endl;
		cout << "v.norm(): " << v.norm() << endl;
	}
	else
	{
		ConjugateGradient<SparseMatrix<double> > cg;

		cg.compute(sparseA);
		v.setZero();
		v = cg.solveWithGuess(b, v);
	}

	GetPositions(x);
	x = x + v * timeStep;
	SetKinematicState(x, v);
}

void FEM::SolveDynamicStep_FullImplicit(void)
{
	// TODO
}

void FEM::SolveStaticStep_NetwonRaphson(void)
{
	// TODO
}

/////////////////////////////////
//// Review before deprecate ////
////////////////////////////////
//
//

void FEM::SolveLinearSystem_Eigen(void)
{
	unsigned int nSize = (unsigned int)3 * vectorFreeNodes.size();
	SparseMatrix<double> sparseA(nSize, nSize);
	VectorXd v(nSize), x(nSize), b(nSize);

	//ComputeInternalForceJacobian();
	//AssembleA_Matrix_Eigen(sparseA);
	//ComputeRhsToEigen(b, v);

	ComputeForceAndJacobian();
	AssembleDynamicSystem_RHS(b);
	AssembleDynamicSystem_Matrix(sparseA);

	if (directSolver)
	{
		llt.compute(sparseA); // The compute() method is equivalent to calling both analyzePattern() and factorize()
		v = llt.solve(b);
	}
	else
	{
		ConjugateGradient<SparseMatrix<double> > cg;

		cg.compute(sparseA);
		//v.setZero();
		v = cg.solveWithGuess(b, v);
	}

	AddActiveConstraints_Eigen(v, b);
}

void FEM::ComputeAii_Submatrix(Node* node)
{
	// A = (M + \Delta t * C + (\Delta t)^2 * K)
	// C = \alpha * M + \beta * K
	Matrix3d K = node->GetKii();
	Matrix3d M = node->GetMass() * Matrix3d::Identity();
	Matrix3d C = massDamping * M + stiffDamping * K;
	node->SetAii(M + timeStep * C + timeStepSquare * K);
}

void FEM::ComputeAij_Submatrix(Edge* edge)
{
	// M = Matrix3d::Zero()
	// A = (M + \Delta t * C + (\Delta t)^2 * K)
	// C = \alpha * M + \beta * K
	Matrix3d K = edge->GetKij();
	Matrix3d C = stiffDamping * K;
	edge->SetAij(timeStep * C + timeStepSquare * K);
}

void FEM::AssembleA_Matrix_Eigen(SparseMatrix<double>& sparseA)
{
	unsigned int nFreeNodes = (unsigned int)vectorFreeNodes.size(), i, row, col;
	vector<Triplet<double> > tripletVector;
	vector<Edge*>::iterator it, itEnd;
	Matrix3d A33;
	Node* node_i;

	// Should be changed to sparseA.insertBack(i,j) = A33(m,n); function for efficiency,
	// but elements have to be ordered (eigen.tuxfamily.org/dox-3.0/TutorialSparse.html)
	for (i = 0; i < nFreeNodes; i++)
	{
		node_i = vectorFreeNodes[i];
		row = col = 3 * node_i->GetId_Free();

		// Compute Aii 3x3 block for each node
		ComputeAii_Submatrix(node_i);
		A33 = node_i->GetAii();

		FillTripletSparseBlock3x3(tripletVector, row, col, A33);

		// Compute Aij 3x3 block for all edges of each node
		for (it = node_i->GetAdjacentEdges().begin(), itEnd = node_i->GetAdjacentEdges().end(); it != itEnd; it++)
		{
			if (!(*it)->GetOrigin()->IsFixed() && !(*it)->GetHead()->IsFixed())
			{
				ComputeAij_Submatrix(*it);

				if (node_i->GetId_Full() == (*it)->GetOrigin()->GetId_Full())
				{
					col = 3 * (*it)->GetHead()->GetId_Free();
					A33 = (*it)->GetAij();
				}
				else
				{
					col = 3 * (*it)->GetOrigin()->GetId_Free();
					A33 = (*it)->GetAij().transpose();
				}

				FillTripletSparseBlock3x3(tripletVector, row, col, A33);
			}
		}
	}
	sparseA.setFromTriplets(tripletVector.begin(), tripletVector.end());
}

void FEM::SetRhsToEigen(VectorXd& b)
{
	unsigned int nFreeNodes = (unsigned int)vectorFreeNodes.size();

	for (unsigned int i = 0, j = 0; i < nFreeNodes; i++, j += 3)
	{
		Vector3d _b = vectorFreeNodes[i]->Get_bi();

		b(j) = _b[0];
		b(j + 1) = _b[1];
		b(j + 2) = _b[2];
	}
}

void FEM::ComputeRhsToEigen(VectorXd& _b, VectorXd& _v)
{
	unsigned int nFreeNodes = (unsigned int)vectorFreeNodes.size();

	// b = M * v + \Delta t * (- K * x - f_k + f_ext)
	for (unsigned int i = 0, j = 0; i < nFreeNodes; i++, j += 3)
	{
		Node* node_i = vectorFreeNodes[i];

		//node_i->CalculateF_ext();
		//node_i->CalculateF_damp(massDamping);

		Vector3d totalForce = -node_i->GetF_int() + node_i->GetF_ext() + node_i->GetF_dam();

		node_i->SetF_tot(totalForce);

		Vector3d Mv = node_i->GetMass() * node_i->GetVelocity();
		Vector3d b = Mv + timeStep * totalForce;

		node_i->Set_bi(b);
		_b(j) = b[0];
		_b(j + 1) = b[1];
		_b(j + 2) = b[2];

		Vector3d v = node_i->GetVelocity();
		_v(j) = v[0];
		_v(j + 1) = v[1];
		_v(j + 2) = v[2];
	}
}

void FEM::RhsPositionsAndVelocitiesFromEigen(const VectorXd& b, const VectorXd& x, const VectorXd& v)
{
	unsigned int nFreeNodes = (unsigned int)vectorFreeNodes.size();

	for (unsigned int i = 0, j = 0; i < nFreeNodes; i++, j += 3)
	{
		Node* node_i = vectorFreeNodes[i];
		node_i->Set_bi(Vector3d(b(j), b(j + 1), b(j + 2)));
		node_i->SetPosition(Vector3d(x(j), x(j + 1), x(j + 2)));
		node_i->SetVelocity(Vector3d(v(j), v(j + 1), v(j + 2)));
	}
}

void FEM::GetForces(VectorXd& f) const
{
	unsigned int nFreeNodes = (unsigned int)vectorFreeNodes.size();

	for (unsigned int i = 0, j = 0; i < nFreeNodes; i++, j += 3)
	{
		Vector3d totalForce = vectorFreeNodes[i]->GetF_tot();

		f(j) = totalForce[0];
		f(j + 1) = totalForce[1];
		f(j + 2) = totalForce[2];
	}
}

void FEM::FormulateActiveConstraints(unsigned int& _nActive, unsigned int& _nStrainLimiting, unsigned int& _nContacts, VectorXd& _C0, SparseMatrix<double>& _J, SparseMatrix<double>& _JT, vector<unsigned int>& inds, double& _error)
{
	_nActive = _nStrainLimiting = 0;
	_error = 0.0;

	switch (constraintsUsed)
	{
	case STRAIN_LIMITING:
		FormulateStrainLimitingConstraints_Eigen(_nStrainLimiting, _C0, _J, _JT);
		_error = CalculateNonLinearConstraintsError(_nStrainLimiting);
		_nActive = _nStrainLimiting;
		break;
	default:
		break;
	}
}

void FEM::FormulateStrainLimitingConstraints_Eigen(unsigned int& _nStrainLimiting, VectorXd& _C0, SparseMatrix<double>& _J, SparseMatrix<double>& _JT)
{
	unsigned int nTetras = (unsigned int)vectorTetrahedra.size();
	unsigned int nCols = 3 * (unsigned int)vectorFreeNodes.size();

	// Calculate the number of active strain limiting constraints
	_nStrainLimiting = 0;
	for (unsigned int e = 0; e < nTetras; e++)
	{
		Tetrahedron* tetra = vectorTetrahedra[e];
		tetra->CalculateConstraints();
		VectorXd C = tetra->GetConstraints();

		for (unsigned int k = 0; k < nConstraints; k++)
		{
			if (C(k) < 0.0) _nStrainLimiting++;
		}
	}

	if (_nStrainLimiting > 0)
	{
		_J.resize(_nStrainLimiting, nCols);
		_C0.resize(_nStrainLimiting);
		std::vector<Triplet<double> > _Jentries;

		for (unsigned int e = 0, row = 0, l = 0; e < nTetras; e++)
		{
			Tetrahedron* tetra = vectorTetrahedra[e];
			tetra->AssembleJ_active(_Jentries, row); // CalculateConstraints() has to be called before

			VectorXd C = tetra->GetConstraints();

			for (unsigned int k = 0; k < nConstraints; k++)
			{
				if (C(k) < 0.0) // C(k) is active
				{
					_C0(l++) = C(k);
				}
			}
		}

		_J.setFromTriplets(_Jentries.begin(), _Jentries.end());
		_JT = _J.transpose();
	}
}

double FEM::CalculateNonLinearConstraintsError(unsigned int& _nStrainLimiting)
{
	double error = 0.0;

	if (_nStrainLimiting > 0)
	{
		unsigned int nTetras = (unsigned int)vectorTetrahedra.size();

		for (unsigned int e = 0; e < nTetras; e++)
		{
			VectorXd C = vectorTetrahedra[e]->GetConstraints();

			for (unsigned int k = 0; k < nConstraints; k++)
			{
				if (C(k) < 0.0) error -= C(k);
			}
		}
	}

	return initialEdgeLength * error; // 'fabs' is not needed as negative error has been substracted
}

void solveLCP_GS(const MatrixXd& A, VectorXd& x, const VectorXd& b, unsigned int maxSteps, double tol)
{
	unsigned int iter = 0, n = b.size();
	double error = 0.0;

	x.resize(n); x.setZero();

	do {
		error = 0.0;

		//Gauss-Seidel iteration with projection
		for (unsigned int i = 0; i < n; i++)
		{
			double Blambda = 0.0;

			for (unsigned int j = 0; j < i; j++)
			{
				Blambda += A(i, j) * x(j);
			}
			for (unsigned int j = i + 1; j < n; j++)
			{
				Blambda += A(i, j) * x(j);
			}

			double lambda_i = fabs(A(i, i)) < 1e-16 ? 0.0 : (b(i) - Blambda) / A(i, i);
			lambda_i = lambda_i < 0.0 ? 0.0 : lambda_i;

			error += (lambda_i - x(i)) * (lambda_i - x(i));
			x(i) = lambda_i;
		}
		iter++;
	} while (iter < maxSteps && error > tol);
}

void solveLCP_GS(const MatrixXd& A, VectorXd& x, const VectorXd& b, unsigned int maxSteps,
	unsigned int nStrainLimiting, unsigned int nContacts, double tolStrainLimiting)
{
	unsigned int iter = 0, n = b.size();
	bool strainLimitingConverged;
	double diffSizeStrainLimiting, currentSizeStrainLimiting;

	x.resize(n); x.setZero();

	do {
		diffSizeStrainLimiting = currentSizeStrainLimiting = 0.0;

		//Gauss-Seidel iteration with projection
		for (unsigned int i = 0; i < n; i++)
		{
			double Blambda = 0.0;

			for (unsigned int j = 0; j < i; j++)
			{
				Blambda += A(i, j) * x(j);
			}
			for (unsigned int j = i + 1; j < n; j++)
			{
				Blambda += A(i, j) * x(j);
			}

			double lambda_i = fabs(A(i, i)) < ZERO_THRESHOLD ? 0.0 : (b(i) - Blambda) / A(i, i);

			lambda_i = lambda_i < 0.0 ? 0.0 : lambda_i;

			diffSizeStrainLimiting += (lambda_i - x(i)) * (lambda_i - x(i));
			currentSizeStrainLimiting += lambda_i * lambda_i;

			x(i) = lambda_i;
		}

		strainLimitingConverged = (currentSizeStrainLimiting < ZERO_THRESHOLD || diffSizeStrainLimiting / currentSizeStrainLimiting < tolStrainLimiting) ? true : false;
		//strainLimitingConverged = diffSizeStrainLimiting  < 10e-10 ? true : false;
		iter++;
	} while (iter < maxSteps && !strainLimitingConverged);
	//cout << "Exit LCP with " << iter << " iterations" << endl;
}


void FEM::AddActiveConstraints_Eigen(VectorXd& _v, VectorXd& _b)
{
	unsigned int nActive = 0, nStrainLimiting = 0, nContacts = 0;
	unsigned int nSize = 3 * (unsigned int)vectorFreeNodes.size();

	//method = (constraintsUsed == NONE ? UNCONSTRAINED : LINE_SEARCH); // By default, constraints' method is Line-search

	VectorXd v_free(nSize), x(nSize);
	MatrixXd A_inv_x_J_T;
	SparseMatrix<double> J, JT;
	VectorXd C_x0, C_x, lambda, dv;

	vector<unsigned int> inds;
	double error_before = 0.0, error_middle = 0.0, error_after = 0.0, error_after2 = 0.0;
	double error = 0.0, error_new = 0.0, delta = 1.0;

	if (constraintsMethod != UNCONSTRAINED && constraintsMethod != LINE_SEARCH)
	{
		FormulateActiveConstraints(nActive, nStrainLimiting, nContacts, C_x0, J, JT, inds, error_new);
		v_free = _v;
	}

	switch (constraintsMethod)
	{
	case UNCONSTRAINED:
		GetPositions(x);
		x = x + _v * timeStep;
		SetKinematicState(x, _v);
		break;

	case POSITIONS:
		GetPositions(x);

		if (nActive > 0)
		{
			A_inv_x_J_T = llt.solve((Eigen::MatrixXd)JT);
			MatrixXd A_lcp = J * A_inv_x_J_T;
			VectorXd b_lcp = -J * v_free - C_x0 * invTimeStep;
			solveLCP_GS(A_lcp, lambda, b_lcp, 100, 0.0);

			_b = _b + JT * lambda;
			_v = llt.solve(_b);
		}

		x = x + _v * timeStep;
		RhsPositionsAndVelocitiesFromEigen(_b, x, _v);
		break;

	case VELOCITIES:
		GetPositions(x);

		if (nActive > 0)
		{
			A_inv_x_J_T = llt.solve((Eigen::MatrixXd)JT);
			MatrixXd A_lcp = J * A_inv_x_J_T;
			VectorXd b_lcp = -J * v_free;
			solveLCP_GS(A_lcp, lambda, b_lcp, 100, 0.0);

			_b = _b + JT * lambda;
			_v = llt.solve(_b);
		}

		x = x + _v * timeStep;
		RhsPositionsAndVelocitiesFromEigen(_b, x, _v);
		break;

	case LINE_SEARCH:

		//cout << endl << endl << "BEGIN of simulation step" << endl;

		GetPositions(x);
		v_free = _v;

		x = x + v_free * timeStep;
		SetPositions(x);

		// C(x) = C(x0 + dt * v_free) >= 0
		FormulateActiveConstraints(nActive, nStrainLimiting, nContacts, C_x, J, JT, inds, error_new);

		if (nActive > 0)
		{
			do {
				FormulateActiveConstraints(nActive, nStrainLimiting, nContacts, C_x, J, JT, inds, error_new);

				A_inv_x_J_T = llt.solve((Eigen::MatrixXd)JT);
				MatrixXd B = J * A_inv_x_J_T;
				VectorXd vL = -C_x * invTimeStep;

				//cout << "N.Active Constraints: " << nActive;
				solveLCP_GS(B, lambda, vL, 100, nStrainLimiting, nContacts, 1e-8); //Projected Gauss-Seidel. Tolerance = 1e-10 (Strain Limiting)

				error = error_new;

				VectorXd dv0 = A_inv_x_J_T * lambda;

				delta = 1.0;
				do {
					dv = dv0 * delta;
					SetPositions(x + dv * timeStep);

					// C(x) = C(x + dt * dv) >= 0
					FormulateActiveConstraints(nActive, nStrainLimiting, nContacts, C_x, J, JT, inds, error_new);
					//cout << "Line search iteration with " << error_new << " error_new, " << delta << " delta" << endl;

					delta *= 0.5;
				} while (error_new > error && error_new > LS_THRESHOLD);

				_v += dv;
				x = x + dv * timeStep;
				SetPositions(x);
				//cout << "SQP iteration with " << error_new << " error_new" << endl;

			} while (error_new > LS_THRESHOLD);
		}

		SetVelocities(_v);
		break;

	default:
		break;
	}
}

//void FEM::ComputeInternalForceAndJacobian(void)
//{
//	// Interal elastic forces and Jacobian
//
//	for (unsigned int e = 0; e < vectorTetrahedra.size(); e++)
//	{
//		int pos = 0;
//
//		// Stiffness Warping: K'nm = R * Knm * R^-1 = R * Knm * R^T
//		Matrix3d R = (stiffnessWarping ? vectorTetrahedra[e]->CalculateRotation() : Matrix3d::Identity());
//		Matrix3d R_T = R.transpose();
//
//		for (int m = 0; m < 4; m++)
//		{
//			Vector3d f = Vector3d::Zero();
//			Matrix3d K_mm = vectorTetrahedra[e]->GetK_nm(m, m);
//			Matrix3d K_warp = R * K_mm * R_T;
//
//			vectorTetrahedra[e]->GetNode(m)->AddKii(K_warp);
//
//			// Undeformed force
//			f += K_mm * vectorTetrahedra[e]->GetNode(m)->GetPosition0();
//
//			for (int n = m + 1; n < 4; n++)
//			{
//				Matrix3d K_nm = vectorTetrahedra[e]->GetK_nm(m, n);
//				Matrix3d K_warp = R * K_nm * R_T;
//
//				// Bug fixed: global id compared in order to fill the proper upper triangular matrix
//				if (vectorTetrahedra[e]->GetNode(m)->GetId() < vectorTetrahedra[e]->GetNode(n)->GetId())
//				{
//					vectorTetrahedra[e]->GetEdge(pos++)->AddKij(K_warp);
//				}
//				else
//				{
//					vectorTetrahedra[e]->GetEdge(pos++)->AddKij(K_warp.transpose());
//				}
//
//				f += K_nm * vectorTetrahedra[e]->GetNode(n)->GetPosition0();
//				Vector3d f_transpose = K_nm.transpose() * vectorTetrahedra[e]->GetNode(m)->GetPosition0();
//
//				vectorTetrahedra[e]->GetNode(n)->AddF_k(R*(f_transpose)); // Updates lower triangular matrix
//			}
//
//			vectorTetrahedra[e]->GetNode(m)->AddF_k(R*(f));
//		}
//	}
//
//	unsigned int nFreeNodes = (unsigned int)vectorFreeNodes.size();
//
//	for (unsigned int i = 0, j = 0; i < nFreeNodes; i++, j += 3)
//	{
//		vector<Edge*>::iterator it;
//		Node* node_i = vectorFreeNodes[i];
//
//		Vector3d f_k = node_i->GetKii() * node_i->GetPosition(); // Node contribution must be added
//
//		for (it = node_i->GetAdjacentEdges().begin(); it != node_i->GetAdjacentEdges().end(); it++)
//		{
//			if (node_i->GetId() == (*it)->GetOrigin()->GetId())
//			{
//				f_k += (*it)->GetKij() * (*it)->GetHead()->GetPosition();
//			}
//			else
//			{
//				f_k += (*it)->GetKij().transpose() * (*it)->GetOrigin()->GetPosition(); // (K_ij)^T must be used instead of K_ij
//			}
//		}
//
//		node_i->AddF_k(-f_k);
//	}
//}

//void FEM::AssembleMatrix_Jacobian(SparseMatrix<double>& sparseJ)
//{
//	unsigned int nFreeNodes = (unsigned int)vectorFreeNodes.size(), i, row, col;
//	vector< Triplet<double> > tripletVector;
//
//	// Preallocate triplet vector for performance
//
//	if (sparseJ.nonZeros() > 0)
//		if ((int)tripletVector.capacity() < sparseJ.nonZeros())
//			tripletVector.reserve(sparseJ.nonZeros());
//
//	Matrix3d A33;
//	Node* node_i;
//
//	vector<Edge*>::iterator itCur, itEnd;
//
//	for (i = 0; i < nFreeNodes; i++)
//	{
//		node_i = vectorFreeNodes[i];
//		row = col = 3 * node_i->GetFreeNodeId();
//		FillTripletSparseBlock3x3(tripletVector, row, col, node_i->GetKii());
//
//		for (itCur = node_i->GetAdjacentEdges().begin(), itEnd = node_i->GetAdjacentEdges().end(); itCur != itEnd; itCur++)
//		{
//			if (!(*itCur)->GetOrigin()->IsFixed() && !(*itCur)->GetHead()->IsFixed())
//			{
//				if (node_i->GetId() == (*itCur)->GetOrigin()->GetId())
//				{
//					col = 3 * (*itCur)->GetHead()->GetFreeNodeId();
//					A33 = (*itCur)->GetKij();
//				}
//				else
//				{
//					col = 3 * (*itCur)->GetOrigin()->GetFreeNodeId();
//					A33 = (*itCur)->GetKij().transpose();
//				}
//
//				FillTripletSparseBlock3x3(tripletVector, row, col, A33);
//			}
//		}
//	}
//
//	sparseJ = SparseMatrix<double>(3 * nFreeNodes, 3 * nFreeNodes);
//	sparseJ.setFromTriplets(tripletVector.begin(), tripletVector.end());
//	sparseJ.makeCompressed();
//}

//void FEM::ResetForceAndJacobian(void)
//{
//	vector<Edge*>::iterator it;
//
//	for (unsigned int n = 0; n < vectorNodes.size(); n++)
//	{
//		vectorNodes[n]->SetF_int(Vector3d::Zero());
//		vectorNodes[n]->SetF_ext(Vector3d::Zero());
//		vectorNodes[n]->SetF_dam(Vector3d::Zero());
//		vectorNodes[n]->SetF_tot(Vector3d::Zero());
//
//		for (it = vectorNodes[n]->GetAdjacentEdges().begin(); it != vectorNodes[n]->GetAdjacentEdges().end(); it++)
//		{
//			(*it)->SetKij(Matrix3d::Zero());
//		}
//	}
//}

void FEM::FillTripletSparseBlock3x3(vector<Triplet<double> >& _tripletVector, unsigned int row, unsigned int col, const Matrix3d& m33) const
{
	_tripletVector.push_back(Triplet<double>(row, col, m33(0, 0)));
	_tripletVector.push_back(Triplet<double>(row, col + 1, m33(0, 1)));
	_tripletVector.push_back(Triplet<double>(row, col + 2, m33(0, 2)));

	_tripletVector.push_back(Triplet<double>(row + 1, col, m33(1, 0)));
	_tripletVector.push_back(Triplet<double>(row + 1, col + 1, m33(1, 1)));
	_tripletVector.push_back(Triplet<double>(row + 1, col + 2, m33(1, 2)));

	_tripletVector.push_back(Triplet<double>(row + 2, col, m33(2, 0)));
	_tripletVector.push_back(Triplet<double>(row + 2, col + 1, m33(2, 1)));
	_tripletVector.push_back(Triplet<double>(row + 2, col + 2, m33(2, 2)));
}

//void FEM::ComputeForceAndJacobian(void)
//{
//	ResetForceAndJacobian();
//
//	this->ComputeInternalForceAndJacobian();
//	this->ComputeExternalForceAndJacobian();
//	this->ComputeDampingForceAndJacobian();
//	//this->ComputePressureForceAndJacobian();
//
//	// Compute total force (store at nodes)
//
//	unsigned int nFreeNodes = (unsigned int)vectorFreeNodes.size();
//	for (unsigned int i = 0; i < nFreeNodes; i++)
//	{
//		Node* node_i = vectorFreeNodes[i];
//
//		node_i->SetF_Total(node_i->GetF_k() + node_i->GetF_ext() + node_i->GetF_damp());
//	}
//
//	// Assemble total Jacobian matrix
//
//	this->AssembleMatrix_Jacobian(this->sparseJ);
//
//	// Assemble total Damping matrix
//
//	this->AssembleMatrix_Damping(this->sparseD);
//}

void FEM::ComputeInternalForceAndJacobian(void)
{
	// Internal elastic forces and Jacobian

	for (unsigned int e = 0; e < vectorTetrahedra.size(); e++)
	{
		// Compute new rotation

		Matrix3d R = (stiffnessWarping ? vectorTetrahedra[e]->CalculateRotation() : Matrix3d::Identity());

		// Assemble rotations

		MatrixXd Re = MatrixXd(12, 12);

		Re.setZero();
		for (int i = 0; i < 4; ++i)
			Re.block<3, 3>(3 * i, 3 * i) = R;

		// Assemble deformations

		VectorXd vx = VectorXd(12);
		VectorXd v0 = VectorXd(12);
		for (int i = 0; i < 4; ++i)
		{
			Node* node_i = vectorTetrahedra[e]->GetNode(i);
			Vector3d posx = node_i->GetPosition();
			Vector3d pos0 = node_i->GetPosition0();
			vx.block<3, 1>(3 * i, 0) = posx;
			v0.block<3, 1>(3 * i, 0) = pos0;
		}

		// Compute elastic forces

		// Explanation: 
		// 1. Compute tetrahedron rotation T ~ R*T0
		// 2. The deformed configuration is rotated back to material R^T*x
		// 3. Compute deformed and material difference is compute u = R^T*x - x0
		// 4. Compute force in material space based on deformation f0 = K*u
		// 5. Rotate the force forward to deformation spaces R*f0
		// Compressed: f = R*K*(K^T*x - x0)

		const MatrixXd& Ke = vectorTetrahedra[e]->GetK();
		VectorXd vf = -Re*Ke*(Re.transpose()*vx - v0);

		// Assemble elastic forces

		for (int i = 0; i < 4; ++i)
		{
			vectorTetrahedra[e]->GetNode(i)->AddF_int(vf.block<3, 1>(3 * i, 0));
		}

		// Assemble elastic Jacobian

		vectorTetrahedra[e]->setKWarped(Re*Ke*Re.transpose());
	}
}

void FEM::ComputeExternalForceAndJacobian(void)
{
	unsigned int nFreeNodes = (unsigned int)vectorFreeNodes.size();

	// External forces

	for (unsigned int i = 0, j = 0; i < nFreeNodes; i++, j += 3)
	{
		vectorFreeNodes[i]->SetF_ext(Vector3d(0.0, -(vectorFreeNodes[i]->GetMass() * GRAVITY), 0.0));
	}

	// External Jacobian

	// No external forces Jacobian to compute
}

void FEM::ComputeDampingForceAndJacobian(void)
{
	unsigned int nFreeNodes = (unsigned int)vectorFreeNodes.size();

	// Damping forces

	for (unsigned int i = 0, j = 0; i < nFreeNodes; i++, j += 3)
	{
		vectorFreeNodes[i]->SetF_dam(-vectorFreeNodes[i]->GetMass() * massDamping * vectorFreeNodes[i]->GetVelocity());
	}

	// Damping Jacobian

	// No damping forces Jacobian to compute
}

void FEM::AssembleMatrix_Jacobian(SparseMatrix<double>& sparseJ)
{
	unsigned int nFreeNodes = (unsigned int)vectorFreeNodes.size(), row, col;

	VectorTd  tripletVector;

	// Preallocate triplet vector for performance

	if (sparseJ.nonZeros() > 0)
		if ((int)tripletVector.capacity() < sparseJ.nonZeros())
			tripletVector.reserve(sparseJ.nonZeros());

	int nTets = (int)vectorTetrahedra.size();

	for (int e = 0; e < nTets; ++e)
	{
		const MatrixXd& KWarped = vectorTetrahedra[e]->GetKWarped();

		for (int i = 0; i < 4; ++i)
		{
			Node* node_i = vectorTetrahedra[e]->GetNode(i);
			int freeId_i = node_i->GetId_Free();
			if (freeId_i == -1) continue;

			for (int j = 0; j < 4; ++j)
			{
				Node* node_j = vectorTetrahedra[e]->GetNode(j);
				int freeId_j = node_j->GetId_Free();
				if (freeId_j == -1) continue;

				if (freeId_j > freeId_i)
					continue; // Ignore

				row = 3 * freeId_i;
				col = 3 * freeId_j;

				FillTripletSparseBlock3x3(tripletVector, row, col, KWarped.block<3, 3>(3 * i, 3 * j));
			}
		}
	}

	sparseJ = SparseMatrix<double>(3 * nFreeNodes, 3 * nFreeNodes);
	sparseJ.setFromTriplets(tripletVector.begin(), tripletVector.end());
	sparseJ.makeCompressed();
}

void FEM::AssembleMatrix_Damping(SparseMatrix<double>& sparseD)
{
	sparseD = stiffDamping*sparseJ + massDamping*sparseM;
}

void FEM::AssembleMatrix_LumpedMass(SparseMatrix<double>& sparseM)
{
	// Preallocate triplet vector for performance

	VectorTd  tripletVector;

	if (sparseM.nonZeros() > 0)
		if ((int)tripletVector.capacity() < sparseM.nonZeros())
			tripletVector.reserve(sparseM.nonZeros());

	// Iterate through nodes creating the triplets

	unsigned int nFreeNodes = (unsigned int)vectorFreeNodes.size(), i, row, col;

	for (i = 0; i < nFreeNodes; i++)
	{
		Node* node_i = vectorFreeNodes[i];
		row = col = 3 * node_i->GetId_Free();
		AssembleSparseMatrix(tripletVector, row, col, node_i->GetMass()*Matrix3d::Identity());
	}

	// Assemble the mass matrix

	sparseM = SparseMatrix<double>(3 * nFreeNodes, 3 * nFreeNodes);
	sparseM.setFromTriplets(tripletVector.begin(), tripletVector.end());
	sparseM.makeCompressed();
}

//
//
/////////////////////////////////
//// Review before deprecate ////
////////////////////////////////