//=============================================================================
//
//   Co-Rotational Finite Element Method source code
//   Based on Physics-Based Animation book, chapter 10
//
//   Based on original implementation by
//							Miguel A. Otaduy,    URJC Madrid
//							Alvaro G. Perez,     URJC Madrid
//							and Javier S. Zurdo, URJC Madrid
//
//	Authors:
//			Krisztian Amit Birkas, McGill Uni.
//			Jesus Perez Rodriguez, IST Austria
//
//=============================================================================

#include "EnergyElement_StVKFEM.h"

#include "FEM.h"

#include "Tetrahedron.h"

EnergyElement_StVKFEM::EnergyElement_StVKFEM(FEM* fem, Tetrahedron* tetrahedron) : EnergyElement(fem)
{
	energy = 0;
	forces = VectorXd(12);
	jacobian = MatrixXd(12, 12);

	this->tetrahedron = tetrahedron;
}

void EnergyElement_StVKFEM::Initialize()
{
	double E = this->fem->GetYoungModulus();
	double u = this->fem->GetPoissonRatio();
	lame1 = (E*u) / ((1 + u)*(1 - 2*u));
	lame2 = E / (2 * (1 + u));
}
 
void EnergyElement_StVKFEM::ComputeAndStore_Energy()
{
	// Initialize nodes
	Vector3d x1 = tetrahedron->GetNode(0)->GetPosition();
	Vector3d x2 = tetrahedron->GetNode(1)->GetPosition();
	Vector3d x3 = tetrahedron->GetNode(2)->GetPosition();
	Vector3d x4 = tetrahedron->GetNode(3)->GetPosition();

	// Initialize H^-1

	const Matrix3d& V0i = tetrahedron->GetInitialVolumeMatrixInverse();
	double Hi[3][3];
	for (int i = 0; i < 3; ++i)
		for (int j = 0; j < 3; ++j)
			Hi[i][j] = V0i(i, j);

	// Initialize volume

	double V0 = tetrahedron->GetInitialVolume();

	// Compute energy

	{
#include "..\FEM\Maple\CSTetrahedronStVKEnergy.mcg";

		this->energy = t94;
	}
}

void EnergyElement_StVKFEM::ComputeAndStore_ForcesAndJacobian()
{
	// Initialize nodes
	Vector3d x1 = tetrahedron->GetNode(0)->GetPosition();
	Vector3d x2 = tetrahedron->GetNode(1)->GetPosition();
	Vector3d x3 = tetrahedron->GetNode(2)->GetPosition();
	Vector3d x4 = tetrahedron->GetNode(3)->GetPosition();

	// Initialize H^-1

	const Matrix3d& V0i = tetrahedron->GetInitialVolumeMatrixInverse();
	double Hi[3][3];
	for (int i = 0; i < 3; ++i)
		for (int j = 0; j < 3; ++j)
			Hi[i][j] = V0i(i, j);

	// Initialize volume

	double V0 = tetrahedron->GetInitialVolume();

	// Compute force

	{
		double vfx[12];

#include "..\FEM\Maple\CSTetrahedronStVKForce.mcg";

		for (int i = 0; i < 12; ++i)
			this->forces(i) = vfx[i];
	}

	// Compute Jacobian

	{
		double mJx[12][12];

#include "..\FEM\Maple\CSTetrahedronStVKJacobian.mcg";

		for (int i = 0; i < 12; ++i)
			for (int j = 0; j < 12; ++j)
				this->jacobian(i,j) = mJx[i][j];
	}
}

void EnergyElement_StVKFEM::AddForcesContribution(VectorXd& totalForces)
{
	for (int i = 0; i < 4; ++i)
	{
		Node* node_i = tetrahedron->GetNode(i);

		int freeId = node_i->GetId_Free();

		// Is simulated?
		if (freeId < 0)
			continue;

		Vector3d nodeForce = forces.block<3, 1>(3 * i, 0);
		totalForces.block<3, 1>(3 * freeId, 0) += nodeForce;
	}
}

void EnergyElement_StVKFEM::AddJacobianContribution(VectorTd & totalJacobian)
{
	for (int i = 0; i < 4; ++i)
	{
		Node* node_i = tetrahedron->GetNode(i);
		int freeId_i = node_i->GetId_Free();
		if (freeId_i < 0) continue;

		for (int j = 0; j < 4; ++j)
		{
			Node* node_j = tetrahedron->GetNode(j);
			int freeId_j = node_j->GetId_Free();
			if (freeId_j < 0) continue;

			// Is upper triangular?
			if (freeId_i < freeId_j)
				continue;

			AssembleSparseMatrix(totalJacobian, 3 * freeId_i, 3 * freeId_j, jacobian.block<3, 3>(3 * i, 3 * j));
		}
	}
}
