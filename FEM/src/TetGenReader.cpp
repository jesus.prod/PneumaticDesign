#include <fstream>
#include <sstream>
#include <iostream>
#include <algorithm>

#include <cstdio>
#include <ctime>

#include "TetGenReader.h"

// Data structures used only internally -- START

struct TetGenNode {
	int index;
	double x;
	double y;
	double z;

	TetGenNode(int i, double x, double y, double z) {
		index = i;
		this->x = x;
		this->y = y;
		this->z = z;
	}

	bool Equals(TetGenNode a) {
		double distanceSquared =
			std::pow(x - a.x, 2) +
			std::pow(y - a.y, 2) +
			std::pow(z - a.z, 2);

		return distanceSquared < 0.0001; // 0.001 distance tolerated
	}

	Eigen::Vector3d GetPosition() const {
		return Eigen::Vector3d(this->x, this->y, this->z);
	}

	bool Equals(TetGenNode a, double precision) {
		double distanceSquared =
			std::pow(this->x - a.x, 2) +
			std::pow(this->y - a.y, 2) +
			std::pow(this->z - a.z, 2);

		return distanceSquared < precision;
	}
};
struct TetGenFace {
	int A;
	int B;
	int C;

	TetGenFace(int A, int B, int C) {
		this->A = A;
		this->B = B;
		this->C = C;
	}
};
struct TetGenTetrahedron {
	int A;
	int B;
	int C;
	int D;

	TetGenTetrahedron(int A, int B, int C, int D) {
		this->A = A;
		this->B = B;
		this->C = C;
		this->D = D;
	}
};
struct TetGenEdge {
	int A;
	int B;

	TetGenEdge(int A, int B) {
		this->A = A;
		this->B = B;
	}
};
// Data structures used only internally -- END

// Function declarations:
void TetGenReader::readTetgenObject(std::string filename, FEM* &fem, std::vector<Tetrahedron*> &fem_tets) {
	std::vector<TetGenNode> nodes = readNodes(filename + ".1.node");
	std::vector<TetGenFace> faces = readFaces(filename + ".1.face");
	std::vector<TetGenTetrahedron> tets = readTetrahedrons(filename + ".1.ele");
}

void TetGenReader::readPoly(std::string filename1, FEM* &fem, std::vector<Tetrahedron*> &fem_tets) {
	std::vector<TetGenNode> nodes = readNodes(filename1 + ".1.node");
	std::vector<TetGenTetrahedron> tets = readTetrahedrons(filename1 + ".1.ele");
	std::vector<TetGenEdge> edges = readEdges(filename1 + ".1.edge");

	std::vector<TetGenFace> faces;
	std::vector<TetGenFace> chamberFaces;
	std::vector<TetGenFace> notChamberFaces;
	readFacesPoly(filename1 + ".1.face", faces, chamberFaces, notChamberFaces);

	convertIndexingToStartFrom0(nodes, faces, chamberFaces, notChamberFaces, tets, edges);

	addToFEM(nodes, faces, tets, edges, chamberFaces, notChamberFaces, fem, fem_tets);
}

void TetGenReader::readAndCombineTetGenObjects(std::string filename1, std::string filename2, FEM* &fem, std::vector<Tetrahedron*> &fem_tets) {
	std::vector<TetGenNode> nodes = readNodes(filename1 + ".1.node");
	std::vector<TetGenFace> faces = readFaces(filename1 + ".1.face");
	std::vector<TetGenTetrahedron> tets = readTetrahedrons(filename1 + ".1.ele");
	std::vector<TetGenEdge> edges = readEdges(filename1 + ".1.edge");

	std::vector<TetGenNode> nodesChamber = readNodes(filename2 + ".1.node");
	std::vector<TetGenFace> facesChamber = readFaces(filename2 + ".1.face");
	std::vector<TetGenTetrahedron> tetsChamber = readTetrahedrons(filename2 + ".1.ele");
	std::vector<TetGenEdge> edgesChamber = readEdges(filename2 + ".1.edge");

	if (nodesChamber.size() > nodes.size()) {
		std::swap(nodes, nodesChamber);
		std::swap(faces, facesChamber);
		std::swap(tets, tetsChamber);
		std::swap(edges, edgesChamber);
	}
	
	convertIndexingToStartFrom0(nodes, faces, tets, edges);

	Eigen::Vector3d centerOfMass(0, 0, 0);
	for (int i = 0; i < nodes.size(); ++i) {
		centerOfMass += Eigen::Vector3d(nodes[i].x, nodes[i].y, nodes[i].z);
	}
	centerOfMass /= nodes.size();
	
	//std::vector<int> matching = matchNodes(nodes, nodesChamber);
	std::vector<int> matching = matchNodesUsingConvexHull(nodes, nodesChamber);

	std::vector<TetGenFace> chamberFaces;
	std::vector<TetGenFace> notChamberFaces;
	for (int f_i = 0; f_i < faces.size(); ++f_i) {
		std::vector<int> face;
		face.push_back(faces[f_i].A);
		face.push_back(faces[f_i].B);
		face.push_back(faces[f_i].C);

		bool match = true;
		for (int i = 0; i < face.size(); ++i) {
			if (matching[face[i]] == -1) {
				match = false;
				break;
			}
		}

		if (match) {
			// face fully matched
			//fixNormal(faces[f_i], true, nodes, centerOfMass);
			chamberFaces.push_back(faces[f_i]);
		}
		else {
			//fixNormal(faces[f_i], false, nodes, centerOfMass);
			notChamberFaces.push_back(faces[f_i]);
		}
	}

	addToFEM(nodes, faces, tets, edges, chamberFaces, notChamberFaces, fem, fem_tets);
}

void TetGenReader::fixNormal(TetGenFace &face, bool outward, const std::vector<TetGenNode> &nodes, Eigen::Vector3d centerOfMass) {
	int dir = (outward ? 1 : -1);

	std::vector<int> indices;
	indices.push_back(face.A);
	indices.push_back(face.B);
	indices.push_back(face.C);

	std::vector<Eigen::Vector3d> positions;
	for (int i = 0; i < 3; ++i) {
		positions.push_back(nodes[indices[i]].GetPosition());
	}

	
	for (int i = 0; i < 3; ++i) {
		for (int j = 0; j < 3; ++j) {
			for (int k = 0; k < 3; ++k) {
				if (i == j || i == k || j == k) { continue; }

				Face tempFace;
				tempFace.n1 = new Node(positions[i]);
				tempFace.n2 = new Node(positions[j]);
				tempFace.n3 = new Node(positions[k]);
				Eigen::Vector3d normal = tempFace.computeNormal();
				normal.normalize();

				Eigen::Vector3d center = tempFace.computeCenter();
				center -= centerOfMass;
				center.normalize();

				double cosAlphaSign = normal.x() * center.x() + normal.y() * center.y() + normal.z() * center.z();

				if ((outward && cosAlphaSign >= 0) || (!outward && cosAlphaSign < 0)) {
						// good
						face.A = indices[i];
						face.B = indices[j];
						face.C = indices[k];
						return;
				}
			}
		}
	}
	


}

std::vector<int> TetGenReader::matchNodesUsingConvexHull(std::vector<TetGenNode> n1, std::vector<TetGenNode> n2) {
	double minX, maxX, minY, maxY, minZ, maxZ;

	minX = maxX = n2[0].x;
	minY = maxY = n2[0].y;
	minZ = maxZ = n2[0].z;
	for (int i = 1; i < n2.size(); ++i) {
		minX = std::min(n2[i].x, minX);
		maxX = std::max(n2[i].x, maxX);
		minY = std::min(n2[i].y, minY);
		maxY = std::max(n2[i].y, maxY);
		minZ = std::min(n2[i].z, minZ);
		maxZ = std::max(n2[i].z, maxZ);
	}

	std::vector<int> matches(n1.size());
	for (size_t i = 0; i < n1.size(); ++i) {
		bool matched = false;

		if (minX <= n1[i].x && n1[i].x <= maxX &&
			minY <= n1[i].y && n1[i].y <= maxY &&
			minZ <= n1[i].z && n1[i].z <= maxZ) {

			matches[n1[i].index] = 0;
			matched = true;
		}
		
		if (!matched) {
			matches[n1[i].index] = -1;
		}
	}

	return matches;
}

std::vector<int> TetGenReader::matchNodes(std::vector<TetGenNode> n1, std::vector<TetGenNode> n2) {
	std::vector<int> matches(n1.size());
	for (size_t i = 0; i < n1.size(); ++i) {
		bool matched = false;
		for (size_t j = 0; j < n2.size(); ++j) {
			if (n1[i].Equals(n2[j])) {
				if (matched) {
					// if we match more than once for the same node, there may be a problem!
					// instead of this after function runs, we can call isNodeMappingCorrect <- slower
					std::cout << "Matching error: Consider increase node equals precision" << std::endl;
				}

				matches[n1[i].index] = n2[j].index;
				matched = true;
			}
		}
		if (!matched) {
			matches[n1[i].index] = -1;
		}
	}

	return matches;
}

void TetGenReader::convertIndexingToStartFrom0(std::vector<TetGenNode> &n, std::vector<TetGenFace> &f, std::vector<TetGenTetrahedron> &t, std::vector<TetGenEdge> &e) {
	int shift = -n[0].index;
	for (size_t i = 0; i < n.size(); ++i) {
		n[i].index += shift;
	}
	for (size_t i = 0; i < f.size(); ++i) {
		f[i].A += shift;
		f[i].B += shift;
		f[i].C += shift;
	}
	for (size_t i = 0; i < t.size(); ++i) {
		t[i].A += shift;
		t[i].B += shift;
		t[i].C += shift;
		t[i].D += shift;
	}
	for (size_t i = 0; i < e.size(); ++i) {
		e[i].A += shift;
		e[i].B += shift;
	}
}

void TetGenReader::convertIndexingToStartFrom0(std::vector<TetGenNode> &n, std::vector<TetGenFace> &f, std::vector<TetGenFace> &f2, std::vector<TetGenFace> &f3, std::vector<TetGenTetrahedron> &t, std::vector<TetGenEdge> &e) {
	int shift = -n[0].index;
	for (size_t i = 0; i < n.size(); ++i) {
		n[i].index += shift;
	}
	for (size_t i = 0; i < f.size(); ++i) {
		f[i].A += shift;
		f[i].B += shift;
		f[i].C += shift;
	}
	for (size_t i = 0; i < f2.size(); ++i) {
		f2[i].A += shift;
		f2[i].B += shift;
		f2[i].C += shift;
	}
	for (size_t i = 0; i < f3.size(); ++i) {
		f3[i].A += shift;
		f3[i].B += shift;
		f3[i].C += shift;
	}
	for (size_t i = 0; i < t.size(); ++i) {
		t[i].A += shift;
		t[i].B += shift;
		t[i].C += shift;
		t[i].D += shift;
	}
	for (size_t i = 0; i < e.size(); ++i) {
		e[i].A += shift;
		e[i].B += shift;
	}
}

void TetGenReader::rearrangeIndices(std::vector<TetGenNode> &n, std::vector<TetGenFace> &f, std::vector<TetGenTetrahedron> &t, std::vector<TetGenEdge> &e, std::vector<TetGenFace> &cf) {
	int shift = -n[0].index;
	int max = 0;
	for (size_t i = 0; i < n.size(); ++i) {
		n[i].index += shift;
	}
	for (size_t i = 0; i < f.size(); ++i) {
		f[i].A += shift;
		f[i].B += shift;
		f[i].C += shift;
	}
	for (size_t i = 0; i < t.size(); ++i) {
		t[i].A += shift;
		t[i].B += shift;
		t[i].C += shift;
		t[i].D += shift;
	}
	for (size_t i = 0; i < cf.size(); ++i) {
		cf[i].A += shift;
		cf[i].B += shift;
		cf[i].C += shift;
	}
	for (size_t i = 0; i < e.size(); ++i) {
		e[i].A += shift;
		e[i].B += shift;
	}
}

void TetGenReader::addToFEM(const std::vector<TetGenNode> &n, const std::vector<TetGenFace> &f, const std::vector<TetGenTetrahedron> &t, const std::vector<TetGenEdge> &e, const std::vector<TetGenFace> &cf, const std::vector<TetGenFace> &ncf, FEM* &fem, std::vector<Tetrahedron*> &tets) {
	Eigen::Matrix3d rotation;
	rotation = Eigen::AngleAxisd(M_PI / 2.0, Vector3d::UnitX()) *
		Eigen::AngleAxisd(0, Vector3d::UnitY()) * 
		Eigen::AngleAxisd(0, Vector3d::UnitZ());
	
	double minX = 0.0;
	double minY = 0.0;
	double minZ = 0.0;
	std::vector<Node*> nodes(n.size());
	for (size_t i = 0; i < nodes.size(); ++i) {
		Eigen::Vector3d position = Eigen::Vector3d(n[i].x, n[i].y, n[i].z);
		position = rotation * position;

		nodes[i] = new Node(position);
		nodes[i]->SetId_Full(i);

		minX = std::min(minX, position.x());
		minY = std::min(minY, position.y());
		minZ = std::min(minZ, position.z());
	}
	
	for (size_t i = 0; i < nodes.size(); ++i) {
		if (nodes[i]->GetPosition().y() <= minY+0.00001) {
			nodes[i]->FixNode();
		}
	}

	// TODO: edges are currently not used, so is there a need for it?
	std::vector<Edge*> edges;
	for (size_t i = 0; i < e.size(); ++i) {
		edges.push_back(new Edge(nodes[e[i].A], nodes[e[i].B]));
	}

	for (size_t i = 0; i < t.size(); ++i) {
		Tetrahedron* tetrahedron = new Tetrahedron(
			nodes[t[i].A],
			nodes[t[i].B],
			nodes[t[i].C],
			nodes[t[i].D]
		);
		tetrahedron->SetId(i);

		tets.push_back(tetrahedron);
	}

	std::vector<Face> chamberFaces;
	for (size_t i = 0; i < cf.size(); ++i) {
		chamberFaces.push_back(
			Face(
				nodes[cf[i].A],
				nodes[cf[i].B],
				nodes[cf[i].C]
			)
		);
	}

	std::vector<Face> notChamberFaces;
	for (size_t i = 0; i < ncf.size(); ++i) {
		notChamberFaces.push_back(
			Face(
				nodes[ncf[i].A],
				nodes[ncf[i].B],
				nodes[ncf[i].C]
			)
		);
	}

	std::vector<Face> faces;
	for (size_t i = 0; i < f.size(); ++i) {
		faces.push_back(
			Face(
				nodes[f[i].A],
				nodes[f[i].B],
				nodes[f[i].C]
			)
		);
	}

	//fem = new FEM(tets, edges, nodes, chamberFaces, 0.48, 50000000);
	std::cout << "Number of Tetrahedrons: " << tets.size() << std::endl;
	std::cout << "Number of Faces: " << faces.size() << " = (num of chamber faces: " << chamberFaces.size() << ") + (num of not chamber faces: " << notChamberFaces.size() << ")" << std::endl;

	//fem = new FEM(tets);
	fem = new FEM(tets, edges, nodes);
	fem->chamberFaces = chamberFaces;
	fem->notChamberFaces = notChamberFaces;
	fem->halfEdgeDS_mesh.build(chamberFaces);
	fem->faces = faces;

	fem->SetUseStiffnessWarping(true);
	fem->UseDirectSolver(true); //Cholesky
	fem->PrepareSimulationData();
}

// checks if node mapping is unique (no a->b, a->c, or a->c, b->c)
bool TetGenReader::isNodeMappingCorrect(std::vector<std::pair<int, int>> pairs) {
	std::vector<int> a(pairs.size());
	std::vector<int> b(pairs.size());

	for (size_t i = 0; i < pairs.size(); ++i) {
		a[i] = pairs[i].first;
		b[i] = pairs[i].second;
	}

	std::sort(a.begin(), a.end());
	for (size_t i = 1; i < a.size(); ++i) {
		if (a[i] == a[i - 1]) {
			return false;
		}
	}

	std::sort(b.begin(), b.end());
	for (size_t i = 1; i < b.size(); ++i) {
		if (b[i] == b[i - 1]) {
			return false;
		}
	}

	return true;
}

std::vector<TetGenNode> TetGenReader::readNodes(std::string fileName) {
	std::vector<std::string> lines;
	readFileLinesInto(fileName, lines);

	std::vector<TetGenNode> nodes;

	// skip first line - not relevant to nodes
	for (size_t i = 1; i < lines.size(); ++i) {
		std::istringstream stm(lines[i]);

		// perform formatted input from the string stream
		double x, y, z;
		int index;
		if (stm >> index >> x >> y >> z)
		{
			nodes.push_back(TetGenNode(index, x, y, z));
			//std::cout << index << " " << x << " " << y << " " << z << std::endl;
		}
	}

	return nodes;
}

std::vector<TetGenFace> TetGenReader::readFaces(std::string fileName) {
	std::vector<std::string> lines;
	readFileLinesInto(fileName, lines);

	std::vector<TetGenFace> faces;

	// skip first line - not relevant to nodes
	for (size_t i = 1; i < lines.size(); ++i) {
		std::istringstream stm(lines[i]);

		// perform formatted input from the string stream
		int index; 
		int A, B, C, temp;
		if (stm >> index >> A >> B >> C >> temp)
		{
			faces.push_back(TetGenFace(A, B, C));
			//std::cout << index << " " << A << " " << B << " " << C << std::endl;
		}
	}

	return faces;
}

void TetGenReader::readFacesPoly(std::string fileName, std::vector<TetGenFace>& faces, std::vector<TetGenFace>& chamberFaces, std::vector<TetGenFace>& notChamberFaces) {
		std::vector<std::string> lines;
		readFileLinesInto(fileName, lines);

		// skip first line - not relevant to nodes
		for (size_t i = 1; i < lines.size(); ++i) {
			std::istringstream stm(lines[i]);

			// perform formatted input from the string stream
			int index;
			int A, B, C, mark;
			if (stm >> index >> A >> B >> C >> mark)
			{
				faces.push_back(TetGenFace(A, B, C));
				if (mark == 1) {
					notChamberFaces.push_back(TetGenFace(A, B, C));
				}
				else {
					chamberFaces.push_back(TetGenFace(A, B, C));
				}
			}
		}
}

std::vector<TetGenTetrahedron> TetGenReader::readTetrahedrons(std::string fileName) {
	std::vector<std::string> lines;
	readFileLinesInto(fileName, lines);

	std::vector<TetGenTetrahedron> tets;

	// skip first line - not relevant to nodes
	for (size_t i = 1; i < lines.size(); ++i) {
		std::istringstream stm(lines[i]);

		// perform formatted input from the string stream
		int index;
		int A, B, C, D;
		if (stm >> index >> A >> B >> C >> D)
		{
			tets.push_back(TetGenTetrahedron(A, B, C, D));
			//std::cout << index << " " << A << " " << B << " " << C << " " << D << std::endl;
		}
	}

	return tets;
}

std::vector<TetGenEdge> TetGenReader::readEdges(std::string fileName) {
	std::vector<std::string> lines;
	readFileLinesInto(fileName, lines);

	std::vector<TetGenEdge> edges;

	// skip first line - not relevant to nodes
	for (size_t i = 1; i < lines.size(); ++i) {
		std::istringstream stm(lines[i]);

		// perform formatted input from the string stream
		int index;
		int A, B, temp;
		if (stm >> index >> A >> B >> temp)
		{
			edges.push_back(TetGenEdge(A, B));
			//std::cout << index << " " << A << " " << B << " " << C << std::endl;
		}
	}

	return edges;
}

void TetGenReader::readFileLinesInto(std::string fileName, std::vector<std::string> &lines) {
	std::string line;
	std::ifstream myfile(fileName);

	if (myfile.is_open())
	{
		while (getline(myfile, line))
		{
			lines.push_back(line);
		}
		myfile.close();
	}
}